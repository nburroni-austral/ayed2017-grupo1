package struct.impl.queues;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 12/04/2017.
 */
public class PriorityQueueTest {

    PriorityQueue<Integer> queue = new PriorityQueue<>(4);

    @Test
    public void enqueue() throws Exception {
        queue.enqueue(1,4);
        queue.enqueue(2,4);
        queue.enqueue(3,3);
        queue.enqueue(4,3);
        queue.enqueue(5,2);
        queue.enqueue(6,2);
        queue.enqueue(7,1);
        queue.enqueue(8,1);

        assertEquals(8,queue.size());
        assertEquals((Integer) 1, queue.dequeue());
    }

    @Test
    public void dequeue() throws Exception {
        queue.enqueue(1,4);
        queue.enqueue(2,4);
        queue.enqueue(3,3);
        queue.enqueue(4,3);
        queue.enqueue(5,2);
        queue.enqueue(6,2);
        queue.enqueue(7,1);
        queue.enqueue(8,1);

        int initSize = queue.size();

        for(int i = 0; i < initSize - 1; i++){
            queue.dequeue();
        }

        assertEquals((Integer)8,queue.dequeue());
    }
    
    @Test
    public void empty() throws Exception {

    }

}