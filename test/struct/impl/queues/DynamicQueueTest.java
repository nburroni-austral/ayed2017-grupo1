package struct.impl.queues;


import org.junit.Test;
import struct.istruct.Queue;

import static org.junit.Assert.assertEquals;


/**
 * Created by bomber on 4/6/17.
 */
class DynamicQueueTest {

    Queue<Character> queue = new DynamicQueue<>();

    @Test
    void enqueue() {
        queue.enqueue('a');
        queue.enqueue('b');
        queue.enqueue('c');

        assertEquals('a', (char)queue.dequeue());
        assertEquals('b', (char)queue.dequeue());
        assertEquals('c', (char)queue.dequeue());
    }

    @Test
    void dequeue() {
    }

    @Test
    void isEmpty() {
        assertEquals(true, queue.isEmpty());
        queue.enqueue('a');
        assertEquals(false, queue.isEmpty());
        queue.enqueue('b');
        assertEquals(false, queue.isEmpty());
        queue.dequeue();
        queue.dequeue();

    }

    @Test
    void length() {
        queue.enqueue('a');
        assertEquals(1, queue.length());
        queue.enqueue('b');
        assertEquals(2, queue.length());
        queue.dequeue();
        assertEquals(1, queue.length());
    }

    @Test
    void capacity() {
        queue.enqueue('a');

        for (int i=0; i< 20; i++) {
            queue.enqueue('a');
        }
    }

    @Test
    void empty() {
    }

}