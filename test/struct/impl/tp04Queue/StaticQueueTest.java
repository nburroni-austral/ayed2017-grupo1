package struct.impl.tp04Queue;

import org.junit.Test;
import struct.impl.queues.StaticQueue;

import static org.junit.Assert.*;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 05/04/2017.
 */
public class StaticQueueTest {

    StaticQueue<Integer> a = new StaticQueue<>(10);

    @Test
    public void enqueueAndDequeueTest() throws Exception {
        a.enqueue(1);
        a.enqueue(2);
        a.enqueue(3);


        assertEquals(1,a.dequeue(),0.1);
        assertEquals(2,a.dequeue(),0.1);
        assertEquals(3,a.dequeue(),0.1);

        a.enqueue(4);
        a.enqueue(5);
        a.enqueue(6);
        a.enqueue(7);

        assertEquals(4,a.dequeue(),0.1);
        assertEquals(5,a.dequeue(),0.1);
        assertEquals(6,a.dequeue(),0.1);
        assertEquals(7,a.dequeue(),0.1);

        a.enqueue(1);
        a.enqueue(2);
        a.enqueue(3);
        a.enqueue(4);
        a.enqueue(5);
        a.enqueue(6);
        a.enqueue(7);
        a.enqueue(8);
        a.enqueue(9);
        a.enqueue(10);
        a.enqueue(11);

        assertEquals((Integer) 1, a.dequeue());
        assertEquals((Integer) 2, a.dequeue());
        assertEquals((Integer) 3, a.dequeue());
        assertEquals((Integer) 4, a.dequeue());
        assertEquals((Integer) 5, a.dequeue());
        assertEquals((Integer) 6, a.dequeue());
        assertEquals((Integer) 7, a.dequeue());
        assertEquals((Integer) 8, a.dequeue());
        assertEquals((Integer) 9, a.dequeue());
        assertEquals((Integer) 10, a.dequeue());
        assertEquals((Integer) 11, a.dequeue());

    }

    @Test
    public void emptyTest() throws Exception {

    }

}