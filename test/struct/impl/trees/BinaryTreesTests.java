package struct.impl.trees;



import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


class BinaryTreesTests {
    /*
                                                             30
                     ┌───────────────────────┴───────────────────────┐
                      20                                              20
         ┌───────────┴───────────┐                       ┌───────────┴───────────┐
          10                  2                    10                      2
   ┌─────┴─────┐                                         └─────┐                 └─────┐
    8                   6                                                    6                       8
 ┌──┴──┐    ┌──┴──┐                                         ┌──┴──┐                 ┌──┴──┐
  1     2    3     4         3                                        4           2                1


     */


    // Left
    IntegerBiTree lvl5p1 = new IntegerBiTree(1);
    IntegerBiTree lvl5p2 = new IntegerBiTree(2);
    IntegerBiTree lvl4p1 = new IntegerBiTree(8, lvl5p1, lvl5p2);

    IntegerBiTree lvl5p3 = new IntegerBiTree(3);
    IntegerBiTree lvl5p4 = new IntegerBiTree(4);
    IntegerBiTree lvl4p2 = new IntegerBiTree(6, lvl5p3, lvl5p4);

    IntegerBiTree lvl3p1 = new IntegerBiTree(10, lvl4p1, lvl4p2);
    IntegerBiTree lvl3p2 = new IntegerBiTree(2);
    IntegerBiTree lvl2p1 = new IntegerBiTree(20, lvl3p1, lvl3p2);

    // Right
    IntegerBiTree lvl5p5 = new IntegerBiTree(3);
    IntegerBiTree lvl5p6 = new IntegerBiTree(4);
    IntegerBiTree lvl4p3 = new IntegerBiTree(6, lvl5p5, lvl5p6);
    IntegerBiTree lvl3p3 = new IntegerBiTree(10, new IntegerBiTree(), lvl4p3);

    IntegerBiTree lvl5p7 = new IntegerBiTree(2);
    IntegerBiTree lvl5p8 = new IntegerBiTree(1);
    IntegerBiTree lvl4p4 = new IntegerBiTree(8, lvl5p7, lvl5p8);
    IntegerBiTree lvl3p4 = new IntegerBiTree(2, new IntegerBiTree(), lvl4p4);

    IntegerBiTree lvl2p2 = new IntegerBiTree(20, lvl3p3, lvl3p4);

    IntegerBiTree lvl1p1 = new IntegerBiTree(30, lvl2p1, lvl2p2);

    BiTreeAnalyzer analyzer = new BiTreeAnalyzer();

    @Test
    public void nodesOnLevel() {
        assertEquals(2, analyzer.nodesOnLevel(lvl1p1, 2));
        assertEquals(4, analyzer.nodesOnLevel(lvl1p1, 4));

        assertEquals(4, analyzer.nodesOnLevel(lvl3p1, 3));
    }

    @Test
    public void amountOfLeaves() {
        assertEquals(9, analyzer.amountOfLeaves(lvl1p1));
        assertEquals(4, analyzer.amountOfLeaves(lvl3p1));
        assertEquals( 5, analyzer.amountOfLeaves(lvl2p1));
        assertEquals( 4, analyzer.amountOfLeaves(lvl2p2));
    }

    @Test
    public void weight() {
        assertEquals(19, analyzer.weight(lvl1p1));

        assertEquals(7, analyzer.weight(lvl3p1));
        assertEquals(4, analyzer.weight(lvl3p3));
    }

    @Test
    public void elementOcurrences() {
        assertEquals(4, analyzer.elementOccurrences(lvl1p1, 2));
        assertEquals(2, analyzer.elementOccurrences(lvl1p1, 1));

        assertEquals(2, analyzer.elementOccurrences(lvl2p1, 2));



    }
    @Test
    public void height() {
        assertEquals(5, analyzer.height(lvl1p1));

        assertEquals(4, analyzer.height(lvl2p1));
        assertEquals(4, analyzer.height(lvl2p2));

        assertEquals(3, analyzer.height(lvl3p1));
        assertEquals(3, analyzer.height(lvl3p4));

        assertEquals(2, analyzer.height(lvl4p1));
        assertEquals(1, analyzer.height(lvl5p1));
    }

    @Test
    public void getFrontier() {
        ArrayList<Integer> list = new ArrayList<>();
        Integer[] arr = {1,2,3,4,2,3,4,2,1};
        for (Integer i : arr)
            list.add(i);

        assertEquals(list, analyzer.getFrontier(lvl1p1));

        list = new ArrayList<>();
        Integer[] otherArr = {1,2,3,4,2};
        for (Integer i : otherArr)
            list.add(i);
        assertEquals(list, analyzer.getFrontier(lvl2p1));
    }

    @Test
    public void isomorphs () {
        assertTrue(analyzer.areIsomorphs(lvl3p3, lvl3p4));
        assertFalse(analyzer.areIsomorphs(lvl1p1, lvl3p4));
    }

    @Test
    public void similar () {
        assertTrue(analyzer.areSimilar(lvl2p1, lvl2p2));
        assertTrue(analyzer.areSimilar(lvl4p2, lvl4p3));
        assertFalse(analyzer.areSimilar(lvl4p1, lvl4p2));
    }


    @Test
    public void complete() {
        assertTrue(analyzer.isComplete(lvl3p1));
        assertTrue(analyzer.isComplete(lvl2p1));
    }

    @Test
    public void full() {
        assertFalse(analyzer.isFull(lvl2p1));
        assertTrue(analyzer.isFull(lvl3p1));
    }

    @Test
    public void identical() {
        assertTrue(analyzer.areIdentical(lvl4p2, lvl4p3));
        assertFalse(analyzer.areIdentical(lvl4p2, lvl1p1));
        assertFalse(analyzer.areIdentical(lvl4p2, lvl4p1));
    }

    @Test
    public void subTree() {
        assertTrue(analyzer.isSubtree(lvl1p1, lvl4p2));
    }


    // Integer binary trees only
    @Test
    public void sum() {
        assertEquals(11, IntegerBiTree.sumOfTree(lvl4p1));
        assertEquals(13, IntegerBiTree.sumOfTree(lvl4p2));
        assertEquals(34, IntegerBiTree.sumOfTree(lvl3p1));
    }

    @Test
    public void sumIfMultiple() {
        assertEquals(10, IntegerBiTree.sumOfMultiples(lvl4p1,2));
        assertEquals(9, IntegerBiTree.sumOfMultiples(lvl4p2,3));
    }
}