package main.tp01_Search;

import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;

import static org.junit.Assert.*;


/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 13/03/2017.
 */

public class SorterTest {

    Sorter sorter = new Sorter();
    int[] nums1 = {9,8,7,6,5,4,3,2,1,0};
    int[] nums2 = {9,9,8,8,7,7,6,6,5,5};
    int[] nums3 = {7,4,6,3,4,5,2,6,31,1};

    @Test
    public void insertionSort() throws Exception {
        sorter.insertionSort(nums1);
        sorter.insertionSort(nums2);
        sorter.insertionSort(nums3);

        System.out.println(Arrays.toString(nums1));
        System.out.println(Arrays.toString(nums2));
        System.out.println(Arrays.toString(nums3));
    }

    @Test
    public void bubbleSort() throws Exception {
        sorter.bubbleSort(nums1);
        sorter.bubbleSort(nums2);
        sorter.bubbleSort(nums3);

        System.out.println(Arrays.toString(nums1));
        System.out.println(Arrays.toString(nums2));
        System.out.println(Arrays.toString(nums3));
    }

    @Test
    public void selectionSort() throws Exception {
        sorter.selectionSort(nums1);
        sorter.selectionSort(nums2);
        sorter.selectionSort(nums3);

        System.out.println(Arrays.toString(nums1));
        System.out.println(Arrays.toString(nums2));
        System.out.println(Arrays.toString(nums3));
    }

    @Test
    public void recursiveSelectionSort() throws Exception {
        sorter.recursiveSelectionSort(nums1);
        sorter.recursiveSelectionSort(nums2);
        sorter.recursiveSelectionSort(nums3);

        System.out.println(Arrays.toString(nums1));
        System.out.println(Arrays.toString(nums2));
        System.out.println(Arrays.toString(nums3));
    }


    Comparator<String> byLength = new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {
            int conclusion = 0;

            if (o1.length() > o2.length()) conclusion = 1;
            if (o1.length() == o2.length()) conclusion = 0;
            if (o1.length() < o1.length()) conclusion = -1;

            return conclusion;
        }
    };

    String[] unsorted = {"aaaaaaaaa","bb","cccc","ddddddddddddddddddd","e","ffff","ggg"};
    String[] sorted = {"e", "bb", "ggg", "cccc", "ffff", "aaaaaaaaa", "ddddddddddddddddddd"};


    @Test
    public void selectionSortByComparator() throws Exception {
        System.out.println(Arrays.toString(unsorted));
        sorter.selectionSortByComparator(unsorted, byLength);
        System.out.println(Arrays.toString(unsorted));
        assertArrayEquals(sorted, unsorted);
    }

    @Test
    public void insertionSortByComparator() throws Exception {
        System.out.println(Arrays.toString(unsorted));
        sorter.insertionSortByComparator(unsorted, byLength);
        System.out.println(Arrays.toString(unsorted));
        assertArrayEquals(sorted, unsorted);
    }

    @Test
    public void bubbleSortByComparator() throws Exception {
        System.out.println(Arrays.toString(unsorted));
        sorter.bubbleSortByComparator(unsorted, byLength);
        System.out.println(Arrays.toString(unsorted));
        assertArrayEquals(sorted, unsorted);
    }

}