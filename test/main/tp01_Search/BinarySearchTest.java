package main.tp01_Search;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 13/03/2017.
 */
public class BinarySearchTest {
    @Test
    public void findIndex() throws Exception {
        int[] sorted1 = {1,2,3,4,5,6,7,8,9};
        int[] sorted2 = {1,2,3,4,5,6,7,8};
        int[] sorted3 = {3,4,5,6,7,8,9};

        BinarySearch binarySearcher = new BinarySearch();

        assertEquals(1, binarySearcher.findIndex(2,sorted1));
        assertEquals(1, binarySearcher.findIndex(2,sorted2));
        assertEquals(-1, binarySearcher.findIndex(2,sorted3));
    }

}