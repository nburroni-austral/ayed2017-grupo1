package main.tp01_Search;

import org.junit.Test;

import java.util.Arrays;
import java.util.function.Consumer;

import static org.junit.Assert.assertArrayEquals;

/**
 * Created by bomber on 3/14/17.
 */
class StringSorterTest {

    String[] toSort = {"a", "c", "ab", "cd", "abc", "ab", "b",  "x", "z", "y"};
    String[] sorted = {"a", "ab", "ab", "abc", "b", "c", "cd", "x", "y", "z"};
    StringSorter sorter = new StringSorter();

    @Test
    void insertionSort() {
        testMethod(sorter::insertionSort, toSort);
        assertArrayEquals(sorted, toSort);
    }

    @Test
    void bubbleSort() {
        testMethod(sorter::bubbleSort, toSort);
        assertArrayEquals(sorted, toSort);
    }

    @Test
    void selection() {
        testMethod(sorter::selection, toSort);
        assertArrayEquals(sorted, toSort);
    }

    void testMethod(Consumer<String[]> sortingAl, String[] toSort) {
        System.out.println("Testing...");
        System.out.println(Arrays.toString(toSort));
        sortingAl.accept(toSort);
        System.out.println(Arrays.toString(toSort));
    }

}