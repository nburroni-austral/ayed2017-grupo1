package main.tp03_Stack;

import org.junit.Test;
import struct.impl.stacks.DynamicStack;

import java.util.EmptyStackException;

import static org.junit.Assert.*;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 22/03/2017.
 */
public class DynamicStackTest {
    DynamicStack<Integer> stack = new DynamicStack<>();

    @Test
    public void push() throws Exception {
        stack.push(1);
        assertEquals(1, stack.size());
        stack.push(2);
        assertEquals(2, stack.size());
        stack.push(3);
        assertEquals(3, stack.size());
        stack.push(4);
        assertEquals(4, stack.size());
        stack.push(5);
        assertEquals(5, stack.size());
    }

    @Test (expected = EmptyStackException.class)
    public void pop() throws Exception {
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);

        stack.pop();
        assertEquals((Integer) 4, stack.peek());
        stack.pop();
        assertEquals((Integer)3, stack.peek());
        stack.pop();
        assertEquals((Integer)2, stack.peek());
        stack.pop();
        assertEquals((Integer)1, stack.peek());
        stack.pop();
        stack.peek();
    }

    @Test
    public void empty() throws Exception {
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);
        assertEquals(5,stack.size());

        stack.empty();
        assertEquals(0,stack.size());
    }

}