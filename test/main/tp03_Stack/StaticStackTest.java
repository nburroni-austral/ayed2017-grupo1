package main.tp03_Stack;

import org.junit.Test;
import struct.impl.stacks.StaticStack;

import java.util.EmptyStackException;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 22/03/2017.
 */
public class StaticStackTest {
    StaticStack<Integer> stack = new StaticStack<>(10);

    @Test
    public void pushTest() throws Exception {

        stack.push(1);
        System.out.println("Top: " + stack.peek());
        stack.push(2);
        System.out.println("Top: " + stack.peek());
        stack.push(3);
        System.out.println("Top: " + stack.peek());
        stack.push(4);
        System.out.println("Top: " + stack.peek());
        stack.push(5);
        System.out.println("Top: " + stack.peek());

        System.out.println("Size: " + stack.size());
        System.out.println("");
    }

    @Test
    public void popTest() throws Exception {
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);
        stack.push(6);

        System.out.println("Top: " + stack.peek());

        stack.pop();

        System.out.println("Top: " + stack.peek());
    }


    @Test (expected = EmptyStackException.class)
    public void emptyTest() throws Exception {

        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);
        stack.push(6);

        stack.empty();

        stack.peek();

    }

}