package main.tp08_BinarySearchTrees;

import org.junit.Test;

import java.util.Queue;

import static org.junit.Assert.*;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 24/04/2017.
 */
public class BiSearchTreeTest {

    BiSearchTree<Integer,String> root = new BiSearchTree<>(5, "abcde");

    public void fillRoot(){
        root.insert(4,"abcd");
        root.insert(6,"abcdef");
        root.insert(7,"abcdefg");
        root.insert(8,"abcdefgh");
    }

    @Test
    public void containsKeyTest() throws Exception {
        fillRoot();

        assertTrue(root.contains(4));
        assertTrue(root.contains(5));
        assertTrue(root.contains(6));
        assertTrue(root.contains(7));
        assertTrue(root.contains(8));
    }

    @Test
    public void containsValTest() throws Exception {
        fillRoot();

        assertTrue(root.contains("abcd"));
        assertTrue(root.contains("abcde"));
        assertTrue(root.contains("abcdef"));
        assertTrue(root.contains("abcdefg"));
        assertTrue(root.contains("abcdefgh"));
    }

    @Test
    public void remove() throws Exception {
        fillRoot();

        root.remove(4);
        assertFalse(root.contains(4));
        root.remove(5);
        assertFalse(root.contains(5));
        root.remove(6);
        assertFalse(root.contains(6));
        root.remove(7);
        assertFalse(root.contains(7));
    }

    @Test
    public void search() throws Exception {
        fillRoot();

        assertEquals("abcd", root.search(4));
    }

    @Test
    public void getValuesOrderedByKeysTest() throws Exception {
        fillRoot();

        Queue<String> ordered = root.getValuesOrderedByKeys();

        for(String q : ordered){
            System.out.println(q);
        }
    }

    @Test
    public void fillListWithOrderedValuesTest() throws Exception {
        fillRoot();


    }

}