package main.tp06_SortedList;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 17/05/2017.
 */
public class BusSystemTest {
    BusSystem busSystem = new BusSystem();

    @Test
    public void addBusT() throws Exception {
        Bus bus = new Bus(1,2,3,false);
        busSystem.addBus(bus);

        assertEquals(bus,busSystem.getBusSortedList().getActual());
    }

    @Test
    public void saveT() throws Exception {
        Bus bus = new Bus(1, 2, 3, false);
        busSystem.addBus(bus);

        busSystem.save();

       // assertTrue(busSystem.getBusSortedList().getActual().equals(busSystem.load().getActual()));

    }

    @Test
    public void loadT() throws Exception {

    }

}