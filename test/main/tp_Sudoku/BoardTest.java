package main.tp_Sudoku;

import org.junit.Test;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 30/03/2017.
 */

public class BoardTest {

    Board board = new Board(9);

    @Test
    public void solve() throws Exception {

        board.addValue(9, 0, 1);
        board.addValue(7, 1, 1);
        board.addValue(5, 1, 2);
        board.addValue(2, 2, 1);
        board.addValue(2, 0, 4);
        board.addValue(6, 0, 5);
        board.addValue(4, 1, 5);
        board.addValue(8, 2, 3);
        board.addValue(7, 0, 6);
        board.addValue(8, 0, 8);
        board.addValue(3, 2, 8);
        board.addValue(5, 3, 0);
        board.addValue(3, 4, 0);
        board.addValue(4, 4, 1);
        board.addValue(9, 5, 0);
        board.addValue(8, 5, 1);
        board.addValue(2, 3, 3);
        board.addValue(9, 3, 5);

        // row test
        /*board.addValue(1,0,0);
        board.addValue(3,0,2);
        board.addValue(9,0,3);*/

        board.print();

        System.out.println("\n ------------------------------------------- ");

        board.solve();

        board.print();
    }

}