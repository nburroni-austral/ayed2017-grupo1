package main.tp07_BinaryTrees;

import org.junit.Test;
import struct.impl.trees.BinaryTree;

import static org.junit.Assert.*;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 08/04/2017.
 */
public class TreeAnalyzerTest {

    BinaryTree<Integer> leaf1 = new BinaryTree<>(2);
    BinaryTree<Integer> leaf2 = new BinaryTree<>(5);
    BinaryTree<Integer> leaf3 = new BinaryTree<>(11);
    BinaryTree<Integer> leaf4 = new BinaryTree<>(4);
    BinaryTree<Integer> noLeaf = new BinaryTree<>();

    BinaryTree<Integer> subTree1 = new BinaryTree<>(6, leaf2 ,leaf3 );
    BinaryTree<Integer> subTree2 = new BinaryTree<>(9, leaf4 , noLeaf );
    BinaryTree<Integer> noSubtree = new BinaryTree<>();

    BinaryTree<Integer> tree1 = new BinaryTree<>(7,leaf1 ,subTree1 );
    BinaryTree<Integer> tree2 = new BinaryTree<>(5, noSubtree , subTree2 );

    BinaryTree<Integer> root = new BinaryTree<>(2, tree1 , tree2);

    TreeAnalyzer treeAnalyzer = new TreeAnalyzer();

    @Test
    public void weight() {
        assertEquals(9,treeAnalyzer.weight(root));
    }

    @Test
    public void leaves() {
        assertEquals(4,treeAnalyzer.leaves(root));
    }

    @Test
    public void timesPresent() throws Exception {
        assertEquals(2,treeAnalyzer.timesPresent(root,2));
        assertEquals(2,treeAnalyzer.timesPresent(root,5));
    }

    @Test
    public void elementsAtLevel() throws Exception {
        assertEquals(1,treeAnalyzer.elementsAtLevel(root,0));
        assertEquals(2,treeAnalyzer.elementsAtLevel(root,1));
        assertEquals(3,treeAnalyzer.elementsAtLevel(root,2));
        assertEquals(3,treeAnalyzer.elementsAtLevel(root,3));
    }

    @Test
    public void height() throws Exception{
        assertEquals(3,treeAnalyzer.height(root));
    }

}