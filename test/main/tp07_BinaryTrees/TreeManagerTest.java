package main.tp07_BinaryTrees;

import org.junit.Test;
import struct.impl.trees.BinaryTree;
import struct.impl.trees.BiTreeAnalyzer;

import static org.junit.Assert.*;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 11/04/2017.
 */
public class TreeManagerTest {
    BinaryTree<Integer> leaf1 = new BinaryTree<>(2);
    BinaryTree<Integer> leaf2 = new BinaryTree<>(5);
    BinaryTree<Integer> leaf3 = new BinaryTree<>(11);
    BinaryTree<Integer> leaf4 = new BinaryTree<>(4);
    BinaryTree<Integer> noLeaf = new BinaryTree<>();

    BinaryTree<Integer> subTree1 = new BinaryTree<>(6, leaf2 ,leaf3 );
    BinaryTree<Integer> subTree2 = new BinaryTree<>(9, leaf4 , noLeaf );
    BinaryTree<Integer> noSubtree = new BinaryTree<>();

    BinaryTree<Integer> tree1 = new BinaryTree<>(7,leaf1 ,subTree1 );
    BinaryTree<Integer> tree2 = new BinaryTree<>(5, noSubtree , subTree2 );

    BinaryTree<Integer> root = new BinaryTree<>(2, tree1 , tree2);

    TreeManager treeManager = new TreeManager();
    TreeAnalyzer treeAnalyzer = new TreeAnalyzer();
    BiTreeAnalyzer biTreeAnalyzer = new BiTreeAnalyzer();

    @Test
    public void saveAndRead() throws Exception {
        treeManager.save(root,"cachilo");
        BinaryTree<Integer> recovered = (BinaryTree<Integer>) treeManager.read("cachilo");

        assertTrue(treeAnalyzer.preOrder(root).equals(treeAnalyzer.preOrder(recovered)));

    }

}