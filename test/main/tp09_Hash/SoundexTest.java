package main.tp09_Hash;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SoundexTest {
    @Test
    void encode() {
        assertEquals(Soundex.encode("Robert"), Soundex.encode("Rupert"));
        assertEquals("R150", Soundex.encode("Rubin"));
    }
}