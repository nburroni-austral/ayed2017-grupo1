package main.tp03_Stack.Calculator;


import struct.impl.stacks.DynamicStack;

/**
 * Calculator
 *
 * A DynamicStack-based representation of a calculator.
 *
 * @author Nicolás Gargano <nicolas.gargano @ ing.austral.edu.ar>, Lautaro Paskevicius <lautaro.paskevicius @ ing.austral.edu.ar>
 */

public class Calculator {
    private DynamicStack<String> stackedExpression = new DynamicStack<>();

    /**
     * <b>toPostfix</b>
     *
     * Transforms a String written in the infix notation to a DynamicStack<String> representing the postfix notation equivalent.
     *
     * @param infix A string written in the common infix notation, supposing a correct mathematical operation, spaces between operators and numbers, and no parentheses.
     *              e.g.  4 * 2 + 12 ^ -2 - 32 * 14
     * @return A DynamicStack<String> ordered in the postfix notation.
     *              e.g.  4 2 * 12 -2 ^ + 32 12 * -
     */
    public void toPostfix(String infix){
        DynamicStack<String> operatorsStack = new DynamicStack<>();  // A stack that pushes operators following its hierarchy. Higher reach bottom, lower tend to the top.

        for (String token : infix.split("\\s")){
            if(!operatorsStack.isEmpty()){
                String topOperator =  operatorsStack.peek();

                //If token has bigger hierarchy, push in stack.
                // If not, its either a number, a negative number, or a higher operator

                if(hierarchy(token.charAt(0)) > hierarchy(topOperator.charAt(0))){ //Is the first character a higher operator than the top? No? Enter (0 higher than 3)
                    operatorsStack.push(token);
                } else if ( token.length() == 1) {  // Check if its a number or an operator
                    if (hierarchy(token.charAt(0)) == 0) {  // Is it a number?
                        stackedExpression.push(token);
                    } else {                                // Then its a higher operator
                        while (operatorsStack.size() > 0){  // Empty operatorStack into the stackedExpression
                            stackedExpression.push(operatorsStack.peek());
                            operatorsStack.pop();
                        }
                        operatorsStack.push(token);         // Push operator into the empty operatorStack
                    }
                } else if ( token.length() > 1) {   // Its clearly a number
                    stackedExpression.push(token);
                }
            } else {
                if(hierarchy(token.charAt(0)) > 0){   // If its an operator
                    operatorsStack.push(token);
                } else {
                    stackedExpression.push(token);    // Else, is a number
                }
            }
        }

        while (operatorsStack.size() > 0){    // Finally, empty operators stack
            stackedExpression.push(operatorsStack.peek());
            operatorsStack.pop();
        }

    }

    public Double solve(){

        return Double.parseDouble(stackedExpression.peek());
    }

    private Double simpleSolve(char operator, Double partA, Double partB) {

        if(( stackedExpression.peek()).charAt(0) == '+') return partA + partB;
        if(( stackedExpression.peek()).charAt(0) == '-') return partA - partB;
        if(( stackedExpression.peek()).charAt(0) == '*') return partA * partB;
        if(( stackedExpression.peek()).charAt(0) == '/') return partA / partB;
        if(( stackedExpression.peek()).charAt(0) == '^') return Math.pow(partA,partB);

        return null;
    }

    /**
     * <b>hierarchy</b>
     *
     * Checks the order of exection of an operation respecting its operator.
     *
     * @param operator A character that could be an operator, and if not, a number.
     * @return An int representing the order of operations in a mathematical operation.
     */
    public int hierarchy(char operator){
        if(operator == '+' || operator == '-') return 1;
        if(operator == '*' || operator == '/') return 2;
        if(operator == '^') return 3;
        return 0;
    }
}
