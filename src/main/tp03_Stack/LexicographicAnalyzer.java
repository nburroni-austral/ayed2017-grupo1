package main.tp03_Stack;

import struct.impl.stacks.DynamicStack;
import struct.istruct.Stack;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


/**
 * Provides different methods of checking the balance of brackets, parenthesis and curly braces, between strings.
 * @author Nicolás Gargano <nicolas.gargano @ ing.austral.edu.ar>, Lautaro Paskevicius <lautaro.paskevicius @ ing.austral.edu.ar>
 */
public class LexicographicAnalyzer {
    public LexicographicAnalyzer() {
        stack = new DynamicStack<>();
    }

    private Stack<Character> stack;

    /**
     *
     * Checks whether brackets, parenthesis and curly braces are balanced on input file.
     *
     * @param path Path to the file to check.
     */
    public boolean fromPath(String path) {
        File file = new File(path);
        if (!file.exists()) {
            System.out.println(file + " does not exist.");
        }
        if (!(file.isFile() && file.canRead())) {
            System.out.println(file.getName() + " cannot be read from.");
        }
        try {
            byte[] encoded = Files.readAllBytes(Paths.get(path));
            return isValid(new String(encoded));

        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     *
     * Checks whether brackets, parenthesis and curly braces are balanced on input.
     *
     * @param text String to check.
     */
    public boolean isValid(String text) {
        return isValid(text.toCharArray());
    }

    /**
     *
     * Checks whether brackets, parenthesis and curly braces are balanced on input.
     *
     * @param chars array of chars to check.
     */
    public boolean isValid(char[] chars) {
        for (char c : chars) {
            if (c == '(' | c == '[' | c == '{')
                stack.push(c);

            else if (c == ')')
                if(stack.isEmpty())
                    return false;
                else if ('(' == stack.peek())
                    stack.pop();
                else
                    return false;

            else if (c == ']')
                if(stack.isEmpty())
                    return false;
                else if ('[' == stack.peek())
                    stack.pop();
                else
                    return false;

            else if (c == '}')
                if(stack.isEmpty())
                    return false;
                else if ('{' == stack.peek())
                    stack.pop();
                else
                    return false;
        }

        return stack.isEmpty();
    }
}
