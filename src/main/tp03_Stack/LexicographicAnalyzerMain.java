package main.tp03_Stack;

import main.tp03_Stack.LexicographicAnalyzer;

import java.util.Scanner;

public class LexicographicAnalyzerMain {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Lexicographic Analyzer");
        System.out.println("1. Custom Text");
        System.out.println("2. From file");

        LexicographicAnalyzer lexAn = new LexicographicAnalyzer();
        boolean isValid = false;
        switch (scanner.nextInt()) {
            case 1:
                System.out.println("Enter text:");
                isValid = lexAn.isValid(scanner.nextLine());
            case 2:
                System.out.println("Enter file path:");
                isValid = lexAn.fromPath(scanner.nextLine());
        }

        System.out.println("Your file is " + (isValid? "valid": "invalid" + "."));
    }
}
