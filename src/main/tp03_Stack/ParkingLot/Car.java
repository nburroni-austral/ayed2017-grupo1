package main.tp03_Stack.ParkingLot;

/**
 * Car
 *
 * OOP representation of a real life car; a mean of transportation.
 *
 * @author Nicolás Gargano <nicolas.gargano @ ing.austral.edu.ar>, Lautaro Paskevicius <lautaro.paskevicius @ ing.austral.edu.ar>
 */

public class Car {

    private int serialID;
    private String brand;
    private String model;
    private String color;

    public Car(String brand, int serialID, String model) {
        this.brand = brand;
        this.serialID = serialID;
        this.model = model;
        this.color = "Not specified";
    }

    public int getSerialID() {
        return serialID;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public void setColor(String color){
        this.color = color;
    }
}
