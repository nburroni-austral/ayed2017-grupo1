package main.tp03_Stack.ParkingLot;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 25/03/2017.
 */
public class CarNotFoundExcep extends Throwable {
    public CarNotFoundExcep(String message){
        super(message);
    }
}
