package main.tp03_Stack.ParkingLot;

import struct.impl.stacks.StaticStack;

import java.util.ArrayList;

/**
 * ParkingLot
 *
 * A StaticStack-based representation of a parking lot.
 *
 * @author Nicolás Gargano <nicolas.gargano @ ing.austral.edu.ar>, Lautaro Paskevicius <lautaro.paskevicius @ ing.austral.edu.ar>
 */
public class ParkingLot {

    private double tarifa;
    private StaticStack<Car> parkedCars;
    private ArrayList<Integer> parkedSerials; //This serves as an easier way to check for a Car before moving other cars.
    private StaticStack<Car> auxiliarLot;
    private double recaudation;

    public ParkingLot(){
        this.tarifa = 5.0;
        this.parkedCars = new StaticStack<>(50);
    }

    public void park(Car car){
        parkedCars.push(car);
        parkedSerials.add(car.getSerialID());
    }

    public void retire(Car car) throws CarNotFoundExcep{
        if(parkedSerials.contains(car.getSerialID())){  //Check if car is in there before doing complex op.
            while( !car.equals(parkedCars.peek()) ) {   //Pop to find car
                auxiliarLot.push(parkedCars.peek());
                parkedCars.pop();
            }

            parkedCars.pop();                           //Retire car

            while( !auxiliarLot.isEmpty() ){            //Put back cars
                parkedCars.push(auxiliarLot.peek());
                auxiliarLot.pop();
            }

            recaudation =+ tarifa;
        }

        throw new CarNotFoundExcep("Car is not parked in this lot.");
    }

    public double getRecaudation(){
        return recaudation;
    }
}
