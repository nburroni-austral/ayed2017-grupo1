\documentclass[12pt]{article}
\usepackage[english]{babel}
\usepackage{url}
\usepackage[utf8x]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\graphicspath{{images/}}
\edef\restoreparindent{\parindent=\the\parindent\relax}
\usepackage{parskip}
\restoreparindent
\usepackage{fancyhdr}
\usepackage{vmargin}
\usepackage{color}
\definecolor{pblue}{rgb}{0.13,0.13,1}
\definecolor{pgreen}{rgb}{0,0.5,0}
\definecolor{pred}{rgb}{0.9,0,0}
\definecolor{pgrey}{rgb}{0.46,0.45,0.48}
\usepackage{listings}
\lstset{language=Java,
  showspaces=false,
  showtabs=false,
  breaklines=true,
  showstringspaces=false,
  breakatwhitespace=true,
  commentstyle=\color{pgreen},
  keywordstyle=\color{pblue},
  stringstyle=\color{pred},
  basicstyle=\ttfamily,
  moredelim=[il][\textcolor{pgrey}]{$$},
  moredelim=[is][\textcolor{pgrey}]{\%\%}{\%\%}
}
\usepackage{ragged2e}
\setmarginsrb{3 cm}{2.5 cm}{3 cm}{2.5 cm}{1 cm}{1.5 cm}{1 cm}{1.5 cm}
\setlength\parindent{12pt}

\title{Design by Contracts}				% Title
\author{}								% Author
\date{15 Mar 2017}						% Date

\makeatletter
\let\thetitle\@title
\let\theauthor\@author
\let\thedate\@date
\makeatother

\pagestyle{fancy}
\fancyhf{}
\rhead{\theauthor}
\lhead{\thetitle}
\cfoot{\thepage}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{titlepage}
\centering    \vspace*{0.5 cm}
\includegraphics{austral}\\[1.0 cm]	% University Logo
\textsc{\LARGE Facultad de Ingeniería}\\[1.0 cm]
\textsc{\Large Grupo 1}\\[0.5 cm]
\rule{\linewidth}{0.2 mm} \\[0.4 cm]
{ \huge \bfseries \thetitle}\\
\rule{\linewidth}{0.2 mm} \\[1 cm]

\begin{minipage}{0.4\textwidth}
\begin{center} \large
\emph{Integrantes:}\\
			Nicolás Gargano\\
            Lautaro Paskevicius\\
\end{center}
\end{minipage} \\[1 cm]

\end{titlepage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\tableofcontents
\pagebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Definition}

\begin{justify}
Design by Contracts is an aproach to designing software. The central idea is a metaphor which mimics business life, where a "client" and a "supplier" agree on a "contract" that governs their behavior in the interaction.

As an example:

\begin{itemize}
\item The supplier must provide a certain product (obligation) and is entitled to expect that the client has paid its fee (benefit).
\item The client must pay the fee (obligation) and is entitled to get the product (benefit).
\item Both parties must satisfy certain obligations, such as laws and regulations, applying to all contracts.
\end{itemize}

Similarly, when talking about routines in software, they may:

\begin{itemize}
\item Expect a certain condition to be guaranteed on entry by any client module that calls it: the routine's precondition — an obligation for the client, and a benefit for the supplier (the routine itself), as it frees it from having to handle cases outside of the precondition.
\item Guarantee a certain property on exit: the routine's postcondition — an obligation for the supplier, and obviously a benefit (the main benefit of calling the routine) for the client.
\item Maintain a certain property, assumed on entry and guaranteed on exit: the class invariant.
\end{itemize}
\end{justify}

\section{Utility}

\begin{justify}
The goal of designing by contracts is to improve software reliability by establishing what each piece expects and is expected to do in its interaction with others. The presence of such a specification, even if it does not fully guarantee correctness, helps to avoid surprises and is a good basis for systematic testing and debugging.
\end{justify}
\pagebreak

\section{Code Example}
\begin{lstlisting}
/**
 * Bike
 *
 * Simple representation of a bike
 *
 * @author Nicolas Gargano & Lautaro Paskevicius
 */

public class Bike implements Comparable<Bike>{

    private /*@ non_null @*/ BikeLight[] lights;
    private /*@ non_null @*/ double speed;
    private /*@ non_null @*/ int gears;
    private /*@ non_null @*/ int currentGear;
    private /*@ non_null @*/ int serialNumber;

    /*@
        invariant
        0 < currentGear && currentGear <= gears
        &&
        (\forall int i; 0 <= i && i < lights.length;
            lights[i].getLuminosity() > 500);
    @*/
    public Bike( /*@ non_null @*/ int gears, /*@ non_null @*/ int serialNumber, /*@ non_null @*/ BikeLight[] lights){
        this.gears = gears;
        this.serialNumber = serialNumber;
        this.currentGear = 1;
        this.lights = lights;
    }


    /*@
        requires currentGear < gears;
        assignable gears;
        ensures currentGear == \old(currentGear) - 1;
     @*/
    public void shiftGearUp() {
        currentGear++;
    }

    /*@
        requires currentGear > 1;
        assignable gears;
        ensures currentGear == \old(currentGear) + 1;
     @*/
    public void shiftGearDown() {
        currentGear--;
    }

    /*@
        requires amount > 0;
        assignable speed;
        ensures  speed == \old(speed) + amount;
    @*/
    public void augmentSpeed (double amount){
        this.speed += amount;
    }

    /*@
        requires amount <= speed && amount > 0 ;
        ensures speed == \old(speed) - amount;
     @*/
    public void decreaseSpeed(double amount) {
        this.speed -= amount;
    }

    /*@ ensures \result == speed; @*/
    public /*@ pure @*/ double getSpeed(){
        return this.speed;
    }

    /*@ ensures \result == currentGears; @*/
    public /*@ pure @*/ double getCurrentGear(){
        return this.currentGear;
    }

    /*@ ensures \result == gears; @*/
    public /*@ pure @*/ int getGears() {
        return gears;
    }

    /*@
        requires speed >= 0;
        assignable speed;
        ensures this.speed == speed;
     @*/
    public void setSpeed(double /*@ non_null @*/ speed) {
        /*@assert speed < 0; @*/
        this.speed = speed;
    }

    /*@ ensures gears == this.gears; @*/
    public void setGears(int /*@ non_null @*/ gears) {
        this.gears = gears;
    }

    /*@ ensures currentGear == this.currentGear; @*/
    public void setCurrentGear(int /*@ non_null @*/ currentGear) {
        assert 0 < currentGear && currentGear <= gears;
        this.currentGear = currentGear;
    }

    @Override
    public int compareTo(Comparable<Bike> x) {
        if(this.getGears() > ((Bike) x).getGears()) return 1;
        if(this.getGears() < ((Bike) x).getGears()) return -1;
        return 0;
    }
\end{lstlisting}
\pagebreak
\begin{lstlisting}
    /**
     * BikeLight
     *
     * Simple representation of a light.
     *
     * @author Nicolas Gargano & Lautaro Paskevicius
     */

    \pagebreak
    class BikeLight {

        // Measured in lumen units
        private double luminosity;
        /*@ invariant luminosity > 0 @*/

        public BikeLight( /*@ non_null @*/ double luminosity){
            //@ assert luminosity > 0;
            this.luminosity = luminosity;
        }

        /*@ ensures \result == luminosity; @*/
        public /*@ pure @*/ double getLuminosity() {
            return luminosity;
        }

        /*@
            requires luminosity > 0;
            ensures luminosity == this.luminosity;
        @*/
        public void setLuminosity(double luminosity) {
            this.luminosity = luminosity;
        }
}
}



\end{lstlisting}

\end{document}