package main.tp02_JML;

import struct.istruct.Comparable;

/**
 * Bike
 *
 * Simple representation of a bike
 *
 * @author Nicolas Gargano & Lautaro Paskevicius
 */

public class Bike implements Comparable<Bike>{

    private /*@ non_null @*/ BikeLight[] lights;
    private /*@ non_null @*/ double speed;
    private /*@ non_null @*/ int gears;
    private /*@ non_null @*/ int currentGear;
    private /*@ non_null @*/ int serialNumber;


    public Bike( /*@ non_null @*/ int gears, /*@ non_null @*/ int serialNumber){
        this.gears = gears;
        this.serialNumber = serialNumber;
        this.currentGear = 1;
        this.lights = new BikeLight[0];
    }

    /*@
        invariant
        0 < currentGear && currentGear <= gears
        &&
        (\forall int i; 0 <= i && i < lights.length;
            lights[i].getLuminosity() > 500);
    @*/
    public Bike( /*@ non_null @*/ int gears, /*@ non_null @*/ int serialNumber, /*@ non_null @*/ BikeLight[] lights){
        this.gears = gears;
        this.serialNumber = serialNumber;
        this.currentGear = 1;
        this.lights = lights;
    }


    /*@
        requires currentGear < gears;
        assignable gears;
        ensures currentGear == \old(currentGear) - 1;
     @*/
    public void shiftGearUp() {
        currentGear++;
    }

    /*@
        requires currentGear > 1;
        assignable gears;
        ensures currentGear == \old(currentGear) + 1;
     @*/
    public void shiftGearDown() {
        currentGear--;
    }

    /*@
        requires amount > 0;
        assignable speed;
        ensures  speed == \old(speed) + amount;
    @*/
    public void augmentSpeed (double amount){
        this.speed += amount;
    }

    /*@
        requires amount <= speed && amount > 0 ;
        ensures speed == \old(speed) - amount;
     @*/
    public void decreaseSpeed(double amount) {
        this.speed -= amount;
    }

    /*@ ensures \result == speed; @*/
    public /*@ pure @*/ double getSpeed(){
        return this.speed;
    }

    /*@ ensures \result == currentGears; @*/
    public /*@ pure @*/ double getCurrentGear(){
        return this.currentGear;
    }

    /*@ ensures \result == gears; @*/
    public /*@ pure @*/ int getGears() {
        return gears;
    }

    /*@
        requires speed >= 0;
        assignable speed;
        ensures this.speed == speed;
     @*/
    public void setSpeed(double /*@ non_null @*/ speed) {
        /*@assert speed < 0; @*/
        this.speed = speed;
    }

    /*@ ensures gears == this.gears; @*/
    public void setGears(int /*@ non_null @*/ gears) {
        this.gears = gears;
    }

    /*@ ensures currentGear == this.currentGear; @*/
    public void setCurrentGear(int /*@ non_null @*/ currentGear) {
        assert 0 < currentGear && currentGear <= gears;
        this.currentGear = currentGear;
    }

    @Override
    public int compareTo(Comparable<Bike> x) {
        if(this.getGears() > ((Bike) x).getGears()) return 1;
        if(this.getGears() < ((Bike) x).getGears()) return -1;
        return 0;
    }

    /**
     * BikeLight
     *
     * Simple representation of a light.
     *
     * @author Nicolas Gargano & Lautaro Paskevicius
     */
    class BikeLight {

        // Measured in lumen units
        private double luminosity;
        /*@ invariant luminosity > 0 @*/

        public BikeLight( /*@ non_null @*/ double luminosity){
            //@ assert luminosity > 0;
            this.luminosity = luminosity;
        }

        /*@ ensures \result == luminosity; @*/
        public /*@ pure @*/ double getLuminosity() {
            return luminosity;
        }

        /*@
            requires luminosity > 0;
            ensures luminosity == this.luminosity;
        @*/
        public void setLuminosity(double luminosity) {
            this.luminosity = luminosity;
        }
    }
}
