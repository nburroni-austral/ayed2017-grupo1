package main.Utils.SwingUtil;

import java.awt.*;

/**
 * Util that calculates the appropriate scaling factor for pixel size based on a standard monitor resolution.
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */


public class ScalingUtil {

    // The vertical resolution standard for most monitors
    private static final int STD_HEIGHT = 1080;

    /**
     * Returns the scaling factor for the current system.
     * For example, a factor of 2 means that the size in pixels must be 2 times larger to keep a consistent size in the
     * current system.
     *
     * @return scaling factor
     */

    public static double getSystemScaling() {
        int height = Toolkit.getDefaultToolkit().getScreenSize().height;
        return ((double) height / STD_HEIGHT);
    }
}
