package main.Utils.SwingUtil;

import javax.swing.*;
import java.awt.*;

/**
 * Util that simplifies the creation of scaled swing layout components, like buttons and fillers.
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */

public class LayoutUtil {
    private final double SCALING;

    public LayoutUtil(double SCALING) {
        this.SCALING = SCALING;
    }

    /**
     * Creates a JButton with the specified size and text, scaled according to the SCALING factor set in the object.
     *
     * @param text of the button
     * @param width in pixels
     * @param height in pixels
     * @return a scaled button with the size and text entered
     */
    public JButton createButton(String text, int width, int height){
        int scaledWidth = (int) (width* SCALING);
        int scaledHeight = (int) (height* SCALING);
        JButton button = new JButton(text);
        button.setFont(new Font("Button",Font.PLAIN,(int) (height*0.4* SCALING)));
        Dimension buttonDimension = new Dimension(scaledWidth,scaledHeight);
        button.setMaximumSize(buttonDimension);
        button.setPreferredSize(buttonDimension);
        button.setMinimumSize(buttonDimension);
        button.setAlignmentX(Component.CENTER_ALIGNMENT);
        return button;
    }

    /**
     * Creates a swing filler with the specified size, scaled according to the SCALING factor set in the object.
     *
     * @param size of the filler
     * @return swing filler
     */

    public Box.Filler createFiller(int size){
        int scaledSize = (int) (size* SCALING);
        Dimension fillerSize = new Dimension(scaledSize, scaledSize);
        return new Box.Filler(fillerSize, fillerSize, fillerSize);
    }
}
