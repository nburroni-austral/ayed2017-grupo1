package main.tp_IntroSwing.views;

import main.tp_IntroSwing.controller.Controller;

import javax.swing.*;
import java.awt.*;

public class GameView extends JPanel{


    /*
        Creates a view with a picture of the game and a button that directs to the main menu
     */
    public GameView(Controller controller){
        setLayout(new BorderLayout());

        JButton backButton = new JButton("Back");
        backButton.addActionListener(controller.getGoToMainMenuListener());
        //System.out.println(System.getProperty("java.class.path"));
        JLabel gameImage = new JLabel(new ImageIcon("C:\\Users\\garba\\Documents\\ayed2017-grupo1\\src\\struct\\impl\\tp_IntroSwing\\assets\\game.png"));
        //Resources.class.getResource("/struct/impl/tp_IntroSwing/assets/game.png"
        add(gameImage, BorderLayout.CENTER);
        add(backButton,BorderLayout.PAGE_END);
    }
}
