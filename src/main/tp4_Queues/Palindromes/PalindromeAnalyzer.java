package main.tp4_Queues.Palindromes;

import struct.impl.queues.StaticQueue;
import struct.impl.stacks.DynamicStack;
import struct.istruct.Queue;
import struct.istruct.Stack;

/**
 * Class that analyzes words and tells if they are palindromes
 */
public class PalindromeAnalyzer {
    private Stack<Character> stack;
    private Queue<Character> queue;

    /**
     * Initializes a PalindromeAnalyzer with a stack and a queue
     */
    public PalindromeAnalyzer() {
        this.stack = new DynamicStack<>();
        this.queue = new StaticQueue<>();
    }

    /**
     * Checks if a string of characters is a palindrome.
     *
     * @param str String to check
     * @return true if it is a palindrome, false otherwise
     */
    public boolean isPalindrome(String str) {
        reset();
        stageFromString(str);
        return checkPalindrome();
    }

    /**
     * Prepares the queue and stack from a string
     * @param str phrase to analyze
     */
    private void stageFromString(String str) {
        for (char c : str.toLowerCase().toCharArray())
            if (c >= 'a' & c <= 'z') {
                stack.push(c);
                queue.enqueue(c);
            }
    }

    /**
     * Checks if current queue and stack contain a palindrome
     * @return true if it is a palindrome, false otherwise
     */
    private boolean checkPalindrome() {
        while (!stack.isEmpty())
            if (stack.peek() == queue.dequeue())
                stack.pop();
            else
                return false;

        return true;
    }

    /**
     * Reset stack and queue
     */
    private void reset() {
        stack.empty();
        queue.empty();
    }

}
