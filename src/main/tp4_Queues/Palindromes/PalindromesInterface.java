package main.tp4_Queues.Palindromes;

import struct.impl.util.mains.ConsoleInterface;

import java.util.Scanner;

/**
 * An interface that interacts with a palindrome analyzer
 */
public class PalindromesInterface extends ConsoleInterface {

    private PalindromeAnalyzer palindromeAnalyzer = new PalindromeAnalyzer();

    public PalindromesInterface() {
        super("Palindromes");
    }

    /**
     * Runs a simple console interface to interact with a palindrome analyzer
     */
    @Override
    public void runConsoleUI() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter line to be analyzed:");

        boolean isPalindrome = palindromeAnalyzer.isPalindrome(scanner.nextLine());
        System.out.println("Your line is " + (isPalindrome? "" : "not ") + "a palindrome.");

        System.out.println("Another round? (default yes, enter 'no' to exit)");
        String res = scanner.nextLine();

        if (res.equals("no"))
            System.exit(0);

        runConsoleUI();
    }
}
