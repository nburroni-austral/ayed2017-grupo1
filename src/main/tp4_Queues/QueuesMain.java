package main.tp4_Queues;

import main.tp4_Queues.Palindromes.PalindromesInterface;
import struct.impl.util.mains.ConsoleMainMenu;

import java.util.ArrayList;

public class QueuesMain extends ConsoleMainMenu {
    public QueuesMain() {
        super("tp4_Queues Main", new ArrayList<>());
        consoleInterfaces.add(new PalindromesInterface());
    }
}
