package main.tp_SoccerTable;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bomber on 4/20/17.
 */
public class SoccerTableMain {
    public static void main(String[] args) {
//        tp_SoccerTable soccerTable = new tp_SoccerTable();
//
//        long start = System.currentTimeMillis();
//        System.out.println(soccerTable.solve(0));
//        long end = System.currentTimeMillis();
//        soccerTable.printResults();

        List<String> lines = new ArrayList<String>() {};

        try {
            String path = new File("src/main/tp_SoccerTable/inputFile").getAbsolutePath();
            lines = Files.readAllLines(Paths.get(path));
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (String line : lines)
            System.out.println(line);

        SoccerTable soccerTable1 = new SoccerTable(lines);
        soccerTable1.solve(0);
        soccerTable1.printResults();
    }
}
