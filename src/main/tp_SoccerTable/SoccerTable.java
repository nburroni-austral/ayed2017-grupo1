package main.tp_SoccerTable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class SoccerTable {
    private HashMap<String, Team> teams;
    private LinkedList<Result> results;
    private ArrayList<Game> games;

    private int counter = 0;

    private final Result[] possibleResults = {
            new Result(0,3, '2'),
            new Result(3, 0, '1'),
            new Result(1, 1, 'X')
    };

    public SoccerTable() {
        teams = new HashMap<>();
        results = new LinkedList<>();

        teams.put("Deportivo", new Team("Deportivo", 11));
        teams.put("Betis", new Team("Betis", 9));
        teams.put("Sevilla", new Team("Sevilla", 6));
        teams.put("AtlMadrid", new Team("AtlMadrid", 6));
        teams.put("Barcelona", new Team("Barcelona", 5));
        teams.put("AtlBilbao", new Team("AtlBilbao", 4));
        teams.put("Madrid", new Team("Madrid", 2));
        teams.put("Espanyol", new Team("Espanyol", 2));
        teams.put("Valencia", new Team("Valencia", 1));
        teams.put("RealSociedad", new Team("RealSociedad", 1));

        games = new ArrayList<>();

        games.add(new Game(teams.get("Deportivo"), teams.get("RealSociedad")));
        games.add(new Game(teams.get("Barcelona"), teams.get("AtlMadrid")));
        games.add(new Game(teams.get("AtlBilbao"), teams.get("Espanyol")));
        games.add(new Game(teams.get("AtlMadrid"), teams.get("Madrid")));
        games.add(new Game(teams.get("Deportivo"), teams.get("Madrid")));
        games.add(new Game(teams.get("Betis"), teams.get("Deportivo")));
        games.add(new Game(teams.get("RealSociedad"), teams.get("Espanyol")));
        games.add(new Game(teams.get("Valencia"), teams.get("Deportivo")));
        games.add(new Game(teams.get("Deportivo"), teams.get("Barcelona")));
        games.add(new Game(teams.get("Madrid"), teams.get("Barcelona")));
        games.add(new Game(teams.get("Espanyol"), teams.get("Sevilla")));
        games.add(new Game(teams.get("Sevilla"), teams.get("AtlMadrid")));
        games.add(new Game(teams.get("Madrid"), teams.get("Betis")));
        games.add(new Game(teams.get("Valencia"), teams.get("AtlBilbao")));
        games.add(new Game(teams.get("Betis"), teams.get("AtlBilbao")));
        games.add(new Game(teams.get("Valencia"), teams.get("AtlMadrid")));
        games.add(new Game(teams.get("RealSociedad"), teams.get("Betis")));
        games.add(new Game(teams.get("Barcelona"), teams.get("Betis")));
    }

    public SoccerTable(List<String> lines) {
        teams = new HashMap<>();
        results = new LinkedList<>();
        games = new ArrayList<>();

        String[] indicators = lines.get(0).split(" ");
        int numberOfTeams = Integer.valueOf(indicators[0]);
        int numberOfGames = Integer.valueOf(indicators[1]);

        for (int i = 1; i<=numberOfTeams; i++) {
            String[] teamsLines = lines.get(i).split(" ");
            teams.put(teamsLines[0], new Team(teamsLines[0],  Integer.valueOf(teamsLines[1])));
        }

        for (int i = numberOfTeams+1; i<lines.size()-1; i++) {
            String[] gamesLines = lines.get(i).split(" ");
            games.add(new Game(teams.get(gamesLines[0]), teams.get(gamesLines[1])));
        }

    }



    public boolean solve(int index) {
        counter++;

        if (index == games.size()) {
            if (validScores())
                return true;
            return false;
        }

        for (Result r : possibleResults) {
            games.get(index).localTeam.currentScore += r.localTeamPoints;
            games.get(index).visitorTeam.currentScore += r.visitorTeamPoints;
            results.add(r);

            if (wentOver(games.get(index).localTeam) | wentOver(games.get(index).visitorTeam)) {
                backtrack(games.get(index), r, index);
                continue;
            }

            if (solve(index+1))
                return true;
            backtrack(games.get(index), r, index);
        }

        return false;
    }

    private void backtrack(Game game, Result result, int index) {
        for (int i=0; i<index; i++)
            System.out.print("\t");

        System.out.print(""
                + game.localTeam.name.substring(0,3).toUpperCase()
                + " vs. "
                + game.visitorTeam.name.substring(0,3).toUpperCase()
                + " not " + result.localTeamPoints + "-" + result.visitorTeamPoints
                + "\n");

        game.localTeam.currentScore -= result.localTeamPoints;
        game.visitorTeam.currentScore -= result.visitorTeamPoints;
        results.removeLast();
    }

    private boolean validScores() {
        for (Team team : teams.values())
            if (team.currentScore != team.targetScore)
                return false;

        return true;
    }

    private boolean wentOver(Team team) {
        return team.currentScore > team.targetScore;
    }

    public void printResults() {
        for (Result r : results)
            System.out.print(r.res + " ");
        System.out.println();
    }

    private class Result {
        int localTeamPoints;
        int visitorTeamPoints;
        char res;

        Result(int localTeamPoints, int visitorTeamPoints, char representation) {
            this.localTeamPoints = localTeamPoints;
            this.visitorTeamPoints = visitorTeamPoints;
            this.res = representation;
        }
    }

    private class Game {
        Team localTeam;
        Team visitorTeam;

        Game(Team a, Team b) {
            this.localTeam = a;
            this.visitorTeam = b;
        }
    }

    private class Team {
        String name;
        int currentScore;
        int targetScore;

        Team(String name, int targetScore) {
            this.name = name;
            this.currentScore = 0;
            this.targetScore = targetScore;
        }
    }

    public int getCounter() {
        return counter;
    }
}
