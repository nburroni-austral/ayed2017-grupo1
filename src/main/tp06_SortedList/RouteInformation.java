package main.tp06_SortedList;

public class RouteInformation {
    private int route;
    private int amountOfBusesWithDisabledSupport;
    private int amountOfBusesWithMoreThan27Seats;

    public RouteInformation(int route) {
        this.route = route;
        this.amountOfBusesWithDisabledSupport = 0;
        this.amountOfBusesWithMoreThan27Seats = 0;
    }

    public void incrementAmountOfBusesWithDisabledSupport() {
        this.amountOfBusesWithDisabledSupport++;
    }

    public void incrementAmountOfBusesWithMoreThan27Seats() {
        this.amountOfBusesWithMoreThan27Seats++;
    }

    public int getRoute() {
        return route;
    }

    public int getAmountOfBusesWithDisabledSupport() {
        return amountOfBusesWithDisabledSupport;
    }

    public int getAmountOfBusesWithMoreThan27Seats() {
        return amountOfBusesWithMoreThan27Seats;
    }
}
