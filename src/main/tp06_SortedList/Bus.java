package main.tp06_SortedList;

import java.io.Serializable;

/**
 * Class representing a bus.
 *
 * For the purposes of the city census we consider the bus to be a simple set of some of its properties.
 */
public class Bus implements Comparable<Bus>, Serializable {
    private int route;
    private int internalNumber;
    private int amountOfSeats;
    private boolean supportsDisabled;

    public Bus(int route, int internalNumber, int amountOfSeats, boolean supportsDisabled) {
        this.route = route;
        this.internalNumber = internalNumber;
        this.amountOfSeats = amountOfSeats;
        this.supportsDisabled = supportsDisabled;
    }

    /**
     * Order is determined first by route and second by internal number, with ascending order.
     * If they are both equal, the buses are considered to have the same precedence in order.
     *
     * @param other Bus to compare against
     * @return
     */
    @Override
    public int compareTo(Bus other) {
        if (route != other.getRoute())
            return route-other.getRoute();

        if (internalNumber != other.getInternalNumber())
            return internalNumber-other.getInternalNumber();

        return 0;
    }

    @Override
    public String toString() {
        return "Route: " + route + ", int: " + internalNumber;
    }

    // Getters and setters ---

    public int getRoute() {
        return route;
    }

    public void setRoute(int route) {
        this.route = route;
    }

    public int getInternalNumber() {
        return internalNumber;
    }

    public void setInternalNumber(int internalNumber) {
        this.internalNumber = internalNumber;
    }

    public int getAmountOfSeats() {
        return amountOfSeats;
    }

    public void setAmountOfSeats(int amountOfSeats) {
        this.amountOfSeats = amountOfSeats;
    }

    public boolean supportsDisabled() {
        return supportsDisabled;
    }

    public void setSupportsDisabled(boolean supportsDisabled) {
        this.supportsDisabled = supportsDisabled;
    }
}
