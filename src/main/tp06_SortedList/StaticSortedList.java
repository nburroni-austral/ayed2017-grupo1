package main.tp06_SortedList;

import struct.impl.lists.StaticList;
import struct.istruct.Comparable;
import struct.istruct.list.SortedList;

/**
 * Created by bomber on 5/15/17.
 */
public class StaticSortedList<T extends Comparable<? super T>> extends StaticList<T> implements SortedList<T>{

    @Override
    public void insert(T element) {

    }
}
