package main.tp06_SortedList;


import struct.impl.lists.DynamicSortedList;
import struct.impl.lists.DynamicList;
import struct.istruct.list.List;
import struct.istruct.list.SortedList;

import java.io.*;

/**
 * Represents a system capable of adding, removing and displaying data about buses
 */
public class BusSystem {
    private DynamicSortedList<Bus> busSortedList;

    public BusSystem() {
        this.busSortedList = new DynamicSortedList<>();
    }

    public void addBus(Bus bus) {
        busSortedList.insert(bus);
    }

    public void remove(int routeNumber, int internalNumber) {
        if (busSortedList.isVoid())
            return;

        busSortedList.goTo(0);

        if (busMatchesCredentials(busSortedList.getActual(), routeNumber, internalNumber))
            busSortedList.remove();

        while (!busSortedList.endList()) {
            busSortedList.goNext();
            if (busMatchesCredentials(busSortedList.getActual(), routeNumber, internalNumber)) {
                busSortedList.remove();
                return;
            }

        }
    }

    public void save() {
        try {
            FileOutputStream fileOut = new FileOutputStream("src/main/tp06_SortedList/BusDB");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);


            out.writeObject(busSortedList);

            out.close();
            fileOut.close();

            System.out.printf("Serialized data is saved in src/main/tp06_SortedList/BusDB");
        }catch(IOException i) {
            i.printStackTrace();
        }
    }

    public void load() {
        try {
            FileInputStream fileIn = new FileInputStream("src/main/tp06_SortedList/BusDB");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileIn);

            this.busSortedList = (DynamicSortedList<Bus>) objectInputStream.readObject();

            objectInputStream.close();
            fileIn.close();

        } catch(IOException i) {
            i.printStackTrace();
            return;
        }catch(ClassNotFoundException c) {
            System.out.println("SortedList class not found");
            c.printStackTrace();
            return;
        }
    }

    private boolean busMatchesCredentials(Bus bus, int routeNumber, int internalNumber) {
        return bus.getRoute() == routeNumber
                & bus.getInternalNumber() == internalNumber;
    }

    public SortedList<Bus> getBusSortedList() {
        return busSortedList;
    }

    public List<RouteInformation> getRoutesInformationList() {
        List<RouteInformation> routesInformationList = new DynamicList<>();
        SortedList<Bus> busesList = getBusSortedList();

        if (busesList.isVoid())
            return routesInformationList;

        busesList.goTo(0);
        RouteInformation routeInformation = new RouteInformation(busesList.getActual().getRoute());
        Bus currentBus = busesList.getActual();
        int lastRoute = busesList.getActual().getRoute();


        while (busesList.getActualPosition() < busesList.size()-1) {
            currentBus = busesList.getActual();
            if (currentBus.getRoute() != lastRoute) {
                lastRoute = currentBus.getRoute();
                routesInformationList.insertNext(routeInformation);
                routeInformation = new RouteInformation(currentBus.getRoute());
            }

            if (currentBus.supportsDisabled())
                routeInformation.incrementAmountOfBusesWithDisabledSupport();

            if (currentBus.getAmountOfSeats() > 27)
                routeInformation.incrementAmountOfBusesWithMoreThan27Seats();

            busesList.goNext();
        }

        currentBus = busesList.getActual();
        if (currentBus.getRoute() != lastRoute) {
            routesInformationList.insertNext(routeInformation);
            routeInformation = new RouteInformation(currentBus.getRoute());
        }

        if (currentBus.supportsDisabled())
            routeInformation.incrementAmountOfBusesWithDisabledSupport();

        if (currentBus.getAmountOfSeats() > 27)
            routeInformation.incrementAmountOfBusesWithMoreThan27Seats();

        routesInformationList.insertNext(routeInformation);
        return routesInformationList;
    }
}
