package main.tp06_SortedList;


import struct.istruct.list.List;
import struct.istruct.list.SortedList;

import java.util.Scanner;

public class BusSystemConsoleInterface {
    BusSystem system;
    Scanner scanner = new Scanner(System.in);
    Runnable[] runner = {
            this::addBusToSystem,
            this::removeBusFromSystem,
            this::listBuses,
            this::printAmountOfBusesWithDisabledSupportPerRoute,
            this::printAmountOfBusesWithMoreThan27SeatsPerRoute,
            this::save,
            this::load,
            this::exit,
    };

    public static void main (String[] args) {
        BusSystemConsoleInterface consoleInterface = new BusSystemConsoleInterface();
        consoleInterface.system.addBus(new Bus(11,11,11, true));
        consoleInterface.system.addBus(new Bus(11,11,11, false));
        consoleInterface.system.addBus(new Bus(13,11,11, false));
        consoleInterface.system.addBus(new Bus(13,11,11, true));
        consoleInterface.system.addBus(new Bus(14,11,11, false));
        consoleInterface.run();
    }

    public BusSystemConsoleInterface() {
        this.system = new BusSystem();
    }

    public void run() {
        promptAction();
    }

    private void promptAction() {
        System.out.println("What would you like to do?");
        System.out.println("1. Add bus");
        System.out.println("2. Remove bus");
        System.out.println("3. Print buses");
        System.out.println("4. Amount of buses with disabled support per route");
        System.out.println("5. Amount of buses with more than 27 seats per route");
        System.out.println("6. Save to file");
        System.out.println("7. Load from file");
        System.out.println("8. Exit");

        runner[(scanner.nextInt())-1].run();
        scanner.next();
        promptAction();
    }


    private void printAmountOfBusesWithDisabledSupportPerRoute() {
        List<RouteInformation> routeInformationList = this.system.getRoutesInformationList();
        routeInformationList.goTo(0);

        while (routeInformationList.getActualPosition() < routeInformationList.size()-1) {
            System.out.println("Route: " + routeInformationList.getActual().getRoute() +
                    ", Amount of buses that support disabled: " +  routeInformationList.getActual().getAmountOfBusesWithDisabledSupport());

            routeInformationList.goNext();
        }

        System.out.println("Route: " + routeInformationList.getActual().getRoute() +
                ", Amount of buses that support disabled: " +  routeInformationList.getActual().getAmountOfBusesWithDisabledSupport());
    }

    private void printAmountOfBusesWithMoreThan27SeatsPerRoute() {
        List<RouteInformation> routeInformationList = this.system.getRoutesInformationList();

        routeInformationList.goTo(0);

        while (routeInformationList.getActualPosition() < routeInformationList.size()-1) {
            System.out.println("Route: " + routeInformationList.getActual().getRoute() +
                    ", Amount of buses with more than 27 seats: " +  routeInformationList.getActual().getAmountOfBusesWithMoreThan27Seats());

            routeInformationList.goNext();
        }

        System.out.println("Route: " + routeInformationList.getActual().getRoute() +
                ", Amount of buses with more than 27 seats: " +  routeInformationList.getActual().getAmountOfBusesWithMoreThan27Seats());
    }

    private void addBusToSystem() {
        this.system.addBus(getBusFromUser());
    }

    private void removeBusFromSystem() {
        int routeNumber = getRouteNumberFromUser();
        int internalNumber = getInternalNumberFromUser();

        this.system.remove(routeNumber, internalNumber);
    }

    private void listBuses() {
        printLine('=');
        System.out.println("BUS LIST");
        printLine('=');
        SortedList<Bus> list = system.getBusSortedList();

        if (list.isVoid())
            return;

        list.goTo(0);
        printBusLine(list.getActual());
        int lastRouteListed = list.getActual().getRoute();

        while(list.getActualPosition() < list.size()-1) {
            list.goNext();
            if (lastRouteListed != list.getActual().getRoute()) {
                printLine('-');
                lastRouteListed = list.getActual().getRoute();
            }

            printBusLine(list.getActual());
        }
        printLine('=');
    }

    private void printLine(char c) {
        for (int i=0; i<56; i++)
            System.out.print(c);
        System.out.println("|");
    }
    private void printBusLine(Bus bus) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Route: " + bus.getRoute())
                .append("\t")
                .append("Number:" + bus.getInternalNumber())
                .append("\t")
                .append("Seats:" + bus.getAmountOfSeats())
                .append("\t")
                .append("Disabled Support:")
                .append(bus.supportsDisabled()? "yes" : "no")
                .append("\t")
                .append("|");

        System.out.println(stringBuilder);
    }

    private Bus getBusFromUser() {
        int routeNumber = getRouteNumberFromUser();
        int internalNumber = getInternalNumberFromUser();
        int amountOfSeats = getAmountOfSeatsFromUser();
        boolean supportsDisabled = askWhetherItSupportsDisabled();

        return new Bus(routeNumber, internalNumber, amountOfSeats, supportsDisabled);
    }

    private String getInputFromUser(String message) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(message + "\n");
        return scanner.next();
    }

    private int getRouteNumberFromUser() {
        return Integer.valueOf(getInputFromUser("Enter route number:"));
    }

    private int getInternalNumberFromUser() {
        return Integer.valueOf(getInputFromUser("Enter internal number:"));
    }

    private int getAmountOfSeatsFromUser() {
        return Integer.valueOf(getInputFromUser("Enter amount of seats:"));
    }

    private boolean askWhetherItSupportsDisabled() {
        char answer = getInputFromUser("Does it support disabled? (y/n | default no)").charAt(0);
        return answer == 'y';
    }

    private void save() {
        this.system.save();
    }

    private void load() {
        this.system.load();
    }

    private void exit() {
        System.exit(0);
    }
}
