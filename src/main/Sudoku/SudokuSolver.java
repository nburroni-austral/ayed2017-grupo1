package main.Sudoku;

import main.Sudoku.Exceptions.UnsolvableExc;
import struct.impl.stacks.DynamicStack;

import java.util.ArrayList;

/**
 * SudokuSolver solves the sudoku received in the constructor as a matrix.
 *
 * @author Manuel Pedrozo
 * @author Tomas Perez Molina
 */
public class SudokuSolver {
    private DynamicStack<DynamicStack<DynamicStack<Integer>>> mainStack;
    private boolean[][] mirror; //true for user inputted numbers.
    private int[][] matrix;
    private int  i,j;

    public SudokuSolver(int[][] matrix){
        this.matrix = matrix;
        makeMirror();
        mainStack = new DynamicStack<>();
        mainStack.push(new DynamicStack<>());
    }

    /**
     * Advances one step in the solution of the sudoku in the matrix array.
     */
    public boolean nextStep() throws UnsolvableExc{
        if(i >= 0 && i < matrix.length){
            if(j >= 0 && j < matrix[i].length){
                if(j == mainStack.peek().size()){
                    DynamicStack<Integer> colStack = getPosibleValues(i,j,matrix);
                    if(colStack.isEmpty()){
                        j--;
                    }
                    else{
                        mainStack.peek().push(colStack);
                        matrix[i][j] = colStack.peek();
                        j++;
                    }
                }
                else{
                    mainStack.peek().peek().pop();
                    if(mainStack.peek().peek().isEmpty()){
                        if(!mirror[i][j])
                            matrix[i][j] = 0;
                        mainStack.peek().pop();
                        j--;
                    }
                    else{
                        matrix[i][j] = mainStack.peek().peek().peek();
                        j++;
                    }
                }
            }
            else if(j > 0){
                mainStack.push(new DynamicStack<>());
                i++;
                j = 0;
            }
            else {
                j = matrix[i].length - 1;
                i--;
                mainStack.pop();
            }
            return true;
        }
        else if(i < 0) {
            throw new UnsolvableExc();
        }
        return false;
    }

    private void makeMirror(){
        mirror = new boolean[matrix.length][matrix[0].length];
        for(int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if(matrix[i][j] != 0) {
                    mirror[i][j] = true;
                    if (getPosibleValues(i, j, matrix).isEmpty()){
                        //throw new UnsolvableExc();
                    }
                }
            }
        }
    }

    private DynamicStack<Integer> getPosibleValues(int cellRow, int cellCol, int[][] matrix){

        DynamicStack<Integer> resultStack = new DynamicStack<>();

        ArrayList<Integer> possibleValues = new ArrayList<>();

        if(mirror[cellRow][cellCol])
            possibleValues.add(matrix[cellRow][cellCol]);
        else
            for(int i = 1; i <= matrix.length; i++){
                possibleValues.add(i);
            }

        ArrayList<Integer> invalidValues = new ArrayList<>();
        //Check column
        for(int row = 0; row < matrix.length; row++){
            int current = matrix[row][cellCol];
            if(current != 0 && row != cellRow) invalidValues.add(current);
        }

        //Check row
        for(int col = 0; col < matrix[cellRow].length; col++){
            int current = matrix[cellRow][col];
            if(current != 0 && col != cellCol) invalidValues.add(current);
        }

        int bigSquareSize = (int) Math.sqrt(matrix.length);
        int topLeftSqrRow = cellRow;
        int topLeftSqrCol = cellCol;
        while(topLeftSqrRow % bigSquareSize != 0){
            topLeftSqrRow--;
        }
        while (topLeftSqrCol % bigSquareSize != 0){
            topLeftSqrCol--;
        }

        //Check Square
        for(int row = topLeftSqrRow; row < topLeftSqrRow + bigSquareSize; row++) {
            for (int col = topLeftSqrCol; col < topLeftSqrCol + bigSquareSize; col++) {
                int current = matrix[row][col];
                if(current != 0 && row != cellRow && col != cellCol) invalidValues.add(current);
            }
        }

        possibleValues.removeAll(invalidValues);
        for (int i: possibleValues) {
            resultStack.push(i);
        }

        return resultStack;
    }

    public boolean[][] getMirror() {
        return mirror;
    }
}
