package main.Sudoku.Frames;

import main.Sudoku.SudokuGridPanel;
import main.Utils.SwingUtil.LayoutUtil;
import main.Utils.SwingUtil.ScalingUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * SudokuFrame draws a window that contains a sudoku grid, a "solve" button,
 * a "reset" button and a hidden error label.
 * @author Manuel Pedrozo
 * @author Tomas Perez Molina
 */

public class SudokuFrame extends JFrame{

    private JLabel errorLabel;
    private SudokuGridPanel sudokuGridPanel;

    public SudokuFrame(SudokuGridPanel sudokuGridPanel, ActionListener solveButtonAL, ActionListener resetButtonAL){
        super("Sudoku");
        double systemScaling = ScalingUtil.getSystemScaling();
        LayoutUtil boxUtil = new LayoutUtil(systemScaling);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));
        getContentPane().add(boxUtil.createFiller(30));

        JPanel middleContainer = new JPanel();
        middleContainer.setLayout(new BoxLayout(middleContainer, BoxLayout.Y_AXIS));
        middleContainer.add(boxUtil.createFiller(30));

        this.sudokuGridPanel = sudokuGridPanel;
        middleContainer.add(sudokuGridPanel);
        sudokuGridPanel.setVisible(true);

        middleContainer.add(boxUtil.createFiller(30));

        JPanel errorLabelContainer = new JPanel();
        errorLabelContainer.setLayout(new BoxLayout(errorLabelContainer,BoxLayout.X_AXIS));
        errorLabel = new JLabel("Unsolvable sudoku");
        errorLabel.setFont(new Font("Error",Font.PLAIN,(int) (15* systemScaling)));
        errorLabelContainer.add(errorLabel);
        hideErrorLabel();

        middleContainer.add(errorLabelContainer);
        middleContainer.add(boxUtil.createFiller(30));

        JButton solveButton = boxUtil.createButton("Solve", 100, 50);
        JButton resetButton = boxUtil.createButton("Reset", 100, 50);
        solveButton.addActionListener(solveButtonAL);
        resetButton.addActionListener(resetButtonAL);

        JPanel buttonContainer = new JPanel();
        buttonContainer.setLayout(new BoxLayout (buttonContainer, BoxLayout.X_AXIS));

        buttonContainer.add(resetButton);
        buttonContainer.add(boxUtil.createFiller(30));
        buttonContainer.add(solveButton);

        middleContainer.add(buttonContainer);
        middleContainer.add(boxUtil.createFiller(30));


        getContentPane().add(middleContainer);
        getContentPane().add(boxUtil.createFiller(30));


        setResizable(false);
        pack();
    }

    public void showSelf(){
        setVisible(true);
    }

    public void hideSelf(){
        setVisible(false);
    }

    public JFormattedTextField[][] getSudokuCells(){
        return this.sudokuGridPanel.getCells();
    }

    /**
     * Updates the grid to the received solution.
     * @param solution two dimensional array with the sudoku solution.
     */
    public void solve(int[][] solution, boolean[][] userInputted){
        sudokuGridPanel.solve(solution, userInputted);
    }

    /**
     * Resets the sudoku grid.
     */

    public void resetSudoku(){
        sudokuGridPanel.reset();
    }

    /**
     * Shows the error label.
     */

    public void showErrorLabel(){
        errorLabel.setForeground(Color.RED);
    }

    /**
     * Hides the error label.
     */

    public void hideErrorLabel(){
        errorLabel.setForeground(getBackground());
    }

}
