package main.Sudoku;

import main.Utils.SwingUtil.ScalingUtil;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.text.NumberFormat;

/**
 * Represents a sudoku grid.
 *
 * @author Manuel Pedrozo
 * @author Tomas Perez Molina
 */
public class SudokuGridPanel extends JPanel{
    private JFormattedTextField[][] cells;

    public SudokuGridPanel(int rows, int cols, int cellSize) {
        super();
        double systemScaling = ScalingUtil.getSystemScaling();
        int scaledCellSize = (int) (cellSize* systemScaling);
        int borderSize = (int) (5* systemScaling);
        setBorder(new EmptyBorder(borderSize, borderSize, borderSize, borderSize));
        int squareSize = (int) Math.sqrt(rows);
        setLayout(new GridLayout(squareSize,1, 0, borderSize));
        setBackground(Color.black);
        JPanel[][] squarePanels = new JPanel[squareSize][squareSize];
        for(int i = 0; i < squareSize; i++){
            JPanel currentRowPanel = new JPanel(new GridLayout(1, squareSize, borderSize, 0));
            for(int j = 0; j < squareSize; j++){
                JPanel currentSquarePanel = new JPanel(new GridLayout(squareSize,squareSize));
                currentRowPanel.add(currentSquarePanel);
                squarePanels[i][j] = currentSquarePanel;
            }
            currentRowPanel.setBackground(Color.black);
            add(currentRowPanel);
        }

        cells = new JFormattedTextField[rows][cols];

        Dimension prefSize = new Dimension(scaledCellSize, scaledCellSize);
        int scaledFont = (int) (50* systemScaling);
        Font font = new Font("SudokuOld",Font.BOLD, scaledFont);
        NumberFormatter nf = new NumberFormatter();
        nf.setFormat(NumberFormat.getInstance());
        nf.setMinimum(1);
        nf.setMaximum(rows);

        for(int row = 0; row < cells.length; row++){
            for(int col = 0; col < cells[row].length; col++){
                JFormattedTextField currentField = new JFormattedTextField(nf);
                currentField.setPreferredSize(prefSize);
                currentField.setFont(font);
                currentField.setDisabledTextColor(Color.black);
                currentField.setHorizontalAlignment(JTextField.CENTER);
                cells[row][col] = currentField;
                squarePanels[row/3][col/3].add(currentField);
            }
        }
    }

    /**
     * Resets the sudoku grid by setting every cells text to "".
     */
    public void reset(){
        for(int row = 0; row < cells.length; row++){
            for(int col = 0; col < cells[row].length; col++){
                cells[row][col].setEditable(true);
                cells[row][col].setText("");
                cells[row][col].setDisabledTextColor(Color.black);
            }
        }
    }

    public JFormattedTextField[][] getCells(){
        return cells;
    }

    /**
     * Updates the grid to the received solution.
     * @param solution two dimensional array with the sudoku solution.
     */
    public void solve(int[][] solution, boolean[][] userInputted){
        if(solution.length != cells.length || solution[0].length != cells.length)
            return;

        for(int row = 0; row < cells.length; row++){
            for(int col = 0; col < cells[row].length; col++){
                if(!userInputted[row][col]){
                    if(solution[row][col] == 0){
                        cells[row][col].setText("");
                    }
                    else{
                        cells[row][col].setText("" + solution[row][col]);
                        cells[row][col].setDisabledTextColor(Color.red);
                    }
                }
            }
        }
    }

    public void disableCells(){
        for(int i = 0; i < cells.length; i++){
            for(int j = 0; j < cells[i].length; j++){
                cells[i][j].setEnabled(false);
            }
        }
    }

    public void enableCells(){
        for(int i = 0; i < cells.length; i++){
            for(int j = 0; j < cells[i].length; j++){
                cells[i][j].setEnabled(true);
            }
        }
    }
}
