package main.Sudoku;

import main.Sudoku.Exceptions.InvalidFormatExc;
import main.tp_Sudoku.ImpossibleSudokuException;

import javax.swing.*;

/**
 * SudokuGridUtil has a method that parses a two dimensional JFormattedTextField array.
 *
 * @author Manuel Pedrozo
 * @author Tomas Perez Molina
 */
public class SudokuGridUtil {
    public SudokuGridUtil(){}

    /**
     * Parses the received two dimensional JFormattedTextField array to an int array.
     * @param cells JFormattedTextField two dimensional array to be parsed.
     * @return parsed array.
     */
    public int[][] parseToInt(JFormattedTextField[][] cells){
        int[][] result = new int[cells.length][cells[0].length];
        try {
            for (int i = 0; i < cells.length; i++) {
                for (int j = 0; j < cells[i].length; j++) {
                    String text = cells[i][j].getText();
                    if(!text.equals(""))
                        result[i][j] = Integer.parseInt(text);
                    else
                        result[i][j] = 0;
                }
            }
            return result;
        }
        catch (NumberFormatException exc){
            throw new ImpossibleSudokuException(); //InvalidFormat (exception that was in orig code) not same as ImpossibleSudoku (our implementation of Unsolvable); might fail
        }
    }

}
