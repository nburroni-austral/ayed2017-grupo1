package main.Sudoku;

import main.Sudoku.Exceptions.UnsolvableExc;
import main.Sudoku.Frames.SudokuFrame;
import main.tp_Sudoku.ImpossibleSudokuException;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Manages the functionality of SudokuFrame.
 *
 * @author Manuel Pedrozo
 * @author Tomas Perez Molina
 */

public class SudokuController {
    private SudokuFrame sudokuFrame;
    private SudokuSolver sudokuSolver;
    private SudokuGridUtil sudokuGridUtil;
    private SudokuGridPanel sudokuGridPanel;
    private Timer timer;

    public SudokuController() {
        sudokuGridPanel = new SudokuGridPanel(9, 9, 50);
        sudokuFrame = new SudokuFrame(sudokuGridPanel, new SolveButtonAL(), new ResetButtonAL());
        sudokuSolver = null;
        sudokuGridUtil = new SudokuGridUtil();
        sudokuFrame.showSelf();
    }

    /**
     * Solves the sudoku, updates the user interface.
     * Shows the error label if its unsolvable
     */

    private class SolveButtonAL implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            sudokuFrame.hideErrorLabel();
            try {
                if (timer.isRunning())
                    return;
            }catch (NullPointerException e){}

            int[][] parsedCells = sudokuGridUtil.parseToInt(sudokuFrame.getSudokuCells());

            ActionListener al = e -> {
                try {
                    if (sudokuSolver.nextStep()) {
                        sudokuFrame.solve(parsedCells, sudokuSolver.getMirror());
                    }else {
                        timer.stop();
                    }
                } catch (UnsolvableExc f){
                    System.out.println("Unsolvable sudoku!");
                    System.exit(1);
                }
            };

            try{
                sudokuSolver = new SudokuSolver(parsedCells);
                timer = new Timer(10, al);
                timer.setRepeats(true);
                timer.start();
                sudokuGridPanel.disableCells();
            }catch (ImpossibleSudokuException exc){
                sudokuFrame.showErrorLabel();
                System.out.println(exc.toString());
            }
        }
    }

    /**
     * Resets the sudoku grid.
     */

    private class ResetButtonAL implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            try{
                timer.stop();
            }
            catch (NullPointerException e){}
            sudokuFrame.resetSudoku();
            sudokuFrame.hideErrorLabel();
            sudokuGridPanel.enableCells();
        }
    }
}
