package main.tp07_BinaryTrees;

import struct.impl.trees.BinaryTree;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 08/04/2017.
 */
public class TreeAnalyzer {

    /**
     * <b>weight</b>
     *
     * Amount of descendants plus one.
     *
     * @param tree tree to operate with
     * @return an int representing the tree's weight.
     */
    public int weight (BinaryTree tree){

        if(tree.isEmpty()){
            return 0;
        }

        return 1 + weight(tree.getLeft()) + weight(tree.getRight());
    }

    /**
     * <b>leaves</b>
     *
     * Counts elements of a tree without children.
     *
     * @param tree tree to operate with.
     * @return
     */
    public int leaves(BinaryTree tree) {
        if(tree.isEmpty()){
            return 0;
        }

        if(tree.getRight().isEmpty() && tree.getLeft().isEmpty()) {
            return 1;
        }

        return leaves(tree.getRight()) + leaves(tree.getLeft());

    }

    /**
     * <b>timesPresent</b>
     *
     * Counts how many times a value is present in the tree. (Public caller for private method)
     *
     * @param tree tree to operate with.
     * @param value value to look for.
     * @param <T> Type of values handled in tree.
     * @return times value is present in tree.
     */
    public <T> int timesPresent(BinaryTree<T> tree, T value){
        return timesPresent(tree,value,0);
    }

    private <T> int timesPresent(BinaryTree<T> tree, T value, int count) {

        if(tree.isEmpty()){
            return count;
        }

        if(tree.getRoot().equals(value)){
            count++;
        }

        return count + timesPresent(tree.getLeft(),value,0) + timesPresent(tree.getRight(),value,0);
    }

    /**
     * <b>elementsAtLevel</b>
     *
     * Counts how many elements are present in a level (Public caller for private method).
     * (level of an element: length between the element and the root)
     *
     * @param tree tree to operate with.
     * @param level level of tree to count in.
     * @return number of elements present in level of tree.
     */
    public int elementsAtLevel(BinaryTree tree, int level){
        return elementsAtLevel(tree, level, 0);
    }

    private int elementsAtLevel(BinaryTree tree, int level, int count) {
        if(tree.isEmpty()) return 0;

        if(level != count){
            return elementsAtLevel(tree.getLeft(), level, 1+count) + elementsAtLevel(tree.getRight(),level,1+count);
        }

        return 1;
    }

    /**
     *<b>height</b>
     *
     * A tree's height is the length between it's root and the farthest leaf.(Public caller for private method)
     *
     * @param tree tree to operate with.
     * @return amount of edges conforming the path between root and leaf.
     */
    public int height(BinaryTree tree){
        return height(tree,-1);
    }

    private int height(BinaryTree tree, int currentHeight) {
        if(tree.isEmpty()) return currentHeight;

        currentHeight++;

        return Math.max(height(tree.getLeft(),currentHeight),height(tree.getRight(),currentHeight));
    }

    /**
     * <b>preOrder</b>
     *
     * Generates a string showing the values of a tree following:
     * root left right
     *
     * (Public caller for private method)
     *
     * @param tree tree to operate with
     * @param <T> type of values handled in tree passed
     * @return string representing ordered tree
     */
    public <T> String preOrder(BinaryTree<T> tree){
        return preOrder(tree, "");
    }

    private  <T> String preOrder(BinaryTree<T> tree, String order){
        if(tree.isEmpty()) return order;

        return tree.getRoot().toString() + " " + preOrder(tree.getLeft(),order)+  " "  + preOrder(tree.getRight(),order) ;
    }

    /**
     * <b>inOrder</b>
     *
     * Generates a string showing the values of a tree following:
     * left root right
     *
     * (Public caller for private method)
     *
     * @param tree tree to operate with
     * @param <T> type of values handled in tree passed
     * @return string representing ordered tree
     */
    public <T> String inOrder(BinaryTree<T> tree){
        return inOrder(tree, "");
    }

    private  <T> String inOrder(BinaryTree<T> tree, String order){
        if(tree.isEmpty()) return order;

        return inOrder(tree.getLeft(),order) + " " + tree.getRoot().toString() + " " + inOrder(tree.getRight(),order);
    }

    /**
     * <b>postOrder</b>
     *
     * Generates a string showing the values of a tree following:
     * left right root
     *
     * (Public caller for private method)
     *
     * @param tree tree to operate with
     * @param <T> type of values handled in tree passed
     * @return string representing ordered tree
     */
    public <T> String postOrder(BinaryTree<T> tree){
        return postOrder(tree, "");
    }

    private  <T> String postOrder(BinaryTree<T> tree, String order){
        if(tree.isEmpty()) return order;

        return preOrder(tree.getLeft(),order) + " " + preOrder(tree.getRight(),order) + " " + tree.getRoot().toString();
    }


}
