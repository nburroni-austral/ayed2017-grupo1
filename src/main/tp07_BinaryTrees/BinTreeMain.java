package main.tp07_BinaryTrees;

import struct.impl.trees.BinaryTree;
import main.tp07_BinaryTrees.TreeAnalyzer;
import main.tp07_BinaryTrees.TreeManager;

import java.util.Scanner;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 10/04/2017.
 */
public class BinTreeMain {

    public static void main(String[] args){
        BinaryTree<Integer> leaf1 = new BinaryTree<>(2);
        BinaryTree<Integer> leaf2 = new BinaryTree<>(5);
        BinaryTree<Integer> leaf3 = new BinaryTree<>(11);
        BinaryTree<Integer> leaf4 = new BinaryTree<>(4);
        BinaryTree<Integer> noLeaf = new BinaryTree<>();

        BinaryTree<Integer> subTree1 = new BinaryTree<>(6, leaf2 ,leaf3 );
        BinaryTree<Integer> subTree2 = new BinaryTree<>(9, leaf4 , noLeaf );
        BinaryTree<Integer> noSubtree = new BinaryTree<>();

        BinaryTree<Integer> tree1 = new BinaryTree<>(7,leaf1 ,subTree1 );
        BinaryTree<Integer> tree2 = new BinaryTree<>(5, noSubtree , subTree2 );

        BinaryTree<Integer> root = new BinaryTree<>(2, tree1 , tree2);

        TreeAnalyzer treeAnalyzer = new TreeAnalyzer();
        TreeManager treeManager = new TreeManager();
        Scanner scanner = new Scanner(System.in);

        while (true){

            System.out.println("TREE ANALYZER");

            System.out.println(" Predefined binary tree: \n");

            System.out.println(root.getRoot());
            System.out.println("| \\");
            System.out.println(tree1.getRoot() + "\t" + tree2.getRoot());
            System.out.println("| \t| \\");
            System.out.println(leaf1.getRoot() + "\t" + subTree1.getRoot() + "\t" + subTree2.getRoot());
            System.out.println("\t| \\  |");
            System.out.println("\t" + leaf2.getRoot() + " " + leaf3.getRoot() + " " + leaf4.getRoot());

            System.out.println("");

            System.out.println("1. Weight");
            System.out.println("2. Leaves");
            System.out.println("3. Get how many times a value shows up");
            System.out.println("4. Get how many elements are in a certain level");
            System.out.println("5. Height");
            System.out.println("6. Save tree");
            System.out.println("7. Read from path");
            System.out.println("8. Print pre-order");
            System.out.println("9. Print in-order");
            System.out.println("10. Print post-order");
            System.out.println("11. Exit");

            System.out.println("Type choice: ");

            int c = scanner.nextInt();

            switch (c){
                case 1:
                    System.out.println("Tree weight: " + treeAnalyzer.weight(root));
                    break;
                case 2:
                    System.out.println("Tree leaves: " + treeAnalyzer.leaves(root));
                    break;
                case 3:
                    System.out.println("Enter the value to look for: ");
                    int value = scanner.nextInt();
                    System.out.println("Times value " + value + " appears : " + treeAnalyzer.timesPresent(root,value));
                    break;
                case 4:
                    System.out.print("Enter the level to look at: ");
                    int level = scanner.nextInt();
                    System.out.println("Amount of values at level " + level + ": " + treeAnalyzer.elementsAtLevel(root,level));
                    break;
                case 5:
                    System.out.println("Tree height: " + treeAnalyzer.height(root));
                    break;
                case 6:
                    System.out.print("Enter file name: ");

                    String name = scanner.next();

                    treeManager.save(root, name);

                    System.out.println("Saved at .jar path!");
                    break;
                case 7:
                    System.out.println("Type BinaryTree path");
                    String path = scanner.next();

                    try{
                        BinaryTree read = treeManager.read(path);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
                case 8:
                    System.out.println("Pre - order: " + treeAnalyzer.preOrder(root));
                    break;
                case 9:
                    System.out.println("In - order: " + treeAnalyzer.inOrder(root));
                    break;
                case 10:
                    System.out.println("Post - order: " + treeAnalyzer.postOrder(root));
                    break;
                case 11:
                    System.exit(0);
                    break;
                default:
                    System.out.println("Not an option.");
                    break;
            }
        }

    }
}
