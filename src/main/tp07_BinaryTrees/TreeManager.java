package main.tp07_BinaryTrees;

import struct.impl.trees.BinaryTree;

import java.io.*;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 10/04/2017.
 */
public class TreeManager {

    /**
     * <b>save</b>
     *
     * Takes a binary tree and saves it in the path of the .jar file with the name passed.
     *
     * @param tree tree to save in disk
     * @param name name of archive to be created
     */
    public void save(BinaryTree tree, String name){

        FileOutputStream fileOutputStream;
        ObjectOutputStream objectOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(name);
            objectOutputStream = new ObjectOutputStream(fileOutputStream);

            objectOutputStream.writeObject(tree);
        } catch (Exception e){
            //none
        } finally {
            try{
                if(objectOutputStream != null) objectOutputStream.close();
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /**
     *
     * Reads a Binary tree from disk guided by the path specified
     *
     * @param path path to look for a BinaryTree
     * @return recovered BinaryTree
     */
    public BinaryTree read(String path){

        FileInputStream fileInputStream;
        ObjectInputStream objectInputStream = null;

        try{

            fileInputStream = new FileInputStream(path);
            objectInputStream = new ObjectInputStream( fileInputStream );

            BinaryTree tree = (BinaryTree) objectInputStream.readObject();

            return tree;

        } catch( Exception e ){
            e.printStackTrace();
        } finally {
            try{
                if( objectInputStream != null ) objectInputStream.close();
            }catch( Exception e ){
                e.printStackTrace();
            }
        }

        return null;
    }
}
