package main.tp05_Lists;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 19/04/2017.
 */
public class Client {
    private LocalTime entry;

    public Client(){
        this.entry = LocalTime.now();
    }

    public long getWaited(){
        return ChronoUnit.SECONDS.between(entry, LocalTime.now());
    }
}
