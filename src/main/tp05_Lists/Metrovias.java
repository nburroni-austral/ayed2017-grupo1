package main.tp05_Lists;

import struct.impl.lists.StaticList;

import java.util.Random;


/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 19/04/2017.
 */
public class Metrovias {

    StaticList<Window> windows;
    int cicles;
    Random random = new Random();

    public Metrovias(int openingTime, int closingTime, int cicleLapse, int cashiers){
        this.windows = new StaticList<>(cashiers);

        cicles = (closingTime - openingTime) * 3600 / cicleLapse;

        for(int i = 0; i < cashiers; i++){
            windows.goTo(i);
            windows.insertPrev(new Window(0.7));
        }
    }

    public void simulate(){
        for(int i = 0; i < cicles - 3; i++){
            Client client1 = new Client();
            Client client2 = new Client();
            Client client3 = new Client();
            Client client4 = new Client();
            Client client5 = new Client();

            assignClient(client1);
            assignClient(client2);
            assignClient(client3);
            assignClient(client4);
            assignClient(client5);

            for(int j = 0; j < windows.size(); j++){
                windows.getActual().serve();
            }
        }

        for(int i = 0; i < 3; i++){
            Client client1 = new Client();
            Client client2 = new Client();
            Client client3 = new Client();
            Client client4 = new Client();
            Client client5 = new Client();

            assignClient(client1);
            assignClient(client2);
            assignClient(client3);
            assignClient(client4);
            assignClient(client5);

            for(int j = 0; j < windows.size(); j++){
                windows.getActual().serveNow();
            }
        }

        for(int i = 0; i < windows.size(); i++){
            windows.goTo(i);

            System.out.println("Average time per client for window " + (i+1) + ": " + windows.getActual().getMediumTime());
            System.out.println("Total clients: " + windows.getActual().getTotalClients());
            System.out.println("Average idle time for window " + (i+1) + ": " + windows.getActual().getIdle());
            System.out.println("Takings for window " +(i+1) + ": " + windows.getActual().getCollected());
            System.out.println("------");
        }
    }

    private void assignClient(Client client){
        int chosenWindow = random.nextInt(windows.size() - 1);

        windows.goTo(chosenWindow);
        windows.getActual().enqueueClient(client);
    }
}
