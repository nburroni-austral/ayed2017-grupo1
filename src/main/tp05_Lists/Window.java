package main.tp05_Lists;

import struct.impl.queues.DynamicQueue;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 19/04/2017.
 */
public class Window {

    private DynamicQueue<Client> clients = new DynamicQueue<>();
    private double collected;
    private int totalClients;
    private long totalWaited;
    private long idle;
    private LocalTime lastFree;
    private double ticketCost;

    public Window(double ticketCost){
        this.collected = 0.0;
        this.totalClients = 0;
        this.totalWaited = 0;
        this.idle = 0;
        this.lastFree = LocalTime.now();
        this.ticketCost = ticketCost;
    }

    public void serve(){
        if(Math.random() <= 0.3){
            serveNow();
        }
    }

    public void serveNow(){

        if(!clients.isEmpty()){
            this.totalWaited = totalWaited + clients.dequeue().getWaited();
            this.totalClients++;
            this.collected = collected + ticketCost;
            this.idle = idle + ChronoUnit.SECONDS.between(lastFree, LocalTime.now());
            this.lastFree = LocalTime.now();
        }


    }

    public int getTotalClients(){
        return totalClients;
    }
    public void enqueueClient(Client client){
        this.clients.enqueue(client);
    }

    public double getMediumTime(){
        if(totalClients != 0) return totalWaited / totalClients;
        return -1;
    }

    public long getIdle(){
        return idle;
    }

    public  double getCollected(){
        return collected;
    }

}
