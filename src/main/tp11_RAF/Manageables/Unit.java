package main.tp11_RAF.Manageables;

/**
 * Abstract class representing any type of Object to be managed by FileManager
 *
 * @author Nicolas Gargano & Lautaro Paskevicius
 */

public abstract class Unit {
    protected boolean exists;
    protected int ID;

    public boolean exists(){
        return this.exists;
    }
    public int getID(){return this.ID;}
    void setExists(boolean state){
        this.exists = state;
    }
}
