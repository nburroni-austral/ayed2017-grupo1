package main.tp11_RAF.Manageables;

import java.io.IOException;

/**
 * File representation of a Product register. Extension is .prod
 *
 * @author Nicolas Gargano & Lautaro Paskevicius
 */
public class ProductFile extends UnitFile{

    public ProductFile(String pathname) throws IOException {
        super(pathname);
        UNIT_SIZE = 100 + 2 + 4 + 4 + 1; //Error en tamaño por writeUTF
    }

    @Override
    public void write(Unit t) throws IOException {
        Product p = (Product) t;

        raf.seek(raf.length()); //Place cursor at the end of the file
        String name = p.getName();

        while (name.length() <= 100){ //String has constant length of 100, thus readUTF always reads 100 chars
            name = name.concat(" ") ;
        }

        raf.writeUTF(name);
        raf.writeChar(p.getSection());
        raf.writeFloat(p.getPrice());
        raf.writeInt(p.getID());
        raf.writeBoolean(p.exists());
    }

    @Override
    public void write(Unit t, boolean writeActual) throws IOException {
        Product p = (Product) t;

        if(!writeActual){
            raf.seek(raf.length());
        }

        String name = p.getName();

        while (name.length() <= 100){ //String has constant length of 100, thus readUTF always reads 100 chars
            name = name.concat(" ") ;
        }

        raf.writeUTF(name);
        raf.writeChar(p.getSection());
        raf.writeFloat(p.getPrice());
        raf.writeInt(p.getID());
        raf.writeBoolean(p.exists());
    }

    @Override
    Product read() throws IOException {
        Product p = new Product(raf.readUTF(),raf.readChar(),raf.readFloat(),raf.readInt());
        p.setExists(raf.readBoolean());
        return p;
    }

    @Override
    public Product search(int ID) throws IOException {
        long cant = unitQty();
        seek(0);
        Product p;

        for (int i =0 ; i < cant;i++){
            p = read(); //Moves pointer
            if(p.exists() && (p.getID()==ID)) //Cuando busca no debería devolver los que no existen
                return p;
        }

        return new Product();
    }

    @Override
    public void update() throws IOException {
        ProductFile pf = new ProductFile("temp");
        for(int i = 0; i < unitQty(); i++){
            Product p = read();
            if(p.exists()){
                pf.write(p);
            }
        }
        String name = f.getName();
        this.delete();
        pf.close();
        pf.rename(name);
    }

}
