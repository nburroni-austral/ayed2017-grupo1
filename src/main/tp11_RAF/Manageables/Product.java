package main.tp11_RAF.Manageables;

/**
 * File representation of Song. Extension is .song
 *
 * @author Nicolas Gargano & Lautaro Paskevicius
 */
public class Product extends Unit{
    private String name;
    private char section;
    private float price;

    public Product() {
        this.exists = false;
    }

    public Product(String name, char section, float price, int ID) {
        this.name = name;
        this.section = section;
        this.price = price;
        this.ID = ID;
        this.exists = true;
    }

    public String getName() {
        return name;
    }

    public char getSection() {
        return section;
    }

    public float getPrice() {
        return price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSection(char section) {
        this.section = section;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
