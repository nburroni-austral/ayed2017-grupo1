package main.tp11_RAF.Manageables;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.function.Predicate;

/**
 * Abstract class representing a Unit register. Each type of UnitFile has it's unique file extension.
 *
 * @author Nicolas Gargano & Lautaro Paskevicius
 */

public abstract class UnitFile {
    File f;
    RandomAccessFile raf;

    /**
     * An int that indicates how many bits a Unit occupies on a file.
     */
    int UNIT_SIZE;

    public UnitFile(String pathname) throws IOException{
        this.f = new File(pathname);
        this.raf = new RandomAccessFile(f,"rw");
    }

    //Main methods

    /**
     * Takes a Unit and writes it's values in UnitFile
     *
     * @param t Unit to be written in file
     * @throws IOException common exception
     */
    abstract <T extends Unit> void write(T t) throws IOException;

    /**
     * Takes a Unit and writes it's values in UnitFile
     *
     * @param t Unit to be written in file
     * @param writeActual write at current position
     * @throws IOException common exception
     */
    abstract <T extends Unit> void write(T t, boolean writeActual) throws IOException;

    /**
     *  Retrieves information stored in memory to convert UnitFile into a Unit
     *
     * @return Unit read from memory
     * @throws IOException common exception
     */
    abstract Product read() throws IOException;

    /**
     * Closes RandomAccessFile buffer
     *
     * @throws IOException common exception
     */
    public void close() throws IOException {
        raf.close();
    }

    /**
     * Places RandomAccessFile cursor in a certain position
     *
     * @param pos index to place RAF cursor
     * @throws IOException common exception
     */
    public void seek(long pos) throws IOException {
        raf.seek(pos);
    }

    /**
     * Goes to specific Unit position
     *
     * @param unitNum unit number of registration
     * @throws IOException common exception
     */
    public void goTo(long unitNum) throws IOException{
        raf.seek((unitNum-1)*UNIT_SIZE);
    }

    /**
     * Reorganizes file deleting non-existing Units (logically deleted Units)
     *
     * @throws IOException common exception
     */
    public abstract void update() throws IOException;

    /**
     * Deletes UnitFile from memory
     *
     * @throws IOException common exception
     */
    public boolean delete() throws IOException{
        raf.close();
        return f.delete();
    }

    /**
     * Searches for a Unit and deletes if found
     *
     * @param ID unit ID
     * @return true if deleted, false if not found
     * @throws IOException common exception
     */
    public boolean delete(int ID) throws IOException {
        Unit s = search(ID);
        if(s.exists()){
            raf.seek(raf.getFilePointer() - UNIT_SIZE); //Mover el puntero hasta el inicio del objeto encontrado
            s.setExists(false); //Cambiar variable
            write(s,true); //Sobreescribir objeto
            return true;
        }
        return false;
    }

    /**
     * Renames UnitFile with the string passed
     * @param name new name
     * @throws IOException common exception
     */
    public boolean rename(String name) throws IOException{
        raf.close();
        File n = new File(name);
        return f.renameTo(n);
    }

    //Getters

    /**
     *  Counts how many bits are being used by file.
     *
     * @return bits occupied in memory by file
     * @throws IOException common exception
     */
    public long length() throws IOException {
        return raf.length();
    }

    /**
     * Counts how many Units are stored in a UnitFile
     *
     * @return how many units of data are in a UnitFile
     * @throws IOException common exception
     */
    public long unitQty() throws IOException {
        return raf.length() / UNIT_SIZE;
    }

    /**
     * Returns Unit if found match with unit ID
     * @param ID unit ID
     * @return unit found or null
     * @throws IOException common exception
     */
    public abstract Unit search(int ID) throws IOException;

    /**
     * Returns all elements of a UnitFile in a Unit array
     *
     * @return unit array
     */
    public ArrayList<Unit> asArray() throws IOException {
        ArrayList<Unit> listed = new ArrayList<>();

        seek(0);

        for(int i = 0; i < unitQty(); i++){
            Unit unit = read();
            if(unit.exists) listed.add(unit); //TODO
        }

        return listed;
    }

    /**
     * Given a Predicate, filter Units and put them in an array
     *
     * @param filter condition to filter by
     * @return all Units that pass the condition
     */
    public  ArrayList<Unit> filterFile(Predicate<Unit> filter) throws IOException {
        ArrayList<Unit> listed = new ArrayList<>();

        for(int i = 0; i < unitQty(); i++){
            Unit unit = read();
            if(filter.test(unit)) listed.add(unit);
        }

        return listed;
    }

    public void goToLast() throws IOException{
        raf.seek(raf.getFilePointer() - UNIT_SIZE);
    }
}
