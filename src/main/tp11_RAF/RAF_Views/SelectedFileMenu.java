package main.tp11_RAF.RAF_Views;

import main.tp11_RAF.Manageables.Product;
import main.tp11_RAF.Manageables.ProductFile;
import main.tp11_RAF.Manageables.Unit;
import main.tp11_RAF.Manageables.UnitFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.function.Predicate;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 09/07/2017.
 */
public class SelectedFileMenu {
    private ProductFile file;
    private Scanner scanner;

    public SelectedFileMenu(UnitFile unitFile){
        file = (ProductFile) unitFile;
        scanner = new Scanner(System.in);

        while (true){
            System.out.println();

            printAllProducts();

            System.out.println();

            System.out.println("\n -- RAF Editor -- ");
            System.out.println("1. Add product");
            System.out.println("2. Remove product");
            System.out.println("3. Edit product");
            System.out.println("4. Update memory");
            System.out.println("5. Information");
            System.out.println("6. Exit");
            System.out.print("Opt: ");
            int opt = scanner.nextInt();

            switch (opt){
                case 1:
                    System.out.println("\n -- Add product -- ");
                    System.out.print("Name: ");
                    String nameAdd = scanner.next();
                    System.out.print("Section: ");
                    char sectAdd = scanner.next(".").charAt(0);
                    System.out.print("Price: $");
                    float priceAdd = scanner.nextFloat();
                    System.out.print("ID: ");
                    int IDAdd = scanner.nextInt();

                    try {
                        file.write(new Product(nameAdd,sectAdd,priceAdd,IDAdd),false);
                    } catch (IOException e){
                        System.out.println("Couldn't add product");
                        break;
                    }
                    break;
                case 2:
                    System.out.println("\n -- Remove product -- ");
                    System.out.print("ID: ");
                    int IDRem = scanner.nextInt();
                    try {
                        if(!file.delete(IDRem)) System.out.println("Product not found");
                    }catch (IOException e){
                        System.out.println("Couldn't remove product");
                    }
                    break;
                case 3:
                    System.out.print("ID: ");
                    int IDEdit = scanner.nextInt();
                    try {
                        Product toEdit = file.search(IDEdit);
                        new ProdEditMenu(toEdit);
                    }catch (IOException e){
                        System.out.println("Couldn't edit product");
                    }
                    break;
                case 4:
                    try {
                        file.update();
                        break;
                    }catch (IOException e){
                        System.out.println("Couldn't update file");
                    }
                    break;
                case 5:
                    new FileInfoMenu(file);
                    break;
                case 6:
                    return;
                default:
                    System.out.println("Invalid option.");
                    break;
            }

        }
    }

    private void printAllProducts(){
        try {
            for(Object e : file.asArray()){
                Product g = (Product) e;
                System.out.print("\n Section: " + g.getSection());
                System.out.print("\t Price: " + g.getPrice());
                System.out.print("\t Exists: " + g.exists());
                System.out.print("\t ID: " + g.getID());
                System.out.print("\t Name: " + g.getName());
            }
        } catch (IOException f){
            f.printStackTrace();
        }
    }

    private class ProdEditMenu {
        Scanner scanner = new Scanner(System.in);

        public ProdEditMenu(Product toEdit) {
            while (true){
                System.out.print("\n Section: " + toEdit.getSection());
                System.out.print("\t Price: $" + toEdit.getPrice());
                System.out.print("\t Name: " + toEdit.getName());

                System.out.println("\n -- RAF Editor / "+ toEdit.getID() +" -- \n");
                System.out.println("1. Change name");
                System.out.println("2. Change section");
                System.out.println("3. Change price");
                System.out.println("4. Exit: ");
                System.out.print("Opt: ");
                int opt = scanner.nextInt();

                switch (opt){
                    case 1:
                        System.out.println("Old name: " + toEdit.getName());
                        System.out.print("New name");
                        String newName = scanner.next();
                        toEdit.setName(newName);
                        break;
                    case 2:
                        System.out.println("Old section: " + toEdit.getSection());
                        System.out.print("New section");
                        char newSection = scanner.next(".").charAt(0);
                        toEdit.setSection(newSection);
                        break;
                    case 3:
                        System.out.println("Old price: " + toEdit.getPrice());
                        System.out.print("New price");
                        int newPrice = scanner.nextInt();
                        toEdit.setPrice(newPrice);
                        break;
                    case 4:
                        try {
                            //Put pointer at beginning of product to edit
                            file.goToLast(); //En común con delete()
                            //Then write
                            file.write(toEdit, true);
                            return;
                        }catch (IOException e){
                            System.out.println("Couldn't edit product");
                        }
                    default:
                        System.out.println("Invalid option.");
                        break;
                }
            }
        }
    }

    private class FileInfoMenu {
        Scanner scanner = new Scanner(System.in);
        public FileInfoMenu(ProductFile file) {
            while (true){
                System.out.println("\n -- RAF Editor -- ");
                System.out.println("1. Price bigger than ...");
                System.out.println("2. Price lesser than ...");
                System.out.println("3. Name starting with ...");
                System.out.println("4. Exit");
                System.out.print("Opt: ");
                int opt = scanner.nextInt();
                switch (opt){
                    case 1:
                        System.out.println("\n Price bigger than: ");
                        float price = scanner.nextFloat();
                        Predicate<Unit> biggerThan = (g) -> {
                            Product p = (Product) g;
                            if(price <= p.getPrice()) return true;
                            return false;
                        };
                        try {
                            ArrayList<Unit> passed = file.filterFile(biggerThan);
                            for(Unit u : passed){
                                Product f = (Product) u;
                                System.out.println("Name: " + f.getName());
                                System.out.print("\t Section: " + f.getSection());
                                System.out.print("\t Price: " + f.getPrice());
                            }
                            break;
                        } catch (IOException e){
                            e.printStackTrace();
                            break;
                        }
                    case 2:
                        System.out.println("\n Price lesser than: ");
                        float price1 = scanner.nextFloat();
                        Predicate<Unit> lesserThan = (g) -> {
                            Product p = (Product) g;
                            if(price1 >= p.getPrice()) return true;
                            return false;
                        };
                        try {
                            ArrayList<Unit> passed = file.filterFile(lesserThan);
                            for(Unit u : passed){
                                Product f = (Product) u;
                                System.out.println("Name: " + f.getName());
                                System.out.print("\t Section: " + f.getSection());
                                System.out.print("\t Price: " + f.getPrice());
                            }
                            break;
                        } catch (IOException e){
                            e.printStackTrace();
                            break;
                        }
                    case 3:
                        System.out.println("\n Price bigger than: ");
                        char starts = scanner.next(".").charAt(0);
                        Predicate<Unit> startsWith = (g) -> {
                            Product p = (Product) g;
                            if(starts == p.getName().charAt(0)) return true;
                            return false;
                        };
                        try {
                            ArrayList<Unit> passed = file.filterFile(startsWith);
                            for(Unit u : passed){
                                Product f = (Product) u;
                                System.out.println("Name: " + f.getName());
                                System.out.print("\t Section: " + f.getSection());
                                System.out.print("\t Price: " + f.getPrice());
                            }
                            break;
                        } catch (IOException e){
                            e.printStackTrace();
                            break;
                        }
                    case 4:
                        return;
                    default:
                        System.out.println("Invalid option.");
                        break;
                }
            }
        }
    }
}
