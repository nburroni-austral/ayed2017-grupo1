package main.tp11_RAF.RAF_Views;

import main.tp11_RAF.FileAdmin;
import main.tp11_RAF.Manageables.UnitFile;
import main.tp11_RAF.RAF_Controller.RAFController;

import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 09/07/2017.
 */
public class MainMenu {
    RAFController controller;
    Scanner scanner;

    public MainMenu(){
        try {
            controller = new RAFController(new FileAdmin());
            scanner = new Scanner(System.in);
        } catch (IOException e){
            e.printStackTrace();
        }

        while (true){
            System.out.println(" -- RAF Editor -- \n");

            printAllFiles();

            System.out.println();

            System.out.println("1. Add File");
            System.out.println("2. Remove File");
            System.out.println("3. Select File");
            System.out.println("4. Exit");
            System.out.print("Opt: ");
            int opt = scanner.nextInt();

            switch (opt){
                case 1:
                    System.out.println(" -- Add File -- ");
                    System.out.print("Enter file name:");
                    String nameAdd = scanner.next();
                    controller.getFileAdmin().createFile(nameAdd + ".prod");
                    break;
                case 2:
                    System.out.println(" -- Remove File -- ");
                    System.out.print("Enter file name:");
                    String nameRem = scanner.next();
                    controller.getFileAdmin().deleteFile(nameRem + ".prod");
                    break;
                case 3:
                    System.out.println(" -- Select File -- ");
                    System.out.print("Enter file name:");
                    String nameSel = scanner.next();
                    try {
                        UnitFile selected = controller.getFileAdmin().selectFile(nameSel + ".prod");
                        new SelectedFileMenu(selected);
                    } catch (IOException e){
                        System.out.println("Couldn't select file.");
                        break;
                    }
                    break;
                case 4:
                    return;
                default:
                    System.out.println("Invalid option.");
                    break;
            }

        }
    }

    public void printAllFiles(){
        for(String e : controller.getFileNames()){
            System.out.println(e);
        }
    }
}
