package main.tp11_RAF.RAF_Controller;

import main.tp11_RAF.FileAdmin;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 09/07/2017.
 */
public class RAFController {
    private FileAdmin fileAdmin;

    public RAFController(FileAdmin fileAdmin) {
        this.fileAdmin = fileAdmin;
    }

    public FileAdmin getFileAdmin() {
        return fileAdmin;
    }

    public String[] getFileNames() {
        return fileAdmin.listFileNames();
    }
}
