package main.tp11_RAF;

import main.tp11_RAF.Manageables.out.ClientFile;
import main.tp11_RAF.Manageables.ProductFile;
import main.tp11_RAF.Manageables.out.SongFile;
import main.tp11_RAF.Manageables.UnitFile;

import java.io.File;
import java.io.IOException;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 20/06/2017.
 */
public class FileAdmin {

    private File dir;

    public FileAdmin() throws IOException{
        this.dir = new File("FileAdmin");
        if(!dir.exists() && !dir.mkdir()) throw new IOException(); //If it doesn't exists and couldn't convert it to a dir
    }

    public boolean createFile(String name){
        String extension = name.split("([.])")[1];
        String pathname = dir.getAbsolutePath()+ "\\" + name;
        switch (extension){
            case "prod":
                return addProd(pathname);
            /*case "song":
                return addSong(pathname);
            case "client":
                return addClient(pathname);*/
            default:
                return false;
        }
    }

    public boolean deleteFile(String name){
        try {
            String extension = name.split("([.])")[1];
                switch (extension){
                    case "prod":
                        ProductFile file = (ProductFile) selectFile(name);
                        return file.delete();
                    default:
                        return false;
                }
        } catch (IOException f){
            return false;
        }
    }

    public UnitFile selectFile(String name) throws IOException {
        String pathname = dir.getAbsolutePath() + "\\" + name;
        File toSelect = new File(pathname);
        if(toSelect.exists()){
            String extension = name.split("([.])")[1];
            switch (extension){
                case "prod":
                    return new ProductFile(pathname);
                /*case "song":
                    return new SongFile(pathname);
                case "client":
                    return new ClientFile(pathname);*/
                default:
                    return null;
            }
        }
        return null;
    }

    public String[] listFileNames(){
        File[] listOfFiles = dir.listFiles();

        String[] fileNames = new String[listOfFiles.length];

        for (int i = 0, j = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                fileNames[j] = listOfFiles[i].getName();
                j++;
            }
        }

        return fileNames;
    }

    private boolean addProd(String pathname){
        try {
            ProductFile pf = new ProductFile(pathname);
            return true;
        } catch (IOException e){
            return false;
        }
    }

    /*private boolean addSong(String pathname){
        try {
            SongFile sf = new SongFile(pathname);
            return true;
        } catch (IOException e){
            return false;
        }
    }

    private boolean addClient(String pathname){
        try {
            ClientFile cf = new ClientFile(pathname);
            return true;
        } catch (IOException e){
            return false;
        }
    }*/

}
