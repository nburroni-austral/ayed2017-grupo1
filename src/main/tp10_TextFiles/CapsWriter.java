package main.tp10_TextFiles;

import java.io.*;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 16/05/2017.
 */
public class CapsWriter {
    private BufferedReader br;
    private BufferedWriter bw;

    public File toUpperCase(File file){
        File upperCased = new File(".\\upperCased.txt");

        try {
            br = new BufferedReader(new FileReader(file));
            bw = new BufferedWriter(new FileWriter(upperCased));
        } catch (IOException e){
            e.printStackTrace();
        }

        try{
            int c;
            while ((c = br.read()) != -1){
                int charac = Character.toUpperCase(c);
                bw.write(charac);
                bw.flush();
            }
            bw.close();
        }catch (IOException e){
            e.printStackTrace();
        }

        return upperCased;
    }

    public File toLowerCase(File file){
        File lowerCased = new File(".\\lowerCased.txt");

        try {
            br = new BufferedReader(new FileReader(file));
            bw = new BufferedWriter(new FileWriter(lowerCased));
        } catch (IOException e){
            e.printStackTrace();
        }

        try{
            int c;
            while ((c = br.read()) != -1){
                int charac = Character.toLowerCase(c);
                bw.write(charac);
                bw.flush();
            }
            bw.close();
        }catch (IOException e){
            e.printStackTrace();
        }

        return lowerCased;

    }
}
