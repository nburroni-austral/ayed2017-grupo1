package main.tp10_TextFiles;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 16/05/2017.
 */
public class CustomReader {
    private BufferedReader reader;

    public CustomReader(String fileName) {
        try {
            this.reader = new BufferedReader(new FileReader(fileName));
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public String read(char type) throws IOException{
        if(type == 'C') {
            int read = readChar();
            if(read != -1) return Character.toString((char) read);
            return null;
        }
        if (type == 'L') return readLine();
        throw new IOException("CustomReader type not valid.");
    }

    public void close(){
        try {
            reader.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    private int readChar(){
        try {
            return reader.read();
        } catch(IOException e){
            e.printStackTrace();
            return -1;
        }
    }

    private String readLine(){
        try {
            return  reader.readLine();
        } catch (IOException e) {
            return null;
        }
    }


}
