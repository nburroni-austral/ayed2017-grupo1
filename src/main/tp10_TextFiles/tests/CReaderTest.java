package main.tp10_TextFiles.tests;

import main.tp10_TextFiles.CustomReader;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 16/05/2017.
 */
public class CReaderTest {
    public static void main(String[] args) {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter("CReaderTest", false));

            bw.write("This is a CustomReader test. \n");
            bw.write("This is a CustomReader test. \n");

            bw.close();
        } catch (IOException e){
            e.printStackTrace();
        }

        CustomReader customReader = new CustomReader("CReaderTest");

        String line;
        StringBuilder builder1 = new StringBuilder();

        try {
            while ((line = customReader.read('C')) != null) {
                builder1.append(line);
            }
        } catch (IOException e){
            e.printStackTrace();
        }

        customReader.close();

        System.out.println(builder1.toString() + "\nThis was a C-type reader test\n");

        CustomReader customReader2 = new CustomReader("CReaderTest");

        StringBuilder builder2 = new StringBuilder();

        try {
            while ((line = customReader2.read('L')) != null) {
                builder2.append(line + "\n");
            }
        } catch (IOException e){
            e.printStackTrace();
        }

        System.out.println(builder2.toString() + "\nThis was an L-type reader test");
    }
}
