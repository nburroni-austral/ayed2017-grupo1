package main.tp10_TextFiles.tests;

import main.tp10_TextFiles.CountryStats;

import java.io.File;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 26/05/2017.
 */
public class CStatsTest {
    public static void main(String[] args) {
        CountryStats countryStats = new CountryStats();
        File countries = new File(".\\CountriesFile");
        if (countries.exists()){
            countryStats.classify(countries);
            countryStats.classify(countries,40,"PBI");
        }
    }
}
