package main.tp10_TextFiles.tests;

import main.tp10_TextFiles.CapsWriter;

import java.io.File;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 21/05/2017.
 */
public class CapsWTest {
    public static void main(String[] args) {
        CapsWriter capsWriter = new CapsWriter();

        capsWriter.toLowerCase(new File(".\\commonCTest.txt"));
        capsWriter.toUpperCase(new File(".\\commonCTest.txt"));
    }
}
