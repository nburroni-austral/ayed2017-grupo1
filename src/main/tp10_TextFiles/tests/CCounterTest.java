package main.tp10_TextFiles.tests;

import main.tp10_TextFiles.CharCounter;

import java.io.File;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 16/05/2017.
 */
public class CCounterTest {
    public static void main(String[] args) {
        CharCounter charCounter = new CharCounter();
        int occ = charCounter.count('u', new File(".\\CCounterTest"));
        System.out.println(occ);
    }
}
