package main.tp10_TextFiles;

import java.io.*;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 26/05/2017.
 */
public class CountryStats {

    public void classify(File countries){
        /*
        * File format:
        * COUNTRY,#Population (in M),#PBI (in M)
        * */
        BufferedReader br;
        BufferedWriter bw1;
        BufferedWriter bw2;

        try{
            br = new BufferedReader(new FileReader(countries));
            bw1 = new BufferedWriter(new FileWriter(".\\Under30M.txt"));
            bw2 = new BufferedWriter(new FileWriter(".\\Over30M.txt"));

            StringBuilder builder1 = new StringBuilder();
            StringBuilder builder2 = new StringBuilder();
            String line;

            while (!(line = br.readLine()).equals("")){
                String[] partition = line.split("([,])+");

                if(Integer.parseInt(partition[1]) <= 30) bw1.write("\n" + line);
                else bw2.write("\n" + line);
            }

            bw1.close();
            bw2.close();
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public void classify(File countries, int point, String criteria){
        /*
        * File format:
        * COUNTRY,#Population (in M),#PBI (in M)
        * */
        BufferedReader br;
        BufferedWriter bw1;
        BufferedWriter bw2;

        if(!criteria.equals("PBI") && !criteria.equals("POB")) throw new RuntimeException("Criteria not valid");

        try{
            br = new BufferedReader(new FileReader(countries));
            bw1 = new BufferedWriter(new FileWriter(".\\Under"+point+"M.txt"));
            bw2 = new BufferedWriter(new FileWriter(".\\Over"+point+"M.txt"));

            String line;

            while (!(line = br.readLine()).equals("")){
                String[] partition = line.split("([,])+");

                if(Integer.parseInt(partition[1]) <= point) {
                    bw1.write("\n" + partition[0]);
                    if(criteria.equals("POB")) bw1.write("," + partition[1]);
                    else bw1.write("," + partition[2]);
                }
                else {
                    bw2.write("\n" + partition[0]);
                    if(criteria.equals("POB")) bw2.write("," + partition[1]);
                    else bw2.write("," + partition[2]);
                }
            }

            bw1.close();
            bw2.close();
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
