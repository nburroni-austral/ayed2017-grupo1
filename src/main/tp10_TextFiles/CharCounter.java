package main.tp10_TextFiles;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 16/05/2017.
 */
public class CharCounter {

    public int count(char find, File file){
        StringBuilder builder = new StringBuilder();

        try {
            BufferedReader bf = new BufferedReader(new FileReader(file));

            String line;
            while ((line = bf.readLine()) != null) {
                    builder.append(line);
            }

            String[] occurrences = builder.toString().split("([" + find + "])+");
            return occurrences.length - 1;
        } catch (IOException e){
            return -1;
        }
    }
}
