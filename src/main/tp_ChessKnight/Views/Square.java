package main.tp_ChessKnight.Views;
import javax.swing.*;
import java.awt.*;

public class Square extends JButton {
    Color color;

    public Square(Color color) {
        this.color = color;

        init();
    }

    public void init() {
        setBackground(color);
    }
}
