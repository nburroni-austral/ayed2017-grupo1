package main.tp_ChessKnight.Views;

import main.tp_ChessKnight.Controllers.BoardViewController;
import main.tp_ChessKnight.Model.Position;
import main.tp_IntroSwing.controller.Controller;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.geom.Path2D;
import java.awt.image.BufferedImage;
import java.util.LinkedList;

/**
 * Created by bomber on 4/3/17.
 */
public class BoardView extends JPanel {
    private BoardViewController controller;
    private Square[][] chessBoardSquares = new Square[8][8];
    // Colums labels
    private final String COLS = "ABCDEFGH";


    // Images TODO find image only for tp_ChessKnight?
    private Image[][] chessPieceImages = new Image[2][6];


    private Position[] positions;

    /**
     * Override the preferred capacity to return the largest it can, in
     * a square shape.  Must (must, must) be added to a GridBagLayout
     * as the only component (it uses the parent as a guide to capacity)
     * with no GridBagConstaint (so it is centered).
     */
    @Override
    public final Dimension getPreferredSize() {
        Dimension d = super.getPreferredSize();
        Dimension prefSize = null;
        Component c = getParent();
        if (c == null) {
            prefSize = new Dimension(
                    (int) d.getWidth(), (int) d.getHeight());
        } else if (c != null &&
                c.getWidth() > d.getWidth() &&
                c.getHeight() > d.getHeight()) {
            prefSize = c.getSize();
        } else {
            prefSize = d;
        }

        int w = (int) prefSize.getWidth();
        int h = (int) prefSize.getHeight();
        // The smallest of the two sizes
        int s = (w > h ? h : w);
        return new Dimension(s, s);
    }

    public void init(Color bg, Color sqColor1, Color sqColor2, Color borderColor) {
        setLayout(new GridLayout(0, 9));

        // Border
        setBorder(new CompoundBorder(
                new EmptyBorder(8, 8, 8, 8),
                new LineBorder(borderColor, 3)
        ));

        // Set the BG
        setBackground(bg);

        // Create the chess board squares
        Insets buttonMargin = new Insets(0, 0, 0, 0);
        for (int i = 0; i < chessBoardSquares.length; i++) {
            for (int j = 0; j < chessBoardSquares[i].length; j++) {

                // Alternate colors of squares
                Color sqColor;
                if ((j % 2 == 1 && i % 2 == 1) | (j % 2 == 0 && i % 2 == 0)) {
                    sqColor = sqColor1;
                } else {
                    sqColor = sqColor2;
                }
                Square sq = new Square(sqColor);
                sq.setMargin(buttonMargin);
                sq.setBorder(new LineBorder(borderColor));
                // Images are 64x64 icons, fill the rest with transparent icons of the same capacity
                sq.setIcon(new ImageIcon(new BufferedImage(64, 64, BufferedImage.TYPE_INT_ARGB)));

                chessBoardSquares[j][i] = sq;
            }
        }

        // Empty cell in the top left corner of the board
        add(new JLabel(""));

        // Fill the top row with column letters
        for (int i = 0; i < 8; i++) {
            add(new JLabel(COLS.substring(i, i + 1),
                    SwingConstants.CENTER));
        }

        // Fill other rows with row numbers on the first column
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                switch (j) {
                    case 0:
                        add(new JLabel("" + (9 - (i + 1)),
                                SwingConstants.CENTER));
                    default:
                        add(chessBoardSquares[j][i]);
                }
            }
        }
    }

    public BoardView(Controller controller) {
        init(Color.darkGray, Color.WHITE, Color.BLACK, Color.GRAY);
    }

    public BoardView(Color bg, Color sqColor1, Color sqColor2, Color borderColor, BoardViewController controller) {
        this.controller = controller;
        this.positions = controller.getPositionsToPaint();
        init(bg, sqColor1, sqColor2, borderColor);
    }

    public void emptySquare(int c, int r) {
        this.chessBoardSquares[c][r].setIcon(new ImageIcon(new BufferedImage(64, 64, BufferedImage.TYPE_INT_ARGB)));
    }


    private void drawConnections(Graphics2D g) {
        g.drawLine(50, 50, 100, 100);
    }


    private static class Line {
        final int x1;
        final int y1;
        final int x2;
        final int y2;

        public Line(int x1, int y1, int x2, int y2) {
            this.x1 = x1;
            this.y1 = y1;
            this.x2 = x2;
            this.y2 = y2;
        }
    }

    private final LinkedList<Line> lines = new LinkedList<>();

    public void addLine(int x1, int y1, int x2, int y2) {
        lines.add(new Line(x1, y1, x2, y2));
        repaint();
    }

    public void clearLines() {
        lines.clear();
        repaint();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setStroke(new BasicStroke(3));
        this.positions = controller.getPositionsToPaint();
        paintPath(g2d);
    }

    public void paintPath(Graphics2D g2d) {
        if (positions.length == 1)
            return;
        for (int i = 0; i < positions.length - 1; i++) {
            Position pos1 = positions[i];
            Position pos2 = positions[i + 1];
            int len = positions.length;

            float hue = 0.3f; //hue
            float saturation = 0.15f * (len - i); //saturation
            float brightness = 0.7f; //brightness

            Color myRGBColor = Color.getHSBColor(hue, saturation, brightness);
            g2d.setColor(myRGBColor);
            paintConnection(chessBoardSquares[pos1.getC()][7-pos1.getR()], chessBoardSquares[pos2.getC()][7-pos2.getR()], g2d);
        }


    }

    public void paintConnection(JComponent source, JComponent dest, Graphics2D g2d) {
        Path2D path = new Path2D.Double();
        path.moveTo(horizontalCenter(source), verticalCenter(source));
        path.curveTo(horizontalCenter(source), verticalCenter(dest),
                horizontalCenter(source), verticalCenter(dest),
                horizontalCenter(dest), verticalCenter(dest));
        g2d.draw(path);

        //        g2d.drawLine((int)horizontalCenter(source), (int)verticalCenter(source), (int)horizontalCenter(dest), (int)verticalCenter(dest));

    }

    protected double horizontalCenter(JComponent bounds) {

        return bounds.getX() + bounds.getWidth() / 2d;

    }

    protected double verticalCenter(JComponent bounds) {

        return bounds.getY() + bounds.getHeight() / 2d;
    }
}
