package main.tp_ChessKnight.old;

/**
 * Created by bomber on 4/5/17.
 */
public class Move {
    private int originCol;
    private int originRow;
    private int destCol;
    private int destRow;

    public Move(int originCol, int originRow, int destCol, int destRow) {
        this.originCol = originCol;
        this.originRow = originRow;
        this.destCol = destCol;
        this.destRow = destRow;
    }

    public Move getReverse() {
        return new Move(destCol, destRow, originCol, originRow);
    }

    public int getOriginCol() {
        return originCol;
    }

    public void setOriginCol(int originCol) {
        this.originCol = originCol;
    }

    public int getOriginRow() {
        return originRow;
    }

    public void setOriginRow(int originRow) {
        this.originRow = originRow;
    }

    public int getDestCol() {
        return destCol;
    }

    public void setDestCol(int destCol) {
        this.destCol = destCol;
    }

    public int getDestRow() {
        return destRow;
    }

    public void setDestRow(int destRow) {
        this.destRow = destRow;
    }
}
