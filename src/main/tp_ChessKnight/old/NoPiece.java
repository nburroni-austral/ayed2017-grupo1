package main.tp_ChessKnight.old;

import struct.impl.stacks.DynamicStack;
import struct.istruct.Stack;

public class NoPiece extends Piece {
    @Override
    public Stack<Move> getPossibleMovesStack() {
        return new DynamicStack<>();
    }
}
