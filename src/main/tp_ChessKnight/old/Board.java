package main.tp_ChessKnight.old;

public class Board {
    int size;
    Piece[][] squares;

    public Piece getPieceFromColRow(int col, int row) {
        return squares[col-1][row-1];
    }

    public Board (int size) {
        this.size = size;
        squares = new Piece[size][size];

        for (int c=0; c<size; c++) {
            for (int r=0; r<size; r++){
                squares[c][r] = new NoPiece();
            }
        }
    }

    public boolean isValidMove(Move move) {
        return isMoveDestinationWithinBoard(move);
    }

    public Piece removeAndGetPiece(int c, int r) {
        Piece piece = squares[c][r];
        squares[c][r] = new NoPiece();
        return piece;
    }

    public void putPiece(int c, int r, Piece piece) {
        squares[c][r] = piece;
    }

    public void doMove(Move move) {
        Piece piece = removeAndGetPiece(move.getOriginCol(), move.getOriginRow());
        piece.moveTo(move.getDestCol(), move.getDestRow());
        putPiece(move.getDestCol(), move.getDestRow(), piece);
    }

    public boolean isMoveDestinationWithinBoard(Move move) {
        int dc = move.getDestCol();
        int dr = move.getDestRow();

        boolean colIsWithinBoard = (0 <= dc && dc < size);
        boolean rowIsWithinBoard = (0 <= dr && dr < size);

        return colIsWithinBoard & rowIsWithinBoard;
    }
}
