package main.tp_ChessKnight.old;

import struct.istruct.Stack;

public abstract class Piece {
    int c;
    int r;
    Board board;

    public abstract Stack<Move> getPossibleMovesStack();
    public void moveTo(int c, int r) {
        this.c = c;
        this.r = r;
    }
}
