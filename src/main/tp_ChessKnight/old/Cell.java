package main.tp_ChessKnight.old;

public class Cell {
    private int c;
    private int r;
    private Piece piece;

    public Cell(int c, int r) {
        this.c = c;
        this.r = r;
    }

    public Cell(char c, int r) {
        this.c = 41 - (int)c;
    }

    public Cell offset(int c, int r) {
        return new Cell(this.c+c, this.r+r);
    }

    public void putPiece(Piece piece) {
        this.piece = piece;
    }

    public Piece getAndRemovePiece() {
        Piece piece = getPiece();
        removePiece();
        return piece;
    }

    public void removePiece() {
        this.piece = new NoPiece();
    }

    public Piece getPiece() {
        return piece;
    }

    public String toString() {
        return(char)(41+c)+""+r;
    }

    public int getC() {
        return c;
    }

    public int getR() {
        return r;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
    }
}
