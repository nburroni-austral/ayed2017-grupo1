package main.tp_ChessKnight.old;

import struct.impl.stacks.DynamicStack;
import struct.istruct.Stack;

public class Knight extends Piece {

    int[][] offsets = {
            // Top right
            { 1, 2}, { 2, 1},
            // Top left
            {-1, 2}, {-2, 1},
            // Bottom Left
            {-1,-2}, {-2,-1},
            // Bottom Right
            { 1,-2}, { 2,-1}
    };

    @Override
    public Stack<Move> getPossibleMovesStack() {
        Stack<Move> possibleMoves = new DynamicStack<>();

        for (int[] offset : offsets) {
            Move candidateMove = new Move(c, r,
                    this.c + offset[0], this.r + offset[1]);

            if(this.board.isValidMove(candidateMove))
                possibleMoves.push(candidateMove);
        }

        return possibleMoves;
    }

}
