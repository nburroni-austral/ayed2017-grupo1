package main.tp_ChessKnight.Model;

import struct.istruct.Stack;

public interface MovementPattern {
    public Stack<Position> getPossibleNextPositionsFromPosition(Position position);
}
