package main.tp_ChessKnight.Model;

import struct.impl.stacks.DynamicStack;
import struct.istruct.Stack;

public class KnightMovementPattern implements MovementPattern{

    int[][] offsets = {
            // Top right
            { 1, 2}, { 2, 1},
            // Top left
            {-1, 2}, {-2, 1},
            // Bottom Left
            {-1,-2}, {-2,-1},
            // Bottom Right
            { 1,-2}, { 2,-1}
    };

    @Override
    public Stack<Position> getPossibleNextPositionsFromPosition(Position position) {
        Stack<Position> possibleNextPositions = new DynamicStack<>();

        for (int[] offset : offsets) {
            Position candidatePosition = position.fromOffset(offset[0], offset[1]);

            if (isValidPosition(candidatePosition))
                possibleNextPositions.push(candidatePosition);
        }

        return possibleNextPositions;
    }

    private boolean isValidPosition(Position position) {
        if (0 <= position.getC() & position.getC() < 8
                & 0 <= position.getR() & position.getR() <8)
            return true;
        return false;
    }
}
