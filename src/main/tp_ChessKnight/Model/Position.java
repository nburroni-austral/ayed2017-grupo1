package main.tp_ChessKnight.Model;

/**
 * Created by bomber on 4/5/17.
 */
public class Position {
    private int c;
    private int r;

    public Position(int c, int r) {
        this.c = c;
        this.r = r;
    }

    public Position fromOffset(int offsetCols, int offsetRows) {
        return new Position(c + offsetCols, r + offsetRows);
    }

    public int getC() {
        return c;
    }

    public int getR() {
        return r;
    }

    public String toString() {
        return (char)(65+c)+""+(r+1)+"--"+c+""+r;
    }
}
