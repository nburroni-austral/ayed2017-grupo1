package main.tp_ChessKnight.Model;


import struct.istruct.Stack;

import java.util.ArrayList;
import java.util.LinkedList;

public class Simulation {
    private MovementPattern movementPattern;
    private LinkedList<Stack<Position>> listOfStacks;
    private Position startingPosition;
    private int maxMoves;
    private boolean isDone;

    public Simulation(Position startingPosition, MovementPattern movementPattern, int maxMoves) {
        this.listOfStacks = new LinkedList<>();
        this.movementPattern = movementPattern;
        this.startingPosition = startingPosition;
        this.maxMoves = maxMoves;
        this.isDone = false;

//        DynamicStack<Position> stack = new DynamicStack<>();
//        stack.push(startingPosition);
//        this.listOfStacks.push(stack);
        firstLoad();
    }

    private void firstLoad() {
        this.listOfStacks.push(movementPattern.getPossibleNextPositionsFromPosition(startingPosition));

        for (int i = 0; i < maxMoves - 1; i++) {
            listOfStacks.push(movementPattern.getPossibleNextPositionsFromPosition(listOfStacks.getFirst().peek()));
        }
    }

    public void doNext() {
        if (listOfStacks.isEmpty()) {
            // No more moves possible
            return;
        }

        if (listOfStacks.getFirst().isEmpty()) {
            this.isDone = true;
        }
        listOfStacks.getFirst().pop();
        makeSure();
    }

    public void makeSure() {
        if (!listOfStacks.getFirst().isEmpty()) {
            if (listOfStacks.size() == maxMoves) {
                return;
            }

            listOfStacks.push(movementPattern.getPossibleNextPositionsFromPosition(listOfStacks.getFirst().peek()));
            makeSure();
            return;
        }

        this.listOfStacks.pop();
        if (!listOfStacks.getFirst().isEmpty())
            this.listOfStacks.getFirst().pop();
        makeSure();
    }

    public Position[] getPositionsInCurrentPath() {
        Position[] arr = new Position[maxMoves];
        ArrayList<Position> positions = new ArrayList();


        for (Stack<Position> stack : listOfStacks)
            positions.add(stack.peek());
        positions.add(startingPosition);
        return positions.toArray(arr);
    }

    public boolean getIsDone() {
        return isDone;
    }
}
