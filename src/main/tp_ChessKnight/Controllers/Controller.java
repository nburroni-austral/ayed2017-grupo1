package main.tp_ChessKnight.Controllers;

import main.tp_ChessKnight.KnightGUI;
import main.tp_ChessKnight.Model.*;

import javax.swing.*;
import java.awt.event.ActionListener;

public class Controller implements KnightGUIController, BoardViewController, MenuController {
    private Simulation sim;
    private KnightGUI knightGUI;

    public Controller() {
        this.sim = new Simulation(new Position(0, 0), new KnightMovementPattern(), 4);
        this.knightGUI = new KnightGUI(this);
    }

    public void run() {
        Runnable r = () -> {
            System.out.println("Running...");
            KnightGUI cg = knightGUI;

            JFrame f = new JFrame("Movimientos del Caballo");
            f.add(cg);

            // Ensures JVM closes after frame(s) closed and all non-daemon threads are finished
            f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

            // See http://stackoverflow.com/a/7143398/418556 for demo.
            f.setLocationByPlatform(true);

            // Ensures the frame is the minimum capacity it needs to be in order display the components within it
            f.pack();

            // Ensures the minimum capacity is enforced.
            f.setMinimumSize(f.getSize());
            f.setVisible(true);
        };

        // Swing GUIs should be created and updated on the EDT
        // http://docs.oracle.com/javase/tutorial/uiswing/concurrency
        SwingUtilities.invokeLater(r);
    }

    @Override
    public ActionListener getOnNextListener() {
        return actionEvent -> {
            sim.doNext();
            knightGUI.getBoardView().repaint();
        };
    }

    @Override
    public ActionListener getOnAutoListener() {
        return actionEvent -> {
        };
    }

    @Override
    public ActionListener getOnStartListener() {
        return null;
    }

    @Override
    public ActionListener getOnExitListener() {
        return null;
    }

    @Override
    public Position[] getPositionsToPaint() {
        return sim.getPositionsInCurrentPath();
    }

    @Override
    public boolean getIsDone() {
        return sim.getIsDone();
    }
}
