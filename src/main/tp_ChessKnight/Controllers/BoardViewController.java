package main.tp_ChessKnight.Controllers;

import main.tp_ChessKnight.Model.Position;

public interface BoardViewController {
    Position[] getPositionsToPaint();
    boolean getIsDone();
}
