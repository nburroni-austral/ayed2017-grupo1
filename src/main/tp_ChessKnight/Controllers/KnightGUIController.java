package main.tp_ChessKnight.Controllers;

import java.awt.event.ActionListener;

public interface KnightGUIController {
    ActionListener getOnNextListener();
    ActionListener getOnAutoListener();
}
