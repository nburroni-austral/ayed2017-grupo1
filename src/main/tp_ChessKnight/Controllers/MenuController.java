package main.tp_ChessKnight.Controllers;

import java.awt.event.ActionListener;

public interface MenuController {
    ActionListener getOnStartListener();
    ActionListener getOnExitListener();
}
