package main.tp_ChessKnight;

import main.tp_ChessKnight.Controllers.Controller;
import main.tp_ChessKnight.Views.BoardView;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

public class KnightGUI extends JPanel {
    // Colors
    private final Color bg = new Color(39, 190, 196);
    private final Color cbcolor1 = new Color(8, 25, 30);
    private final Color cbcolor2 = new Color(33, 53, 56);
    private final Color borderColor = new Color(31, 115, 121);
//    private final Color borderColor = Color.RED;
    Controller controller;
    // Chess board
    private BoardView chessBoard;


    private final JLabel message = new JLabel("Movimientos del Caballo");

    public KnightGUI(Controller controller) {
        this.controller = controller;
        initializeGui();
    }

    public final void initializeGui() {
        setLayout(new BorderLayout(3, 3));
        JButton but = new JButton();
        // Set up main container
        setBorder(new EmptyBorder(5, 5, 5, 5));

        JToolBar tools = new JToolBar();
        tools.setFloatable(false);
        add(tools, BorderLayout.PAGE_END);

        JButton nextPathButton = new JButton("Next");
        nextPathButton.addActionListener(controller.getOnNextListener());

        JButton autoPathButton = new JButton("Auto");
        nextPathButton.addActionListener(controller.getOnAutoListener());

//        tools.addSeparator();
//        tools.add(autoPathButton);
//        tools.addSeparator();
//        tools.add(message);
//        add(new JLabel("Movements' Stacks Panel"), BorderLayout.LINE_START);

        chessBoard = new BoardView(bg, cbcolor1, cbcolor2, borderColor, controller);

        JPanel boardConstrain = new JPanel(new GridBagLayout());
        boardConstrain.setBackground(bg);
        boardConstrain.add(chessBoard);
        add(boardConstrain);
        tools.add(nextPathButton, JToolBar.CENTER);
    }

    public JPanel getBoardView() {
        return chessBoard;
    }




}