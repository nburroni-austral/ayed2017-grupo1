package main.tp_Sudoku;


import struct.impl.stacks.DynamicStack;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 04/04/2017.
 */
public class Board {

    private Cell[][] grid ;
    private int size;
    private boolean solved;

    public Board(int n){
        this.grid = new Cell[n][n];
        this.size = n;
        this.solved = false;

        for(int i = 0; i  < grid.length; i++){
            for(int j = 0; j < grid.length; j++){
                Cell emptyCell = new Cell(i,j);
                DynamicStack<Integer> empty = new DynamicStack<>(); //Empty or 0?
                emptyCell.setCandidateValues(empty);
                grid[i][j] = emptyCell;
            }
        }
    }

    public void addValue(int value, int r, int c){
        Cell cell = new Cell(r,c);
        cell.assignFixedValue(value);
        grid[r][c] = cell;
    }

    public boolean isSolved(){
        return solved;
    }

    public Cell getNextVariableCell(Cell cell){
        Cell nextCell = getNextCell(cell);

        while(!nextCell.isVariable()) {
            nextCell = getNextCell(nextCell);
        }

        return nextCell;
    }

    public void solve(){
        DynamicStack<Cell> history = new DynamicStack<>();
        solve(grid[0][0], history);
    }

    private void solve(Cell cell, DynamicStack<Cell> history) {

        Cell variableCell = getNextVariableCell(cell);

        if(getNextVariableCell(variableCell) == null){
            solved = true;
        } else {
            if(findCandidateValues(variableCell).isEmpty()){
                backtrack(history);
            } else {
                variableCell.setCandidateValues(findCandidateValues(variableCell));
                history.push(variableCell);
                solve(variableCell, history);
            }
        }
    }

    private void backtrack( DynamicStack<Cell> history) {
        history.pop();

        if(history.isEmpty()){
            throw new ImpossibleSudokuException();
        }

        Cell top = (Cell) history.peek();

        top.eliminateCandidate();

        if(top.ranOutOfCandidates()){
            backtrack(history);
        }

        solve(top, history);
    }

    public boolean isSafe(int possibleSolution, int r, int c){

        for(int i = 0; i < size; i++){
            if(grid[r][i].getValue() == possibleSolution) return false;        //is col safe
            if(grid[i][c].getValue() == possibleSolution) return false;        //is row safe
        }

        return isBoxSafe(possibleSolution,r,c);
    }

    public boolean isBoxSafe(int possibleSolution, int r, int c) {

        int gridSize = (int) Math.sqrt(size);

        int i = 0;
        int j = gridSize - 1;
        int k = 0;

        while(k < size){
            if( i <= r && r <= j){  // if the possibleSolution's row is between this row gap, then the row is this

                int l = 0;
                int m = gridSize - 1;

                while(m < grid.length){

                    if(l <= c && c <= m) { // if possibleSolution's col is between this col gap, then the box is this
                        for(int row = i; row < j ; row++){  // Once in the box, search from top
                            for (int col = l; col < m; col++ ){
                                if(grid[row][col].getValue() == possibleSolution) return false; //If found same value with different position, false;
                            }
                        }
                    }

                    l = l + gridSize; // Step to next box - init (cols)
                    m = m + gridSize; // Step to next box - end (cols)
                    k++;
                }

            }

            i = i + gridSize; // Step to next box - init (rows)
            j = j + gridSize; // Step to next box - end  (rows)
            k = k +3; // One row more steps 3 boxes
        }

        return true; //If not found any value in the box like the one passed, true;
    }

    private DynamicStack<Integer> findCandidateValues(Cell cell){
        DynamicStack<Integer> safeValues = new DynamicStack<>();

        for (int value = 1; value <= size; value++){ //Initialize possible values
            if(isSafe(value,cell.getRow(),cell.getCol())) safeValues.push(value);
        }

        return safeValues;
    }

    private Cell getNextCell(Cell cell){
        int row = cell.getRow();
        int col = cell.getCol();

        col++;
        if (col > 8){
            col = 0;
            row++;
        }
        return grid[row][col];
    }

    public void print(){
        for (int i = 0; i < grid.length; i++){
            for (int j = 0; j < grid.length; j++){
                System.out.print(grid[i][j].getValue());
            }
            System.out.println();
        }
    }

}
