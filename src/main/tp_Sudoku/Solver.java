package main.tp_Sudoku;

import struct.impl.stacks.DynamicStack;

import java.util.ArrayList;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 24/06/2017.
 */
public class Solver {
    private int[][] sudoku;
    private DynamicStack<DynamicStack<DynamicStack<Integer>>> solution;
    private boolean[][] boolMap;
    private int row;
    private int col;

    public Solver(int[][] sudoku) {
        this.sudoku = sudoku;
        this.solution = new DynamicStack<>();
        solution.push(new DynamicStack<>());
        this.boolMap = new boolean[sudoku.length][sudoku[0].length];

        for (int i = 0; i < sudoku.length; i++) { //Initialize boolMap
            for (int j = 0; i < sudoku[0].length; j++) {
                if (sudoku[i][j] != 0) this.boolMap[i][j] = true;
                if (findValues(i, j, sudoku).isEmpty()) throw new ImpossibleSudokuException();
            }
        }
    }

    /**
     * Step tracker for solving sudoku. Evaluates continuity and then performs step (if possible)
     *
     * @return true if possible to continue, else false
     */
    public boolean next() {
        if (row >= 0 && row < sudoku.length) { // Dentro de valores posibles de fila
            if (col >= 0 && col < sudoku[row].length) { // Dentro de valores posibles de columna
                if (col == solution.peek().size()) {
                    DynamicStack<Integer> colStack = findValues(row, col, sudoku); //Encontrar posibles valores
                    if (!colStack.isEmpty()) { //Si hay valores posibles, insertar valor
                        solution.peek().push(colStack); //Agregar posibles valores a la solucion
                        sudoku[row][col] = colStack.peek(); //Poner valor inmediato como posible solucion
                        col++; //Pasar a la siguiente columna
                    } else { //De no haber valores posibles, volver al anterior
                        col--; //Volver hacia atras
                    }
                } else {
                    solution.peek().peek().pop(); //Sacar valor posible
                    if (!solution.peek().peek().isEmpty()) { //Si hay otro valor posible, insertar
                        sudoku[row][col] = solution.peek().peek().peek();//Insertando
                        col++; //Pasar a la siguiente columna
                    } else {
                        if (!boolMap[row][col]){ //Si no es una posicion inalterable
                            sudoku[row][col] = 0; //Marcar como vacía
                            solution.peek().pop(); //Sacar stack vacío
                            col--; //Volver hacia atras
                        }
                    }
                }
            } else if (col <= 0) {
                col = sudoku[row].length - 1; //Ir hacia la posición previa
                row--; //Ir hacia la posición previa
                solution.pop(); //Sacar stack vacío
            } else {
                solution.push(new DynamicStack<>()); //Pasar a la siguiente posición
                row++; //Pasar a la siguiente posición
                col = 0; //Pasar a la siguiente posición
            }
            return true;
        } else if (row < 0) {
            throw new ImpossibleSudokuException();
        }
        return false;
    }

    /**
     * Find possible values for a position in sudoku grid
     * @param i row pos
     * @param j col pos
     * @param sudoku sudoku grid
     * @return stacked possible values
     */
    public DynamicStack<Integer> findValues(int i, int j, int[][] sudoku) {
        DynamicStack<Integer> result = new DynamicStack<>();

        ArrayList<Integer> foundValues = new ArrayList<>();

        if (boolMap[i][j]) {
            foundValues.add(sudoku[i][j]);
        } else {
            for (int e = 1; e <= sudoku.length; e++) {
                foundValues.add(e);
            }

            ArrayList<Integer> falseValues = new ArrayList<>();

            //Check row
            for (int col = 0; col < sudoku[i].length; col++) {
                int current = sudoku[i][col];
                if (current != 0 && col != j) falseValues.add(current);
            }

            //Check col
            for (int row = 0; row < sudoku.length; row++) {
                int current = sudoku[row][j];
                if (current != 0 && row != i) falseValues.add(current);
            }

            int gridSize = (int) Math.sqrt(sudoku.length);
            int rowPointer = i;
            int colPointer = j;

            while (rowPointer % gridSize != 0) {
                rowPointer--;
            }

            while (colPointer % gridSize != 0) {
                colPointer--;
            }

            //Check Square
            for (int row = rowPointer; row < rowPointer + gridSize; row++) {
                for (int col = colPointer; col < colPointer + gridSize; col++) {
                    int current = sudoku[row][col];
                    if (row != i && col != j && current != 0) falseValues.add(current);
                }
            }

            foundValues.removeAll(falseValues);

            for (int e : foundValues) {
                result.push(e);
            }
        }

        return result;
    }

    public boolean[][] getBoolMap(){
        return boolMap;
    }

    public int[][] getBoardValues() {
        return sudoku;
    }
}