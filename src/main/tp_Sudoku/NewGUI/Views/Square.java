package main.tp_Sudoku.NewGUI.Views;
import javax.swing.*;
import java.awt.*;

public class Square extends JButton {
    Color color;
    Integer value;

    public Square(Color color) {
        this.color = color;
        this.value = 0;

        init();
    }

    public Square(Color color, Integer value) {
        this.color = color;
        this.value = value;
        init();
    }

    public void init() {
        setBackground(color);
        if (value != 0)
            setText(value.toString());
    }

    public void setValue(Integer i) {
        if (i != 0)
            this.setText(i.toString());
        this.setText(" ");
    }

}
