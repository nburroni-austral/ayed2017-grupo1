package main.tp_Sudoku.NewGUI.Views;


import main.tp_Sudoku.NewGUI.Controller.SolverController;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.geom.Path2D;
import java.awt.image.BufferedImage;
import java.util.LinkedList;

/**
 * Created by bomber on 4/3/17.
 */
public class BoardView extends JPanel {
    private SolverController controller;
    private int sizeRoot;
    private int size;
    private Square[][] sudokuBoard;

    // Colums labels
    /**
     * Override the preferred capacity to return the largest it can, in
     * a square shape.  Must (must, must) be added to a GridBagLayout
     * as the only component (it uses the parent as a guide to capacity)
     * with no GridBagConstaint (so it is centered).
     */
    @Override
    public final Dimension getPreferredSize() {
        Dimension d = super.getPreferredSize();
        Dimension prefSize = null;
        Component c = getParent();
        if (c == null) {
            int a = 0; //TODO
            prefSize = new Dimension(
                    (int) d.getWidth(), (int) d.getHeight());
        } else if (c != null &&
                c.getWidth() > d.getWidth() &&
                c.getHeight() > d.getHeight()) {
            prefSize = c.getSize();
        } else {
            prefSize = d;
        }

        int w = (int) prefSize.getWidth();
        int h = (int) prefSize.getHeight();
        // The smallest of the two sizes
        int s = (w > h ? h : w);
        return new Dimension(s, s);
    }

    private void init(Color bg, Color sqColor1, Color sqColor2, Color borderColor) {
        int[][] boardValues = this.controller.getBoardValues();

        this.size = 9;
        this.sizeRoot = 3;
        this.sudokuBoard = new Square[size][size];

        setLayout(new GridLayout(size, size));

        // Board Border
        setBorder(new CompoundBorder(
                new EmptyBorder(8, 8, 8, 8),
                new LineBorder(borderColor, 3)
        ));

        // Set the BG
        setBackground(bg);

        // Create the chess board squares
        Insets buttonMargin = new Insets(0, 0, 0, 0);
        for (int i = 0; i < sudokuBoard.length; i++) {
            for (int j = 0; j < sudokuBoard[i].length; j++) {

                // Alternate colors of squares
                Color sqColor;
                if ((j % 2 == 1 && i % 2 == 1) | (j % 2 == 0 && i % 2 == 0))
                    sqColor = sqColor1;
                else
                    sqColor = sqColor2;

                // Square border
                Square sq = new Square(sqColor, boardValues[i][j]);
                sq.setMargin(buttonMargin);
                sq.setBorder(new LineBorder(borderColor));

                sudokuBoard[j][i] = sq;
            }
        }

        for (int i = 0; i < sudokuBoard.length; i++) {
            for (int j = 0; j < sudokuBoard[i].length; j++) {
                add(sudokuBoard[j][i]);
            }
        }
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        int[][] boardValues = controller.getBoardValues();

        for(int i = 0; i < sudokuBoard.length; i++) {
            for (int j =0; i<sudokuBoard[i].length; i++) {
                sudokuBoard[i][j].setValue(boardValues[i][j]);
            }
        }
    }

    public BoardView(SolverController controller) {
        init(Color.darkGray, Color.WHITE, Color.BLACK, Color.GRAY);
    }

    public BoardView(Color bg, Color sqColor1, Color sqColor2, Color borderColor, SolverController controller) {
        this.controller = controller;
        init(bg, sqColor1, sqColor2, borderColor);
    }

    public void emptySquare(int c, int r) {
        this.sudokuBoard[c][r].setIcon(new ImageIcon(new BufferedImage(64, 64, BufferedImage.TYPE_INT_ARGB)));
    }

    protected double horizontalCenter(JComponent bounds) {

        return bounds.getX() + bounds.getWidth() / 2d;

    }

    protected double verticalCenter(JComponent bounds) {

        return bounds.getY() + bounds.getHeight() / 2d;
    }
}
