package main.tp_Sudoku.NewGUI;

import main.tp_Sudoku.NewGUI.Controller.Controller;
import main.tp_Sudoku.NewGUI.Views.BoardView;
import main.tp_Sudoku.NewGUI.Views.MainMenuView;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class SudokuGUI extends JPanel {

    private CardLayout cardLayout;
    private JPanel cardsPanel;

    // Colors https://mir-s3-cdn-cf.behance.net/project_modules/fs/c61adb32154055.568c17a369e35.png
    private final Color bg = new Color(84, 122, 130);
    private final Color cbcolor1 = new Color(229, 238, 193);
    private final Color cbcolor2 = new Color(229, 238, 193);
    private final Color borderColor = new Color(162, 212, 171);

    Controller controller;
    // Sudoku Board board
    private BoardView boardView;
    private MainMenuView mainMenuView;


    private final JLabel message = new JLabel("Sudoku Sudoku");

    public SudokuGUI(Controller controller) {
        this.controller = controller;
//        this.mainMenuView = new MainMenuView(controller);
        initializeGui();
    }

    public final void initializeGui() {


        setLayout(new BorderLayout(3, 3));
        JButton but = new JButton();

        // Set up main container
        setBorder(new EmptyBorder(5, 5, 5, 5));

        JToolBar tools = new JToolBar();
        tools.setFloatable(false);
        add(tools, BorderLayout.PAGE_END);

        JButton nextPathButton = new JButton("Next");
        nextPathButton.addActionListener(controller.getNextListener());

        JButton autoPathButton = new JButton("Auto");
//        nextPathButton.addActionListener(controller.getOnAutoListener());


//        cardLayout = new CardLayout();
//        cardsPanel = new JPanel(cardLayout);
//        cardsPanel.add(mainMenuView, "mainMenu");
//        cardsPanel.add(boardView, "solverView");
////        cardsPanel.add(gameView, "game");
//
//        add(boardConstrain);
        //TODO cards
        boardView = new BoardView(bg, cbcolor1, cbcolor2, borderColor, controller);

        JPanel boardConstrain = new JPanel(new GridBagLayout());
        boardConstrain.setBackground(bg);
        boardConstrain.add(boardView);
        add(boardConstrain);
        tools.add(nextPathButton, JToolBar.CENTER);
    }



    public JPanel getBoardView() {
        return boardView;
    }

    public void goToMainMenu() {
        this.cardLayout.show(cardsPanel, "mainMenu");
    }

    public void goToBoard() {
        this.cardLayout.show(cardsPanel, "solverView");
    }
}