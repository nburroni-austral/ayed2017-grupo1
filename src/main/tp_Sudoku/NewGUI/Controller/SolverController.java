package main.tp_Sudoku.NewGUI.Controller;

import java.awt.event.ActionListener;

public interface SolverController {
    int[][] getBoardValues();
    ActionListener getNextListener();
    ActionListener getDoneListener();
}
