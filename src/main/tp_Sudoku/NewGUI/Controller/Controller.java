package main.tp_Sudoku.NewGUI.Controller;

import main.tp_Sudoku.NewGUI.SudokuGUI;
import main.tp_Sudoku.Solver;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller implements MainMenuController, SetupController, SolverController {
    private SudokuGUI sudokuGUI;
    private Solver model;
    int[][] demo = {
            {0,0,0, 0,0,0, 0,0,0},
            {0,0,0, 0,0,0, 0,0,0},
            {0,0,1, 0,0,0, 0,0,0},

            {0,0,0, 0,0,0, 0,0,0},
            {0,0,0, 0,0,0, 0,0,0},
            {0,0,0, 0,4,0, 0,0,0},

            {0,0,0, 0,0,0, 0,0,0},
            {0,0,0, 0,0,0, 0,2,0},
            {0,0,0, 0,0,0, 0,0,0},
    };

    public Controller() {

//        this.model = new Solver(demo);
        this.sudokuGUI = new SudokuGUI(this);
    }

    public void run() {
        Runnable r = () -> {
            System.out.println("Running...");
            SudokuGUI cg = sudokuGUI;

            JFrame f = new JFrame("Sudoku Sudoku");
            f.add(cg);

            // Ensures JVM closes after frame(s) closed and all non-daemon threads are finished
            f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

            // See http://stackoverflow.com/a/7143398/418556 for demo.
            f.setLocationByPlatform(true);

            // Ensures the frame is the minimum capacity it needs to be in order display the components within it
            f.pack();

            // Ensures the minimum capacity is enforced.
            f.setMinimumSize(f.getSize());
            f.setVisible(true);
        };

        // Swing GUIs should be created and updated on the EDT
        // http://docs.oracle.com/javase/tutorial/uiswing/concurrency
        SwingUtilities.invokeLater(r);
    }

    @Override
    public int[][] getBoardValues() {
//        return model.getBoardValues();
        return demo;
    }

    @Override
    public ActionListener getNextListener() {
//        return actionEvent -> model.next();
        return actionEvent -> demo = new int[][]{
                {1, 1, 1, 0, 0, 0, 0, 0, 0},
                {1, 1, 1, 0, 0, 0, 0, 0, 0},
                {1, 1, 1, 0, 0, 0, 0, 0, 0},

                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 4, 0, 0, 0, 0},

                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 2, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
        };
    }

    @Override
    public ActionListener getDoneListener() {
        return null;
    }

    public ActionListener getGoToMainMenuViewListener() {
        return actionEvent -> sudokuGUI.goToMainMenu();
    }

    public ActionListener getGoToBoardViewListener() {
        return actionEvent -> sudokuGUI.goToBoard();
    }

    public ActionListener getExitListener() {
        return actionEvent -> System.exit(0);
    }
}
