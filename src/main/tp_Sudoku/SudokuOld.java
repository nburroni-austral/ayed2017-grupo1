package main.tp_Sudoku;

import struct.impl.stacks.DynamicStack;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 29/03/2017.
 */

public class SudokuOld {

    private int[][] grid;
    private int size;

    public SudokuOld(int n){ // n must be safe, either 4,9,16

        grid = new int[n][n];
        size = n;

        for (int i = 0; i < grid.length; i++){
            for(int j = 0; i < grid.length; i++){
                grid[i][j] = 0;
            }
        }

    }

    public void addValue(int value, int r, int c){
        grid[r][c] = value;
    }

    public void solve(){

        for(int i = 0; i < grid.length; i++){
            for (int j = 0; j < grid.length; j++){
                if(grid[i][j] == 0){
                    DynamicStack<Integer> safeValues = safeValues(i,j);
                    grid[i][j] = (int) safeValues.peek();

                    for (int k = i ; k < grid.length; k++){
                        for(int l = j; l < grid.length; l++){
                            if(grid[k][l] == 0){
                                DynamicStack<Integer> nextSafeValues = safeValues(k,l);

                                if(nextSafeValues.isEmpty()){
                                    safeValues.pop();
                                    grid[i][j] = (int) safeValues.peek();
                                    k = i -1;
                                    l = j -1;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public boolean isSafe(int possibleSolution, int r, int c){

        for(int i = 0; i < size; i++){
            if(grid[r][i] == possibleSolution) return false;        //is col safe
            if(grid[i][c] == possibleSolution) return false;        //is row safe
        }

        return isBoxSafe(possibleSolution,r,c);
    }

    public boolean isBoxSafe(int possibleSolution, int r, int c) {

        int gridSize = (int) Math.sqrt(size);

        int i = 0;
        int j = gridSize - 1;
        int k = 0;

        while(k < size){
            if( i <= r && r <= j){  // if the possibleSolution's row is between this row gap, then the row is this

                int l = 0;
                int m = gridSize - 1;

                while(m < grid.length){

                    if(l <= c && c <= m) { // if possibleSolution's col is between this col gap, then the box is this
                        for(int row = i; row < j ; row++){  // Once in the box, search from top
                            for (int col = l; col < m; col++ ){
                                if(grid[row][col] == possibleSolution) return false; //If found same value with different position, false;
                            }
                          }
                    }

                    l = l + gridSize; // Step to next box - init (cols)
                    m = m + gridSize; // Step to next box - end (cols)
                    k++;
                }

            }

            i = i + gridSize; // Step to next box - init (rows)
            j = j + gridSize; // Step to next box - end  (rows)
            k = k +3; // One row more steps 3 boxes
        }

        return true; //If not found any value in the box like the one passed, true;
    }

    private DynamicStack<Integer> safeValues(int r, int c){
        DynamicStack<Integer> safeValues = new DynamicStack<>();

        for (int value = 1; value <= size; value++){ //Initialize possible values
            if(isSafe(value,r,c)) safeValues.push(value);
        }

        return safeValues;
    }

    public void print(){
        for (int i = 0; i < grid.length; i++){
            for (int j = 0; j < grid.length; j++){
                System.out.print(grid[i][j] + "\t");
            }
            System.out.println("");
        }
    }
}
