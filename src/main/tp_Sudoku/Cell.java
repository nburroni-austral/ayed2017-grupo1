package main.tp_Sudoku;

import struct.impl.stacks.DynamicStack;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 04/04/2017.
 */
public class Cell {

    private boolean isVariable;
    private DynamicStack<Integer> candidateValues;
    private int r;
    private int c;

    public Cell(int r, int c){
        this.candidateValues = new DynamicStack<>();
        this.isVariable = true;
        this.r = r;
        this.c = c;
    }

    public void assignFixedValue(int value){
        candidateValues.push(value);
        this.isVariable = false;
    }

    public void setCandidateValues(DynamicStack<Integer> candidateValues){
        this.candidateValues = candidateValues;
    }

    public DynamicStack<Integer> getCandidateValues(){
        return candidateValues;
    }

    public int getValue(){
        try {
            return candidateValues.peek();
        } catch (Exception e) {
            return 0;
        }
    }

    public boolean isVariable(){
        return isVariable;
    }

    public int getRow() {
        return r;
    }

    public int getCol() {
        return c;
    }

    public void eliminateCandidate() {
        candidateValues.pop();
    }

    public boolean ranOutOfCandidates() {
        return candidateValues.isEmpty();
    }

    public String toString() {
        return (this.getRow()+"-"+this.getCol() +":"+ this.getValue());
    }
}
