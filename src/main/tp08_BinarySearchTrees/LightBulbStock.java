package main.tp08_BinarySearchTrees;

/**
 * Represents the stock of a particular light bulb model
 */
public class LightBulbStock implements Comparable<LightBulbStock>{
//    Código de lámpara (5 caracteres)
//    Watts (entero)
//    Tipo de lámpara (hasta 10 caracteres)
//    Cantidad (entero)

    private String modelCode;
    private int watts;
    private String kind;
    private int quantity;

    public LightBulbStock(String modelCode, int watts, String kind, int quantity) {
        this.modelCode = modelCode;
        this.watts = watts;
        this.kind = kind;
        this.quantity = quantity;
    }

    public LightBulbStock(String modelCode, int watts) {
        this(modelCode, watts, "", 0);
    }

    public LightBulbStock(String modelCode, int watts, String kind) {
        this(modelCode, watts, kind, 0);
    }

    public void addLightBulbs(int quantity) {
        this.quantity += quantity;
    }

    public void removeLightBulbs(int quantity) {
        this.quantity -= quantity;
    }
    /**
     * Compares LightBulbStocks using their codes
     * @param other stock to compare against
     * @return negative number if lower, 0 if there is no difference, positive number if higher
     */
    @Override
    public int compareTo(LightBulbStock other) {
        return getModelCode().compareTo(other.getModelCode());
    }

    public String getModelCode() {
        return modelCode;
    }

    public int getWatts() {
        return watts;
    }

    public String getKind() {
        return kind;
    }

    public int getQuantity() {
        return quantity;
    }
}
