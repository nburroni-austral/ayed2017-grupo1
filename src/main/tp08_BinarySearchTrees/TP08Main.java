package main.tp08_BinarySearchTrees;

import struct.impl.util.mains.ConsoleMainMenu;

public class TP08Main {
    public static void main(String[] args) {
        ConsoleMainMenu tp08MainMenu = new TP08MainMenu();
        tp08MainMenu.run();
    }
}
