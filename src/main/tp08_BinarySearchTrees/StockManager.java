package main.tp08_BinarySearchTrees;

import java.util.LinkedList;
import java.util.List;

public class StockManager {
    BiSearchTree<String, LightBulbStock> tree;

    public void addFromList(LinkedList<LightBulbStock> list) {
        for (LightBulbStock lightBulbStock : list)
            addLightBulbStock(lightBulbStock);
    }

    public void addLightBulbStock(LightBulbStock lightBulbStock) {
        tree.insert(lightBulbStock.getModelCode(), lightBulbStock);
    }

    public void removeLightBulbStock(String modelCode) {
        tree.remove(modelCode);
    }

    public void addLightBulbs(String modelCode, int quantity) {
        tree.search(modelCode).addLightBulbs(quantity);
    }

    public void removeLightBulbs(String modelCode, int quantity) {
        tree.search(modelCode).removeLightBulbs(quantity);
    }

    public List<LightBulbStock> getOrderedReport() {
        return tree.getValuesOrderedByKeys();
    }
}
