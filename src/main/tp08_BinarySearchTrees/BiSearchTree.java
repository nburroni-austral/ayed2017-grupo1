package main.tp08_BinarySearchTrees;

import java.lang.Comparable;
import java.util.LinkedList;
import java.util.List;

public class BiSearchTree<K extends Comparable<? super K>, V> {
    DoubleNode root;

    public BiSearchTree() {
        root = null;
    }

    public BiSearchTree(K key, V value) {
        root = new DoubleNode(key, value);
    }

    public BiSearchTree(K key, V value, BiSearchTree<K, V> left, BiSearchTree<K, V> right) {
        root = new DoubleNode(key, value, left.root, right.root);
    }

    public boolean isEmpty() {
        return root == null;
    }

    public V getValue() {
        return root.value;
    }

    public K getKey() {
        return root.key;
    }

    public BiSearchTree<K, V> getLeft() {
        BiSearchTree<K, V> tree = new BiSearchTree<>();
        tree.root = this.root.left;
        return tree;
    }

    public BiSearchTree<K, V> getRight() {
        BiSearchTree<K, V> tree = new BiSearchTree<>();
        tree.root = this.root.right;
        return tree;
    }

    public boolean contains(K key) {
        if (root == null)
            return false;
        else if(root.key.equals(key))
            return true;
        return getLeft().contains(key) | getRight().contains(key);
    }

    public boolean contains(V value) {
        if (root == null)
            return false;
        else if (root.value.equals(value))
            return true;
        return getLeft().contains(value) | getRight().contains(value);
    }

    public void insert(K key, V element) {
        root = insert(root, key, element);
    }

    private DoubleNode insert(DoubleNode tree, K key, V value) {
        if (tree == null) {
            tree = new DoubleNode();
            tree.key = key;
            tree.value = value;
        }
        if (key.compareTo(tree.key) < 0) tree.left = insert(tree.left, key, value);
        if (key.compareTo(tree.key) > 0) tree.right = insert(tree.right, key, value);
        return tree;
    }

    public void remove(K key) {
        root = remove(key, root);
    }

    private DoubleNode remove(K key,DoubleNode tree) {
        if (key.compareTo(tree.key) < 0) {
            tree.left = remove(key, tree.left);
        } else if (key.compareTo(tree.key) > 0){
            tree.right= remove(key, tree.right);
        } else {
            if(tree.left != null && tree.right != null){
                tree.value = getMinNode(tree.right).value;
                tree.right = removeMin(tree.right);
            } else if(tree.left != null){
                tree = tree.left;
            } else {
                tree = tree.right;
            }
        }
        return tree;
    }

    private DoubleNode removeMin(DoubleNode tree){
        if(tree.left != null){
            tree.left = removeMin(tree.left);
        } else {
            tree = tree.right;
        }

        return tree;
    }

    public V search(K key) {
        return search(root, key).value;
    }

    private DoubleNode search(DoubleNode tree, K key) {

        if (tree.key.compareTo(key) == 0){
            return tree;
        }else if (key.compareTo(tree.key) < 0) {
            return search(tree.left, key);
        } else {
            return search(tree.right, key);
        }

    }

    public V getMax() {
        return getMax(root).value;
    }

    private DoubleNode getMax(DoubleNode tree) {
        if (tree.right == null)
            return tree;
        return getMax(tree.right);
    }

    public V getMin() {
        return getMin(root).value;
    }

    private DoubleNode getMin(DoubleNode tree) {
        if (tree.left == null)
            return tree;
        return getMax(tree.left);
    }

    public LinkedList<V> getValuesOrderedByKeys() {
        LinkedList<V> list = new LinkedList<>();
        fillListWithOrderedValues(this, list);
        return list;
    }

    private void fillListWithOrderedValues(BiSearchTree<K, V> tree, List<V> list) {
        if (tree.isEmpty())
            return;

        fillListWithOrderedValues(tree.getLeft(), list);
        list.add(tree.getValue());
        fillListWithOrderedValues(tree.getRight(), list);
    }

    private DoubleNode getMinNode(DoubleNode tree) {
        if (tree.left == null)
            return tree;
        return getMinNode(tree.left);
    }

    /**
     * <b>DoubleNode</b>
     * <p>
     * Basic structure of a tree (inner class)
     */
    private class DoubleNode {
        V value;
        K key;
        DoubleNode left;
        DoubleNode right;

        DoubleNode(){
            this.value = null;
            this.key = null;
        }

        DoubleNode(K key, V value, DoubleNode left, DoubleNode right) {
            this.key = key;
            this.value = value;
            this.left = left;
            this.right = right;
        }

        DoubleNode(K key, V value) {
            this(key, value, null, null);
        }
    }
}
