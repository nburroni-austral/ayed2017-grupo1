package main.tp08_BinarySearchTrees;

import struct.impl.util.mains.ConsoleMainMenu;

public class TP08MainMenu extends ConsoleMainMenu{
    public TP08MainMenu() {
        super("Binary Search Trees");
        consoleInterfaces.add(new LightBulbStockManagerConsoleInterface());
    }
}
