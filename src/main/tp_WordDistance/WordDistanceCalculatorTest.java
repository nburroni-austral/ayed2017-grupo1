package main.tp_WordDistance;



import org.junit.jupiter.api.Test;

import java.util.function.BiFunction;

import static org.junit.jupiter.api.Assertions.assertEquals;


class WordDistanceCalculatorTest {
    WordDistanceCalculator wordDistanceCalculator = new WordDistanceCalculator();

    @Test
    public void calculateHammingDistance() {
        testHammingMethod(wordDistanceCalculator::calculateHammingDistance);
    }

    private void testHammingMethod(BiFunction<String, String, Integer> algorithm) {
        String a = "hamming";
        String b = "j4mm1n'";
        int distance = algorithm.apply(a, b);

        assertEquals(4,distance);
    }

    @Test
    public void calculateLevenshteinDistance() throws Exception {
        testLevenshteinMethod(wordDistanceCalculator::calculateLevenshteinDistance);
    }

    private void testLevenshteinMethod(BiFunction<String, String, Integer> algorithm) {
        // Test insertions
        String a = "ABCDE";
        String b = "ACE";
        int distance = algorithm.apply(a,b);
        assertEquals(2, distance);

        // Test substitutions
        a = "hamming";
        b = "j4mm1n'";
        distance = algorithm.apply(a,b);
        assertEquals(4, distance);

        // Test deletions
        a = "ABCDE";
        b = "ABCDE123";
        distance = algorithm.apply(a,b);
        assertEquals(3, distance);

        // Test deletions with falling letter
        a = "ABCDE";
        b = "ABCD123E";
        distance = algorithm.apply(a,b);
        assertEquals(3, distance);
    }
}