package main.tp_WordDistance;

/**
 * Provides different methods of measuring distances between strings.
 * @author Nicolás Gargano <nicolas.gargano @ ing.austral.edu.ar>, Lautaro Paskevicius <lautaro.paskevicius @ ing.austral.edu.ar>
 */
public class WordDistanceCalculator {

    /**
     * Finds the hamming distance between two equal-length strings.
     * <p>
     * The Hamming distance between two strings of equal length is the number of positions at which the corresponding symbols are different.
     * In other words, it measures the minimum number of <b>substitutions</b> required to change one string into the other,
     * or the minimum number of errors that could have transformed one string into the other.
     * </p>
     *
     * @param a One of the strings to compare.
     * @param b The other string to compare
     * @return Hamming Distance.
     */
    public int calculateHammingDistance(String a, String b) {
        int distance =0;

        for (int i=0; i<a.length(); i++)
            if (a.charAt(i) != b.charAt(i))
                distance++;

        return distance;
    }


    /**
     * Finds the lowest of the three integer arguments.
     *
     * @param a One of the ints to compare
     * @param b Another int to compare
     * @param c The third int to compare
     * @return The lowest of the three arguments.
     */
    private int findMinimum(int a, int b, int c) {
        if(a<=b && a<=c){
            return a;
        }
        if(b<=a && b<=c){
            return b;
        }
        return c;
    }

    /**
     * Finds the Levenshtein distance between two equal-length strings.
     * <p>
     * The Levenshtein distance is a string metric for measuring the difference between two sequences.
     * Informally, the Levenshtein distance between two words is the minimum number of single-character edits
     * (insertions, deletions or substitutions) required to change one word into the other.
     * </p>
     *
     * @param a One of the strings to compare.
     * @param b The other string to compare
     * @return Levenshtein Distance.
     */
    public int calculateLevenshteinDistance(String a, String b) {
        return calculateLevenshteinDistance(a.toCharArray(),
                b.toCharArray());
    }

    /**
     * Finds the Levenshtein distance between two equal-length strings.
     * <p>
     * The Levenshtein distance is a string metric for measuring the difference between two sequences.
     * Informally, the Levenshtein distance between two words is the minimum number of single-character edits
     * (insertions, deletions or substitutions) required to change one word into the other.
     * </p>
     *
     * @param a One of the char arrays to compare.
     * @param b The other char array to compare
     * @return Levenshtein Distance.
     */
    private int calculateLevenshteinDistance(char [] a, char [] b) {

        // Set up table
        int [][]distance = new int[a.length+1][b.length+1];

        // Fill first row and first column with distances from empty string to prefixes
        for(int i=0;i<=a.length;i++){
            distance[i][0]=i;
        }
        for(int j=0;j<=b.length;j++){
            distance[0][j]=j;
        }

        // Fill table with values corresponding to the shortest distance between prefixes
        for(int i=1;i<=a.length;i++){
            for(int j=1;j<=b.length;j++){
                distance[i][j]= findMinimum(distance[i-1][j]+1,
                        distance[i][j-1]+1,
                        distance[i-1][j-1]+((a[i-1]==b[j-1])?0:1));
            }
        }

        // The value in the last corner represents the distance between the original words
        return distance[a.length][b.length];
    }
}
