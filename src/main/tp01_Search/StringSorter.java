package main.tp01_Search;

public class StringSorter {
    public void insertionSort(String[] strings) {
        int i;
        String toInsert;

        // From index 1 to the end
        for (int wallPos = 1; wallPos < strings.length; wallPos++) {

            // Save the next value to insert
            toInsert = strings[wallPos];

            // Push backwards until we find where it fits
            for (i = wallPos - 1; i >= 0 && strings[i].compareTo(toInsert) > 0; i--) {
                strings[i + 1] = strings[i];
            }

            // Put our value
            strings[i + 1] = toInsert;
        }
    }

    public void bubbleSort(String[] strings) {
        boolean didSwap = true;
        int runs = 0;

        // If we did not make a swap in a run, it means the array is ordered
        while (didSwap) {
            didSwap=false;

            // Move the comparison tray from the beginning to the end swapping when the value in the left is greater than the value in the right
            for (int blockPos = 0; blockPos < strings.length - 1 - runs; blockPos++) {
                if (strings[blockPos].compareTo(strings[blockPos+1]) > 0) {

                    //swap
                    String tmp = strings[blockPos];
                    strings[blockPos] = strings[blockPos+1];
                    strings[blockPos+1] = tmp;

                    didSwap = true;
                }
            }
            runs++;
        }
    }

    public void selection(String[] strings) {
        int candidateIndex;

        // For each position
        for (int currPos = 0; currPos< strings.length; currPos++) {

            // Assume the current position's value is correct
            candidateIndex = currPos;

            // Make run looking for the most fitting value
            for (int checker = currPos; checker < strings.length; checker++) {

                // If we find a more fitting one, update the selection index
                if (strings[checker].compareTo(strings[candidateIndex]) < 0) {
                    candidateIndex = checker;
                }
            }

            // Once the run ends, swap value at the candidate index with the one in current position
            String selectedVal = strings[candidateIndex];
            strings[candidateIndex] = strings[currPos];
            strings[currPos] = selectedVal;
        }
    }

}
