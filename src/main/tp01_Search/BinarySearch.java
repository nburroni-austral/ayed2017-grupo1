package main.tp01_Search;
public class BinarySearch {

    /**
     * <b>findIndex</b>
     *
     * Finds the index where the given value resides.
     *
     * @param k The int value whose index is desired.
     * @param integers An array of ints in which to search the value.
     * @return index Position of the array in which a desired value resides.
     */

    public int findIndex(int k, int[] integers){
        return findIndex(k, integers, 0, integers.length);
    }

    /**
     * <b>findIndex</b>
     *
     * Finds the index where the given value resides.
     *
     * @param k The int value whose index is desired.
     * @param integers An array of ints in which to search the value.
     * @param first index marking the start of the sub-array to search in.
     * @param last index marking the end of the sub-array to search in.
     * @return index Position of the array in which a desired value resides.
     */
    private int findIndex(int k, int[] integers, int first, int last) {

        int middle = (first+last)/2;

        if(first < last){
            if (k == integers[middle]) {
                return middle;
            } else if(k < integers[middle]){
                last = middle;
                return findIndex(k,integers,first,last);
            } else if(k > integers[middle]) {
                first = middle;
                return findIndex(k, integers, first, last);
            }
        }
        return -1;
    }

}
