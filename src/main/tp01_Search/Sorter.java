package main.tp01_Search;

import java.util.Comparator;

/**
 * Sorter
 *
 * The sorter class contains different methods that provide sorting algorithms
 * for different data structures. Specifically, implementations of insertion, bubble, and selection sorts.
 *
 * @author Nicolas Gargano
 * @author Lautaro Paskevicius
 */
public class Sorter {


    /**
     * <b>insertionSort</b>
     *
     * The idea of insertion is the reverse of selection, pick a value and find the position where it belongs.
     *
     * @param nums An int array
     */
    public void insertionSort(int[] nums) {
        int i;
        int toInsert;

        // From index 1 to the end
        for (int wallPos = 1; wallPos < nums.length; wallPos++) {

            // Save the next value to insert
            toInsert = nums[wallPos];

            // Push backwards until we find where it fits
            for (i = wallPos - 1; i >= 0 && nums[i] > toInsert; i--) {
                nums[i + 1] = nums[i];
            }

            // Put our value
            nums[i + 1] = toInsert;
        }
    }

    /**
     * <b>bubbleSort</b>
     *
     * Bubble Sort is a simple comparison-based sorting algorithm in which each pair of adjacent elements is compared and then exhanged if not in order.
     *
     * @param nums An int array.
     */
    public void bubbleSort(int[] nums) {
        boolean didSwap = true;
        int runs = 0;

        // If we did not make a swap in a run, it means the array is ordered
        while (didSwap) {
            didSwap=false;

            // Move the comparison tray from the beginning to the end swapping when the value in the left is greater than the value in the right
            for (int blockPos = 0; blockPos < nums.length - 1 - runs; blockPos++) {
                if (nums[blockPos] > nums[blockPos + 1]) {

                    // swap
                    int tmp = nums[blockPos];
                    nums[blockPos] = nums[blockPos+1];
                    nums[blockPos+1] = tmp;

                    didSwap = true;
                }
            }
            runs++;
        }
    }

    /**
     * <b>selectionSort</b>
     *
     * Selection sort is another comparison-based sorting algorithm based on the idea of finding the most fitting value for a position.
     *
     * @param nums An int array.
     *
     */
    public void selectionSort(int[] nums) {
        int selectedIndex;

        // For each position
        for (int currPos = 0; currPos < nums.length; currPos++) {

            // Assume the current position's value is correct
            selectedIndex = currPos;

            // Make run looking for the most fitting value
            for (int checker = currPos; checker < nums.length; checker++) {

                // If we find a more fitting one, update the selection index
                if (nums[checker] < nums[selectedIndex]) {
                    selectedIndex = checker;
                }
            }

            // Once the run ends, swap value at the selected index with the one in current position
            int selectedVal = nums[selectedIndex];
            nums[selectedIndex] = nums[currPos];
            nums[currPos] = selectedVal;
        }
    }

    /**
     * <b>recursiveSelectionSort</b>
     *
     * Public interface for the recursive selection.
     *
     * @param nums An int array.
     */
    public void recursiveSelectionSort(int[] nums){
       recursiveSelectionSort(nums,0);
    }

    /**
     * <b>recursiveSelectionSort</b>
     *
     * Implements selection sort in a recursive fashion.
     *
     * @param nums an int array.
     * @param pos index for which to make a selection.
     */
    private void recursiveSelectionSort(int[] nums, int pos) {
        int selectedIndex;

        // Break when reached last position
        if (pos >= nums.length-1)
            return;

        // Make run looking for the most fitting value, assuming first that it is the current one
        selectedIndex = pos;

        for (int checker = pos; checker < nums.length; checker++) {

            // If we find a more fitting one, update the selection index
            if (nums[checker] < nums[selectedIndex]) {
                selectedIndex = checker;
            }
        }

        // Once the run ends, swap value at the selected index with the one in current position
        int selectedVal = nums[selectedIndex];
        nums[selectedIndex] = nums[pos];
        nums[pos] = selectedVal;

        recursiveSelectionSort(nums, pos+1);
    }

    /**
     * <b>selectionSortByComparator</b>
     *
     * Allows selection to sort by the criteria established in a comparator.
     *
     * @param toSort An unsorted array of generic type T.
     * @param criteria A comparator criteria for T type values.
     */
    public <T> void selectionSortByComparator(T[] toSort, Comparator<T> criteria){

        int selectedIndex;

        // For each position
        for (int currPos = 0; currPos < toSort.length; currPos++) {

            // Assume the current position's value is correct
            selectedIndex = currPos;

            // Make run looking for the most fitting value
            for (int checker = currPos; checker < toSort.length; checker++) {

                // If we find a more fitting one, update the selection index
                if (criteria.compare(toSort[checker], toSort[selectedIndex]) <= 0) {
                    selectedIndex = checker;
                }
            }

            // Once the run ends, swap value at the selected index with the one in current position
            T selectedVal = toSort[selectedIndex];
            toSort[selectedIndex] = toSort[currPos];
            toSort[currPos] = selectedVal;
        }
    }

    /**
     * <b>insertionSortByComparator</b>
     *
     * Adaptation of insertion sort that is capable of sorting with generics and comparator.
     *
     * @param toSort An unsorted array of generic type T.
     * @param criteria A comparator criteria for T type values.
     */
    public <T> void insertionSortByComparator(T[] toSort, Comparator<T> criteria){

        int i;
        T toInsert;

        // From index 1 to the end
        for (int wallPos = 1; wallPos < toSort.length; wallPos++) {

            // Save the next value to insert
            toInsert = toSort[wallPos];

            // Push backwards until we find where it fits
            for (i = wallPos - 1; i >= 0 && criteria.compare(toSort[i], toInsert) > 0; i--) {
                toSort[i + 1] = toSort[i];
            }

            // Put our value
            toSort[i + 1] = toInsert;
        }
    }

    /**
     * <b>bubbleSortByComparator</b>
     *
     * Adaptation of bubble sort that is capable of sorting with generics and comparator.
     *
     * @param toSort An unsorted array of generic type T.
     * @param criteria A comparator criteria for T type values.
     */
    public <T> void bubbleSortByComparator(T[] toSort, Comparator<T> criteria) {
        boolean didSwap = true;
        int runs = 0;

        // If we did not make a swap in a run, it means the array is ordered
        while (didSwap) {
            didSwap=false;

            // Move the comparison tray from the beginning to the end swapping when the value in the left is greater than the value in the right
            for (int blockPos = 0; blockPos < toSort.length - 1 - runs; blockPos++) {
                if (criteria.compare(toSort[blockPos], toSort[blockPos+1]) > 0) {

                    // swap
                    T tmp = toSort[blockPos];
                    toSort[blockPos] = toSort[blockPos+1];
                    toSort[blockPos+1] = tmp;

                    didSwap = true;
                }
            }
            runs++;
        }
    }
}
