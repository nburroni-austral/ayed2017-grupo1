package main.tp01_Search;

import struct.istruct.Comparable;

/**
 * Merger
 *
 * Single-method class. Merges two sorted arrays of Comparables into a single, sorted, Comparable array.
 *
 * @author Nicolas Gargano & Lautaro Paskevicius.
 *
 */

public class Merger <T extends Comparable<T>> {

    /**
     * merge
     *
     * HorseController class purpose. Algorithm method, joins two sorted arrays into a single sorted array.
     *
     * @param a First sorted array of Comparable.
     * @param b Second sorted array of Comparable.
     * @return Comparable array, result of merged parameters.
     */
    public Comparable<T>[] merge(Comparable<T>[] a, Comparable<T>[] b) {

        // Add a new array with capacity equal to the sum of the inputs' length
        Comparable<T>[] c = new Comparable[a.length + b.length];

        // Initialize indexes for each array
        int ia = 0;
        int ib = 0;
        int ic = 0;


        /*
            As long as a and b have elements to be added, compare the ones at the current indexes and add the lowest,
            then increment the index of the element's origin and ic.
        */
        while (ia < a.length && ib < b.length) {
            if (a[ia].compareTo(b[ib]) >= 0) {
                c[ic] = a[ia];
                ia++;
                ic++;
            } else {
                c[ic] = b[ib];
                ib++;
                ic++;
            }
        }

        // Checks which of the input arrays ran out and adds the elements from the other to c
        if (ia == a.length) {
            for (; ib < b.length; ib++) {
                c[ic] = b[ib];
                ic++;
            }
        } else {
            for (; ia < a.length; ia++) {
                c[ic] = a[ia];
                ic++;
            }
        }
        return c;
    }
}
