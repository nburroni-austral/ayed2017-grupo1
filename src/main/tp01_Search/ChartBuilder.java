package main.tp01_Search;


import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Consumer;


// Builds an illustration for the document showing the different running times of bubble, selection and insertion sorts as implemented in Sorter.
public class ChartBuilder extends Application{
    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Sorting Algorithms");

        System.out.println("About to start timing...");

        // defining the axes
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Size");
        yAxis.setLabel("Time (ns)");

        Sorter sorter = new Sorter();

        // creating the chart
        final LineChart<Number, Number> lineChart =
                new LineChart<>(xAxis, yAxis);

        // Creating series and notifying user
        int size = 20000;
        System.out.print("Timing selection sort...");
        XYChart.Series seriesSelection = makeSeriesFromDataPoints("Selection", createDataSet(sorter::selectionSort, size));

        System.out.print(" Done\nTiming insertion sort...");
        XYChart.Series seriesInsertion = makeSeriesFromDataPoints("Insertion", createDataSet(sorter::insertionSort, size));

        System.out.println(" Done\nTiming bubble sort...");
        XYChart.Series seriesBubble = makeSeriesFromDataPoints("Bubble", createDataSet(sorter::bubbleSort, size));


        // Populating the scene with data
        System.out.println("Populating graph...");
        Scene scene = new Scene(lineChart, 800, 600);
        lineChart.getData().add(seriesBubble);
        lineChart.getData().add(seriesInsertion);
        lineChart.getData().add(seriesSelection);

        stage.setScene(scene);
        stage.show();
    }

    // Makes a Series from a title and a list of data points
    private XYChart.Series makeSeriesFromDataPoints(String seriesTitle, ArrayList<DataPoint> dataPoints) {
        XYChart.Series series = new XYChart.Series();
        series.setName(seriesTitle);

        for (DataPoint dataPoint : dataPoints)
            series.getData().add(new XYChart.Data(dataPoint.getX(), dataPoint.getY()));

        return series;
    }

    // Creates a list of data points to be used on the graph
    private ArrayList<DataPoint> createDataSet(Consumer<int[]> sortingAl, int size) {
        ArrayList<DataPoint> dataPoints = new ArrayList<>();

        int[] bigArray = new int[size];

        // Populate array with random values
        for (int i = 0; i < bigArray.length; i++) {
            bigArray[i] = ThreadLocalRandom.current().nextInt(0, 1001);
        }

        // Feed algorithm with arrays of different sizes and time it, adding the result to the list of data points
        for (int i = 1; i < size; i += 100) {
            int[] arrayToSort = Arrays.copyOfRange(bigArray, 0, i);

            long startTime = System.nanoTime();
            sortingAl.accept(arrayToSort);
            long endTime = System.nanoTime();

            dataPoints.add(new DataPoint(i, (endTime - startTime)));
        }
        return dataPoints;
    }

    // Simple representation of a point in an XY coordinate system
    class DataPoint {
        private long x;
        private long y;

        public DataPoint(long x, long y) {
            this.x = x;
            this.y = y;
        }

        public long getX() {
            return x;
        }

        public long getY() {
            return y;
        }
    }
}
