package main.tp01_Search;

public class SequentialSearch {

    /**
     * <b>findIndex</b>
     *
     * Finds the index where the given value resides.
     *
     * @param k The int value whose index is desired.
     * @param integers An array of ints in which to search the value.
     * @return index Position of the array in which a desired value resides.
     */

    public int findIndex(int k, int[] integers){
        int index = -1;

        for(int i = 0; i < integers.length; i++){
            if(k == integers[i]){
                index = i;
            }
        }

        return index;
    }

}
