package main.tp_234_RB_Trees.Tree_234;

import java.util.ArrayList;

public class Node4<V extends Comparable<? super V>> extends Node3<V>{
    V data3;
    Node<V> center2;

    public Node4() {
        type = 4;
    }

    public Node<V> getCenter2() {
        return center2;
    }

    public void setCenter2(Node<V> center2) {
        if(center2!=null){
            center2.setFather(this);
        }
        this.center2 = center2;
    }

    public Node<V> search(V c) {
        Node<V> node = destroy();
        return node.search(c);
    }

    public void print() {
        System.out.println("D1: "+data1 + " D2: "+data2+ " D3: "+data3);
        if(getLeft()!=null) getLeft().print();
        if(getCenter1()!=null) getCenter1().print();
        if(center2!=null) center2.print();
        if(getRight()!=null) getRight().print();
    }

    public Node<V> insert(V o) {
        setFather(destroy());
        return getFather().insert(o);
    }

    public Node<V> destroy() {
        if(getFather()==null){
            setFather(new Node2());
        }
        setFather(getFather().insert(data2));
        Node2<V> nodeR = new Node2<>();
        nodeR.data1 = data3;
        nodeR.setFather(getFather());
        nodeR.setLeft(center2);
        nodeR.setRight(getRight());
        Node2<V> nodeL = new Node2<>();
        nodeL.data1 = data1;
        nodeL.setFather(getFather());
        nodeL.setLeft(getLeft());
        nodeL.setRight(getCenter1());
        getFather().setChild(nodeR.data1,nodeR);
        getFather().setChild(nodeL.data1,nodeL);
        return getFather();
    }

    public void setChild(V o, Node<V> child) {
        int comparedWithData1 = o.compareTo(data1);
        int comparedWithData2 = o.compareTo(data2);
        int comparedWithData3 = o.compareTo(data3);
        if(comparedWithData1<0) setLeft(child);
        else{
            if(comparedWithData2<0) setCenter1(child);
            else{
                if(comparedWithData3<0) setCenter2(child);
                else{
                    setRight(child);
                }
            }
        }
    }
    public ArrayList<V> getData() {
        ArrayList<V> list = new ArrayList<>();
        list.add(data1);
        list.add(data2);
        list.add(data3);
        return list;
    }
}
