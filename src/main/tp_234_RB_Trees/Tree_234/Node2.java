package main.tp_234_RB_Trees.Tree_234;

import java.util.ArrayList;

public class Node2<V extends Comparable<? super V>> extends Node<V>{
    V data1;
    private Node<V> left;
    private Node<V> right;

    public Node2() {
        type = 2;
    }


    public Node3<V> convertTo3(V o){
        Node3<V> node3 = new Node3();
        node3.setFather(this.getFather());
        node3.setLeft(left);
        node3.setRight(right);
        if(o.compareTo(data1)>0){
            node3.data2 = o;
            node3.data1 = data1;
        }else{
            node3.data2 = data1;
            node3.data1 = o;
        }
        if(getFather()!=null)getFather().setChild(o,node3);
        return node3;
    }

    public Node<V> search(V c) {
        int comparedWithData1 = c.compareTo(data1);
        if(this.isLeaf()) return this;
        else{
            if(comparedWithData1>0){
                return right.search(c);
            }else{
                return left.search(c);
            }
        }
    }

    public boolean isLeaf() {
        if(left ==null && right == null) return true;
        else return false;
    }

    public Node<V> insert(V o) {
        if(data1==null){
            data1 = o;
            return this;
        }else{
            return convertTo3(o);
        }
    }

    public void setChild(V o, Node<V> child) {
        int comparedWithData1 = o.compareTo(data1);
        if(comparedWithData1>0) this.setRight(child);
        else this.setLeft(child);
    }

    public void print() {
        System.out.println("D1: "+data1);
        if(left!=null) left.print();
        if(right!=null) right.print();
    }

    public ArrayList<V> getData() {
        ArrayList<V> list = new ArrayList<>();
        list.add(data1);
        return list;
    }

    public Node<V> getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
        if(left!=null){
            left.setFather(this);
        }
    }


    public Node<V> getRight() {
        return right;
    }

    public void setRight(Node<V> right) {
        this.right = right;
        if(right!=null){
            right.setFather(this);
        }
    }

}
