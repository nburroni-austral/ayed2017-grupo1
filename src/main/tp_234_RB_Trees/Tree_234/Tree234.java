package main.tp_234_RB_Trees.Tree_234;

public class Tree234<V extends Comparable<? super V>> {

    private Node<V> root;

    public Tree234(Comparable o) {
        Node2 node2 = new Node2();
        node2.data1 = o;
        root = node2;
        root.x = 400;
    }

    public Node<V> getRoot() {
        return root;
    }

    public void insert(V o){
        Node node = root.search(o);
        node = node.insert(o);
        Node father = node.getFather();
        if(father!=null){
            while(father.getFather() != null){
                father = father.getFather();
            }
            root = father;
        }
        else{
            root = node;
        }
    }

    public void print() {
        root.print();
    }
}
