package main.tp_234_RB_Trees.Tree_234;

import java.util.ArrayList;

public abstract class Node<V extends Comparable<? super V>> {
    private Node<V> father;
    public int type;
    int x;
    int y;
    public abstract Node<V> search(V c);
    public abstract boolean isLeaf();
    public abstract Node<V> insert(V object);
    public abstract void setChild(V o, Node<V> child);
    public abstract void print();
    public Node<V> getFather() {
        return father;
    }
    public abstract ArrayList<V> getData();
    public void setFather(Node<V> father) {
        this.father = father;
    }

}
