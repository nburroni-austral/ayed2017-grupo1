package main.tp_234_RB_Trees.Tree_234;

public class TreeApp implements Runnable{
    View view;

    public TreeApp(View view) {
        this.view = view;
    }

    public void run() {
        while(true){
            try{
                view.update();
                Thread.sleep(100);
            }catch(InterruptedException e){
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        Tree234 tree = new Tree234(4);
        View view = new View(tree);
        TreeApp treeApp = new TreeApp(view);
        Thread thread = new Thread(treeApp);
        thread.start();
        tree.print();
    }
}
