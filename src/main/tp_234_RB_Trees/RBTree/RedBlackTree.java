package main.tp_234_RB_Trees.RBTree;

import struct.impl.lists.DynamicList;
import struct.istruct.list.List;

public class RedBlackTree<E extends Comparable<? super E>> {
    private RBNode<E> root;

    public void insert(E element) {
        RBNode<E> newNode = new RBNode<>(element);
        if (root == null) {
            root = newNode;
            paintTree(newNode);
            return;
        }

        insert(newNode, root);
        paintTree(newNode);
    }

    private void insert(RBNode<E> newNode, RBNode<E> node) {


        if (newNode.compareTo(node) < 0)
            if (node.left() != null) {
                insert(newNode, node.left());
                return;
            } else {
                node.setLeft(newNode);
                newNode.setFather(node);
                return;
            }

        else if (node.right() != null) {
            insert(newNode, node.right());
            return;
        }

        node.setRight(newNode);
        newNode.setFather(node);
    }

    private void paintTree(RBNode<E> node) {
        if (node.father() == null) {
            node.paintBlack();
            return;
        }

        if (node.father().isBlack())
            return;

        if (isUncleBlack(node)) {

            if (isInternalFromGrandpa(node))
                simpleRotation(node);

            simpleRotation(node);

            return;
        }

        changeUncleFatherColors(node);
    }

    private boolean isUncleBlack(RBNode<E> node) {
        RBNode<E> grandpa = node.father().father();

        if (node.father().compareTo(grandpa) < 0)
            return  grandpa.right() == null | grandpa.left().isBlack();

        return  grandpa.left() == null | grandpa.right().isBlack();
    }

    private boolean isInternalFromGrandpa(RBNode<E> node) {
        return isLeftChild(node) ^ isLeftChild(node.father());
    }

    private void simpleRotation(RBNode<E> node) {
        RBNode<E> father = node.father();
        RBNode<E> grandpa = father.father();

        father.setFather(grandpa.father());

        if (isLeftChild(node)) {
            father.setRight(grandpa);
            grandpa.setLeft(null);
        } else {
            father.setLeft(grandpa);
            grandpa.setRight(null);
        }

        grandpa.paintRed();
        father.paintBlack();

        if (father.father() == null)
            root = father;
    }

    private boolean isLeftChild(RBNode<E> node) {
        return node.compareTo(node.father()) < 0;
    }

    private void changeUncleFatherColors(RBNode<E> node) {
        RBNode<E> grandpa = node.father().father();

        grandpa.paintRed();
        grandpa.left().paintBlack();
        grandpa.right().paintBlack();

        paintTree(grandpa);
    }

    public List<RBNode<E>> inOrder(){
        List<RBNode<E>> inOrder = new DynamicList<>();
        inOrder(root, inOrder);
        return inOrder;
    }

    private void inOrder(RBNode<E> node, List<RBNode<E>> order) {

        if (node == null) return;

        inOrder(node.left(), order);
        order.insertNext(node);
        inOrder(node.right(), order);
    }

    public List<RBNode<E>> preOrder(){
        List<RBNode<E>> preOrder = new DynamicList<>();
        preOrder(root, preOrder);
        return preOrder;
    }

    private void preOrder(RBNode<E> node, List<RBNode<E>> order) {

        if (node == null) return;

        order.insertNext(node);
        preOrder(node.left(), order);
        preOrder(node.right(), order);
    }

    public List<RBNode<E>> postOrder(){
        List<RBNode<E>> postOrder = new DynamicList<>();
        postOrder(root, postOrder);
        return postOrder;
    }

    private void postOrder(RBNode<E> node, List<RBNode<E>> order) {

        if (node == null) return;

        postOrder(node.left(), order);
        postOrder(node.right(), order);
        order.insertNext(node);
    }
}
