package main.tp_234_RB_Trees.RBTree;

import org.jetbrains.annotations.NotNull;

public class RBNode<E extends Comparable<? super E>> implements Comparable<RBNode<E>>{
    private E value;

    private boolean isRed;
    private RBNode<E> father;
    private RBNode<E> left;
    private RBNode<E> right;

    public RBNode() {
        this.isRed = true;
    }

    public RBNode(E value) {
        this(value, null);
    }

    public RBNode(E value, RBNode<E> father) {
        this(value, father, null, null);
    }

    public RBNode(E value, RBNode<E> left, RBNode<E> right) {
        this(value, null, left, right);
    }

    public RBNode(E value, RBNode<E> father, RBNode<E> left, RBNode<E> right) {
        this.value = value;
        this.father = father;
        this.left = left;
        this.right = right;
        this.isRed = true;
    }

    public boolean isBlack() {
        return !isRed;
    }

    public void paintBlack() {
        isRed = false;
    }

    public void paintRed() {
        isRed = true;
    }

    public E value() {
        return value;
    }

    public RBNode<E> father() {
        return father;
    }

    public RBNode<E> left() {
        return left;
    }

    public RBNode<E> right() {
        return right;
    }

    public void setFather(RBNode<E> father) {
        this.father = father;
    }

    public void setLeft(RBNode<E> left) {
        this.left = left;
    }

    public void setRight(RBNode<E> right) {
        this.right = right;

    }

    public boolean isLeaf() {
        return right() == null & left() == null;
    }
    @Override
    public int compareTo(@NotNull RBNode<E> other) {
        return this.value.compareTo(other.value);
    }

    @Override
    public String toString() {
        return value.toString()+"-"+(isRed?"r":"b");
    }
}