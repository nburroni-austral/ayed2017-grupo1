package main.tp_234_RB_Trees.RBTree;


import struct.istruct.list.List;

public class RedBlackTreeConsoleVisualizer {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_MAG = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";

    public static void main(String[] args) {
        RedBlackTree<Integer> tree = new RedBlackTree<>();

//        tree.insert(1);
//        displayInAndPreOrder(tree);
//        tree.insert(2);
//        displayInAndPreOrder(tree);
//        tree.insert(3);
//        displayInAndPreOrder(tree);

        tree = new RedBlackTree<>();

        tree.insert(3);
        displayInAndPreOrder(tree);
        tree.insert(2);
        displayInAndPreOrder(tree);
        tree.insert(1);
        displayInAndPreOrder(tree);
        tree.insert(4);
    }

    public static void displayInAndPreOrder(RedBlackTree<Integer> tree) {
        displayPreOrder(tree);
        displayInOrder(tree);
        System.out.println();
    }

    private static void displayOrder(List<RBNode<Integer>> nodeList) {
        for (int i = 0; i < nodeList.size(); i++) {
            nodeList.goTo(i);
            System.out.print(
                    (nodeList.getActual().isBlack() ? ANSI_CYAN : ANSI_MAG) +
                            nodeList.getActual().toString()
                            + ANSI_RESET + " "
            );
        }
        System.out.println();
    }

    public static void displayInOrder(RedBlackTree<Integer> tree) {
        System.out.println("Inorder");
        displayOrder(tree.inOrder());
    }

    public static void displayPreOrder(RedBlackTree<Integer> tree) {
        System.out.println("Preorder");
        displayOrder(tree.preOrder());
    }

    public static void displayPostOrder(RedBlackTree<Integer> tree) {
        System.out.println("Postorder");
        displayOrder(tree.postOrder());
    }
}
