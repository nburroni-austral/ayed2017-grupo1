package main.tp09_Hash;

import struct.impl.lists.DynamicList;

public class SoundexSpellChecker {
    private SoundexHashTable soundexHashTable;

    public SoundexSpellChecker() {
        this.soundexHashTable = new SoundexHashTable();
    }

    public boolean wordExists(String word) {
        return soundexHashTable.containsWord(word);
    }

    public DynamicList<String> getSuggestions(String word) {
        return soundexHashTable.getSuggestions(word);
    }

    public void addToDictionary(String word) {
        soundexHashTable.addWord(word);
    }
}
