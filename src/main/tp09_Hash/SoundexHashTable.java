package main.tp09_Hash;

import struct.impl.lists.DynamicList;
import struct.impl.lists.DynamicSortedList;
import struct.impl.lists.StaticList;

public class SoundexHashTable {

    private StaticList<DynamicSortedList<String>> buckets;

    public SoundexHashTable() {
        this.buckets = new StaticList<>(26*1000);
        for (int i = 0; i < buckets.getCapacity()-1; i++) {
            buckets.goTo(i);
            buckets.insertNext(new DynamicSortedList<>());
        }
    }

    public void addWord(String word) {
        word = word.toLowerCase();
        buckets.goTo(getIndex(word));

        if (!buckets.getActual().contains(word))
            buckets.getActual().insert(word);
    }

    /**
     * Tells whether a word exists in the dictionary
     * @param word word to search for
     * @return true if the word is in the dictionary, false otherwise
     */
    public boolean containsWord(String word) {
        word = word.toLowerCase();
        buckets.goTo(getIndex(word));
        return buckets.getActual().contains(word);
    }

    /**
     * @param word word to get suggestions for
     * @return list of similar sounding words
     */
    public DynamicList<String> getSuggestions(String word) {
        buckets.goTo(getIndex(word));
        return buckets.getActual();
    }

    /**
     * Finds the index of the given word
     * @param word
     * @return index corresponding to the word
     */
    private int getIndex(String word) {
        String soundex = Soundex.encode(word);

        int firstLetterValue = soundex.charAt(0)-'A';
        int remainingValue = Integer.valueOf(soundex.substring(1,4));

        return firstLetterValue * 1000 + remainingValue;
    }


}
