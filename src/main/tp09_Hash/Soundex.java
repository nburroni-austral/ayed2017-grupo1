package main.tp09_Hash;

public class Soundex {
    public static String encode(String word) {
        return encode(word.toCharArray());
    }

    /**
     * @param c character to convert
     * @return character corresponding to the soundex conversion
     */
    private static char convertCharacter(char c) {
        c = Character.toLowerCase(c);
        String current = String.valueOf(c);

        if ("bfpv"      .contains(current)) return '1';
        if ("cgjkqsxz"  .contains(current)) return '2';
        if ("dt"        .contains(current)) return '3';
        if ("l"         .contains(current)) return '4';
        if ("mn"        .contains(current)) return '5';
        if ("r"         .contains(current)) return '6';
        else return '0';
    }

    /**
     * Encodes a word according to the soundex conversion
     * @param word word to convert
     * @return 4 character soundex code
     */
    private static String encode(char[] word) {
        StringBuilder soundex = new StringBuilder("" + Character.toUpperCase(word[0]));

        char current;
        char previous = word[0];

        for (int i = 1; i < word.length; i++) {
            current = convertCharacter(word[i]);

            if (current != '0' & current != previous)
                soundex.append(current);

            previous = current;
        }

        if (soundex.length() < 4) soundex.append("0000");

        return soundex.substring(0,4);
    }
}
