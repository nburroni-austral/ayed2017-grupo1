package main.tp09_Hash;

import struct.impl.lists.DynamicList;

import java.io.*;
import java.util.Scanner;

public class SpellcheckerTest {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_MAG = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";

    public static void main(String[] args) {
        // Point to files
        File inputFile = new File("src/main/tp09_Hash/short_dictionary");
        SoundexSpellChecker spellChecker = new SoundexSpellChecker();

        try {
            // Set up reader
            FileReader fileReader = new FileReader(inputFile);
            BufferedReader bf = new BufferedReader(fileReader);

            // Read words and add them to the dictionary
            bf.lines().forEach(spellChecker::addToDictionary);

            // Close buffers
            bf.close();
        } catch (IOException e) { e.printStackTrace(); }

        String input = "";
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter Word or phrase");

        while (!input.equals("0")) {
            input = scanner.next();

            if (spellChecker.wordExists(input)) {
                System.out.println(ANSI_CYAN + input + ANSI_RESET + ": Word okay");

            } else {
                DynamicList<String> suggestions = spellChecker.getSuggestions(input);

                if (suggestions.size() == 0) {
                    System.out.println(ANSI_MAG + input + ANSI_RESET + ": Word does not exist, no suggestions.");
                    continue;
                }

                System.out.println(ANSI_MAG + input + ANSI_RESET + ": Word not okay, did you mean...");

                for (int i = 0; i < suggestions.size(); i++) {
                    suggestions.goTo(i);
                    if (i % 10 == 0)
                        System.out.println();
                    System.out.print((i != suggestions.size() - 1 ? " " : " or ") +
                            suggestions.getActual() +
                            (i != suggestions.size() - 1 ? "," : "?"));
                }

                System.out.println();
                System.out.println();
            }
        }
    }
}
