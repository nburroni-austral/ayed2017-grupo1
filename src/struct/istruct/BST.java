package struct.istruct;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 13/04/2017.
 */
public interface BST<T,K> {
    boolean isEmpty();
    T getRoot();
    BST<T,K> getLeft();
    BST<T,K> getRight();
    boolean exists(K key);
    T getMin();
    T getMax();
    T search(K key);
    void insert(T value, K key);
    void delete(K key);
}
