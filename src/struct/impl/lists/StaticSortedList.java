package struct.impl.lists;

import struct.impl.lists.StaticList;
import struct.istruct.list.SortedList;

/**
 * Static implementation of a sorted list (descending order)
 */
public class StaticSortedList<T extends Comparable<? super T>> extends StaticList<T> implements SortedList<T>{

    @Override
    public void insert(T element) {
        // If list is empty insert in first position
        if (isVoid()) {
            insertNext(element);
            return;
        }

        for (int i = 0; i<size();i++) {
            goTo(i);
            if (element.compareTo(getActual()) < 0) {
                insertPrev(element);
                return;
            }
        }

        if (element.compareTo(getActual()) < 0)
            insertPrev(element);
        else insertNext(element);
    }

}
