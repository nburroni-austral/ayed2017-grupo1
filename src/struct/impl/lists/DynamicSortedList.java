package struct.impl.lists;

import struct.istruct.list.SortedList;

import java.util.LinkedList;

/**
 * Static implementation of a sorted list (descending order)
 */
public class DynamicSortedList<T extends Comparable<? super T>> extends DynamicList<T> implements SortedList<T>{

    @Override
    public void insert(T element) {
        // If list is empty insert in first position
        if (isVoid()) {
            insertNext(element);
            return;
        }

        goTo(0);

        // If element is smaller than first insert before
        if (element.compareTo(getActual()) < 0) {
            insertPrev(element);
            return;
        } else {
            while (!endList()) {
                if (element.compareTo(getActual()) < 0)
                    break;
                goNext();
            }
        }
        if (element.compareTo(getActual()) < 0)
            insertPrev(element);
        else insertNext(element);
    }

    public boolean contains(T element) {
        for (int i = 0; i < size(); i++) {
            goTo(i);
            if (getActual().compareTo(element) == 0)
                return true;
        }

        return false;
    }

}
