package struct.impl.util.mains;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Class that contains interfaces and provides access to them through and interactive console menu
 */
public abstract class ConsoleMainMenu {
    public ArrayList<ConsoleInterface> consoleInterfaces;
    private Scanner scanner;
    private String title;

    public ConsoleMainMenu(String title) {
        this(title, new ArrayList<>());
    }

    public ConsoleMainMenu(String title, ArrayList<ConsoleInterface> interfaces) {
        this.scanner = new Scanner(System.in);
        this.title = title;
        this.consoleInterfaces = interfaces;
    }

    /**
     * Present options and allow a user to select one
     */
    public void run() {
        display();

        for (int i=0; i<consoleInterfaces.size(); i++) {
            System.out.println((i+1)+" - "+consoleInterfaces.get(i).getName());
        }

        runSelection(scanner.nextInt()-1);

    }

    private void display() {
        System.out.println(title);
        System.out.println("-----------------");
        System.out.println("Choose an exercise");
    }

    /**
     * Runs the selected interface
     * @param index index of the interface to run
     */
    private void runSelection(int index) {
        this.consoleInterfaces.get(index).runConsoleUI();
    }
}
