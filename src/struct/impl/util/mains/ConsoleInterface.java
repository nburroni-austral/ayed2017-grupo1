package struct.impl.util.mains;

public abstract class ConsoleInterface {
    private String name;

    public ConsoleInterface(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract void runConsoleUI();
}
