package struct.impl.trees;

import struct.istruct.BST;

import java.io.Serializable;

/**
 * <b>BinarySearchTree</b>
 *
 * Implementation of Binary tree ADT, a double-node based data structure.
 *
 * @author Nicolas Gargano & Lautaro Paskeviucius
 */

public class BinarySearchTree<T , K extends Comparable<? super K>> implements BST<T,K> {

    private DoubleNode root;

    /**
     * <b>BinarySearchTree</b>
     * <p>
     * Constructor. Sets root as null.
     */
    public BinarySearchTree() {
        root = null;
    }

    /**
     * @return true/false if root is empty or not
     */
    @Override
    public boolean isEmpty() {
        return root == null;
    }

    /**
     * @return returns value stored at root.
     */
    @Override
    public T getRoot() {
        return root.value;
    }

    /**
     * @return left tree
     */
    public BinarySearchTree<T,K> getLeft() {
        BinarySearchTree<T,K> t = new BinarySearchTree<>();
        t.root = this.root.left;
        return t;
    }

    /**
     * @return right tree
     */
    @Override
    public BinarySearchTree<T,K> getRight() {
        BinarySearchTree<T,K> t = new BinarySearchTree<>();
        t.root = root.right;
        return t;
    }

    /**
     * <b>exists</b>
     * <p>
     * Checks if a value is contained in tree.
     *
     * @param key element's key to look for in tree.
     * @return true/false if contained or not.
     */
    @Override
    public boolean exists(K key) {
        return exists(root, key);
    }

    /**
     * @return minimum value contained in tree
     */
    @Override
    public T getMin() {
        return getMin(root).value;
    }

    /**
     * @return maximum value contained in tree
     */
    @Override
    public T getMax() {
        return getMax(root).value;
    }

    /**
     * <b>search</b>
     *
     *
     *
     * @param key
     * @return
     */
    @Override
    public T search(K key) {
        return search(root, key).value;
    }

    /**
     *
     * @param key
     */
    @Override
    public void insert(T value, K key){
        root = insert(root, value, key);
    }

    /**
     *
     * @param key
     */
    @Override
    public void delete(K key){
        root = delete(root, key);
    }



    // Private methods

    private boolean exists(DoubleNode t, K key) {
        if (t == null)
            return false;
        if (key.compareTo(t.key) == 0)
            return true;
        else if (key.compareTo(t.key) < 0)
            return exists(t.left, key);
        else
            return exists(t.right, key);
    }

    private DoubleNode getMin(DoubleNode t){
        //not empty <- (Cambiar por exepcion)
        if (t.left == null)
            return t;
        else
            return getMin(t.left);
    }

    private DoubleNode getMax(DoubleNode t){
        //not empty <- (Cambiar por exepcion)
        if (t.right == null)
            return t;
        else
            return getMax(t.right);
    }

    private DoubleNode search(DoubleNode t, K key){
        // precondicion: valor a buscar pertenece al arbol <- (Cambiar por exepcion)

        if (key.compareTo( t.key)== 0)
            return t;
        else if (key.compareTo( t.key)< 0)
            return search(t.left, key);
        else
            return search(t.right, key);
    }



    private DoubleNode insert (DoubleNode t, T value, K key) {
        // precondicion: valor a insertar no pertenece al arbol <- (Cambiar por exepcion)

        if (t == null){
            t = new DoubleNode();
            t.value = value;
            t.key = key;
        }
        else if (key.compareTo(t.key) < 0)
            t.left = insert(t.left, value, key);
        else
            t.right = insert(t.right, value, key);
        return t;
    }


    private DoubleNode delete (DoubleNode t, K key) {
        // precondicion: valor a borrar pertence al arbol <- (Cambiar por exepcion)

        if (key.compareTo(t.key) < 0) {
            t.left = delete(t.left, key);
        } else if (key.compareTo(t.key) > 0) {
            t.right = delete(t.right, key);
        } else {
            if (t.left != null && t.right != null ) {
                t.value = getMin(t.right).value;
                t.right = deleteMin(t.right);
            }
            else if (t.left != null)
                t = t.left;
            else
                t =t.right;
        }

        return t;
    }

    private DoubleNode deleteMin(DoubleNode t){
        if (t.left != null)
            t.left = deleteMin(t.left);
        else
            t = t.right;
        return t;
    }

    /**
     * <b>DoubleNode</b>
     *
     * Basic structure of a tree (inner class)
     *
     */
    private class DoubleNode {
        T value;
        K key;
        DoubleNode left;
        DoubleNode right;

        DoubleNode(){
            value = null;
            key = null;
        }

        DoubleNode(T value, K key) {

            this.value = value;
            this.key = key;
        }
    }
}
