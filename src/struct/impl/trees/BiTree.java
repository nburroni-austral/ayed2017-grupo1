package struct.impl.trees;
import struct.istruct.BinaryTree;

import java.io.Serializable;

public class BiTree<T> implements BinaryTree<T>, Serializable {

    private BiTreeNode<T> root;

    public BiTree (T element, BiTree<T> left, BiTree<T> right) {
        root = new BiTreeNode<>(element);
        root.left = left.root;
        root.right = right.root;
    }

    public BiTree (T element) {
        root = new BiTreeNode<>(element);
    }

    public BiTree ()  {
        this.root = null;
    }

    @Override
    public boolean isEmpty() {
        return root == null;
    }

    @Override
    public T getRoot() {
        return root.element;
    }

    @Override
    public BinaryTree<T> getLeft() {
        BiTree<T> t = new BiTree<>();
        t.root = this.root.left;
        return t;
    }

    @Override
    public BinaryTree<T> getRight() {
        BiTree<T> t = new BiTree<>();
        t.root = this.root.right;
        return t;
    }

    private class BiTreeNode<T> {
        T element;
        BiTreeNode left, right;

        public BiTreeNode(T element, BiTreeNode<T> left, BiTreeNode<T> right) {
            this.element = element;
            this.left = left;
            this.right = right;
        }
        public BiTreeNode() {
            this(null, null, null);
        }

        public BiTreeNode(T element) {
            this(element, null, null);
        }
    }

    //    public int amountOfLeaves() {
//        return amountOfLeaves(this);
//    }
//
//    private int amountOfLeaves(BinaryTree tree) {
//        if (tree.isEmpty())
//            return 0;
//        if (tree.getLeft().isEmpty() && tree.getRight().isEmpty())
//            return 1;
//        return amountOfLeaves(tree.getLeft()) + amountOfLeaves(tree.getRight());
//    }

//    public int weight() {
//        return weight(this);
//    }
//
//    private int weight(BinaryTree tree) {
//        if (tree.isEmpty())
//            return 0;
//
//        return 1 + weight(getLeft()) + weight(getRight());
//    }

//    public int elementOccurrences(T el) {
//        return elementOccurrences(this, el);
//    }
//
//    private int elementOccurrences(BinaryTree tree, T el) {
//        if (tree.isEmpty())
//            return 0;
//        return (tree.getRoot().equals(el)? 1:0) +
//                elementOccurrences(tree.getLeft(), el) +
//                elementOccurrences(tree.getRight(), el);
//    }

//    public int nodesOnLevel(int level) {
//        return nodesOnLevel(this, 0, 1);
//    }
//
//    private int nodesOnLevel(BinaryTree tree, int currLevel, int desiredLevel) {
//        if (tree.isEmpty())
//            return 0;
//        if (currLevel == desiredLevel)
//            return 1;
//        return nodesOnLevel(tree.getLeft(), currLevel+1, desiredLevel) +
//                nodesOnLevel(tree.getRight(), currLevel+1, desiredLevel);
//    }

//    public int height() {
//        return height(this);
//    }
//
//    private int height(BinaryTree tree) {
//        if (tree.isEmpty())
//            return 0;
//        return 1 + Math.max(height(tree.getLeft()), height(tree.getRight()));
//    }
}
