package struct.impl.trees;

import java.io.Serializable;

/**
 * <b>BinaryTree</b>
 *
 * Implementation of Binary tree ADT, based on a DoubleNode of type T.
 *
 * @author Nicolas Gargano & Lautaro Paskevicius
 */

public class BinaryTree<T> implements Serializable, struct.istruct.BinaryTree{

    DoubleNode<T> root;

    /**
     * Creates an empty Binary tree
     */
    public BinaryTree() {
        root = null;
    }

    public BinaryTree(T value) {
        root = new DoubleNode<>(value);
    }

    public BinaryTree(T value, BinaryTree<T> left, BinaryTree<T> right) {
        root = new DoubleNode<>(value, left.root, right.root);
    }

    /**
     * @return true/false if root is empty or not
     */
    @Override
    public boolean isEmpty() {
        return root == null;
    }

    /**
     * @return returns root as double node.
     */
    @Override
    public T getRoot() {
        return root.value;
    }

    /**
     * @return left tree
     */
    @Override
    public BinaryTree<T> getLeft() {
        BinaryTree<T> t = new BinaryTree<>();
        t.root = this.root.left;
        return t;
    }

    /**
     * @return right tree
     */
    @Override
    public BinaryTree<T> getRight() {
        BinaryTree<T> t = new BinaryTree<>();
        t.root = root.right;
        return t;
    }

    /**
     * <b>DoubleNode</b>
     *
     * Basic structure of a tree (inner class)
     *
     * @param <T> Values stored in node
     */
    private class DoubleNode<T> implements Serializable {
        T value;
        DoubleNode<T> left;
        DoubleNode<T> right;

        DoubleNode(T value) {
            this.value = value;
        }

        DoubleNode(T value, DoubleNode<T> left, DoubleNode<T> right) {
            this.value = value;
            this.left = left;
            this.right = right;
        }
    }
}