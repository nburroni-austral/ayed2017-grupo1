package struct.impl.trees;

import struct.istruct.BinaryTree;

import java.util.ArrayList;
import java.util.List;

public class BiTreeAnalyzer {

    /**
     * Returns the quantity of leaves in the input tree. That is, the number of nodes without children.
     *
     * @param tree A binary tree
     * @return Amount of leaves in the tree
     */

    public int amountOfLeaves(BinaryTree tree) {
        if (tree.isEmpty())
            return 0;
        if (tree.getLeft().isEmpty() && tree.getRight().isEmpty())
            return 1;
        return amountOfLeaves(tree.getLeft()) + amountOfLeaves(tree.getRight());
    }

    /**
     * Returns the weight of the input tree. That is, the number of nodes it contains together with its children.
     *
     * @param tree A binary tree
     * @return Weight of the tree
     */
    public int weight(BinaryTree tree) {
        if (tree.isEmpty())
            return 0;

        return 1 + weight(tree.getLeft()) + weight(tree.getRight());
    }

    public <T> int elementOccurrences(BinaryTree<T> tree, T el) {
        if (tree.isEmpty())
            return 0;
        return (tree.getRoot().equals(el) ? 1 : 0) +
                elementOccurrences(tree.getLeft(), el) +
                elementOccurrences(tree.getRight(), el);
    }

    /**
     * Count of the nodes present on a tree's given level
     *
     * @param tree  Binary tree to be analyzed
     * @param level Desired level to evaluate
     * @return Amount of nodes in the desired level
     */
    public int nodesOnLevel(BinaryTree tree, int level) {
        return nodesOnLevel(tree, 1, level);
    }

    /**
     * Recursive helper of nodesOnLevel public method.
     *
     * @param tree         Binary tree to be analyzed
     * @param currLevel    Current tree's level
     * @param desiredLevel Desired level to evaluate
     * @return Amount of nodes in the desired level
     */
    private int nodesOnLevel(BinaryTree tree, int currLevel, int desiredLevel) {
        if (tree.isEmpty())
            return 0;
        if (currLevel == desiredLevel)
            return 1;
        return nodesOnLevel(tree.getLeft(), currLevel + 1, desiredLevel) +
                nodesOnLevel(tree.getRight(), currLevel + 1, desiredLevel);
    }

    /**
     * Evaluates the tree's height
     *
     * @param tree Binary tree to be analyzed
     * @return Tree's height
     */
    public int height(BinaryTree tree) {
        if (tree.isEmpty())
            return 0;
        return 1 + Math.max(height(tree.getLeft()), height(tree.getRight()));
    }

    public <T> void showFrontier(BinaryTree<T> tree) {
        for (T el : getFrontier(tree))
            System.out.println(el.toString());
    }

    public <T> ArrayList<T> getFrontier(BinaryTree<T> tree) {
        ArrayList<T> list = new ArrayList<>();
        fillListWithTreeLeaves(tree, list);
        return list;
    }

    private <T> void fillListWithTreeLeaves(BinaryTree<T> tree, ArrayList<T> list) {
        if (tree.isEmpty())
            return;
        if (tree.getLeft().isEmpty() & tree.getRight().isEmpty())
            list.add(tree.getRoot());
        fillListWithTreeLeaves(tree.getLeft(), list);
        fillListWithTreeLeaves(tree.getRight(), list);
    }

    boolean isFull(BinaryTree tree) {
        return weight(tree) == Math.pow(2, height(tree)) - 1;
    }

    /**
     * Checks if tree is complete.
     * A tree is considered complete if it is a leaf or has two complete children.
     *
     * @param tree
     * @return true if tree is complete, false if it is not.
     */
    boolean isComplete(BinaryTree tree) {
        if (tree.getLeft().isEmpty() & tree.getRight().isEmpty())
            return true;
        if (!tree.getLeft().isEmpty() & !tree.getRight().isEmpty())
            return isComplete(tree.getLeft()) & isComplete(tree.getRight());
        return false;
    }
    /**
     * Checks if trees are isomorphic. That is, they have the same structure.
     *
     * @param tree tree to be searched
     * @param other subtree to find in tree
     * @return true if trees are isomorphic, false if they are not.
     */
    boolean areIsomorphs(BinaryTree tree, BinaryTree other) {
        if (tree.isEmpty() && other.isEmpty())
            return true;

        if (tree.isEmpty() | other.isEmpty())
            return false;

        if (areIsomorphs(tree.getLeft(), other.getLeft()) &
                areIsomorphs(tree.getRight(), other.getRight()))
            return true;

        return false;
    }
    /**
     * Checks if trees are similar. That is, they contain the same elements, even if not in the same order or with
     * different structures.
     *
     * @param tree one of the trees to compare
     * @param otherTree the other tree to compare
     * @return true if trees are similar, false if they are not.
     */
    <T> boolean areSimilar(BinaryTree<T> tree, BinaryTree<T> otherTree) {
        List<T> list = new ArrayList<>();
        List<T> otherList = new ArrayList<>();

        fillListWithTreeElements(tree, list);
        fillListWithTreeElements(otherTree, otherList);

        if (list.size() != otherList.size())
            return false;

        for (T el : list) {
            if (otherList.contains(el)) {
//                list.remove(el);
                otherList.remove(el);
            } else {
                return false;
            }
        }
        return true;
    }

    private <T> void fillListWithTreeElements(BinaryTree<T> tree, List<T> list) {
        if (tree.isEmpty())
            return;
        list.add(tree.getRoot());
        fillListWithTreeElements(tree.getLeft(), list);
        fillListWithTreeElements(tree.getRight(), list);
    }

    /**
     * Checks if the second tree is contained in the first one.
     *
     * @param tree tree to be searched
     * @param subTree subtree to find in tree
     * @return true if trees are identical, false if they are not.
     */
    boolean isSubtree(BinaryTree tree, BinaryTree subTree) {
        if (subTree.isEmpty())
            return true;

        if (tree.isEmpty())
            return false;

        if (areIdentical(tree, subTree))
            return true;

        return isSubtree(tree.getLeft(), subTree) | isSubtree(tree.getRight(), subTree);
    }

    /**
     * Checks if trees are identical.
     *
     * @param tree
     * @param other
     * @return true if trees are identical, false if they are not.
     */
    public boolean areIdentical(BinaryTree tree, BinaryTree other) {
        if (tree.isEmpty() && other.isEmpty())
            return true;

        if (tree.isEmpty() || other.isEmpty())
            return false;

        return (tree.getRoot() == other.getRoot()
                && areIdentical(tree.getLeft(), other.getLeft())
                && areIdentical(tree.getRight(), other.getRight()));
    }
}
