package struct.impl.trees;

/**
 * Created by Lautaro Paskevicius (lautaro.paskevicius@ing.austral.edu.ar)
 * on 13/04/2017.
 */
public interface Searchable {
    String getKey();
}
