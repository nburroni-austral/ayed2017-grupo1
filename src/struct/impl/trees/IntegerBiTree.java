package struct.impl.trees;

import struct.istruct.BinaryTree;

public class IntegerBiTree extends BiTree<Integer>{
    public IntegerBiTree() {
        super();
    }

    public IntegerBiTree(Integer i) {
        super(i);
    }

    public IntegerBiTree(Integer i, IntegerBiTree left, IntegerBiTree right) {
        super(i, left, right);
    }

    public static int sumOfTree(BinaryTree<Integer> tree) {
        if (tree.isEmpty())
            return 0;
        return tree.getRoot() + sumOfTree(tree.getLeft()) + sumOfTree(tree.getRight());
    }

    public static int sumOfMultiples(BinaryTree<Integer> tree, int mult) {
        if (tree.isEmpty())
            return 0;

        return (tree.getRoot()%mult == 0? tree.getRoot() : 0) +
                sumOfMultiples(tree.getLeft(), mult) +
                sumOfMultiples(tree.getRight(), mult);
    }

    boolean isStable() {
        return isStable(this, this.getRoot());
    }

    private boolean isStable(BinaryTree<Integer> tree, Integer parentValue) {
        if (!(tree.getRoot() < parentValue))
            return false;
        if (!isStable(tree.getLeft(), tree.getRoot()))
            return false;
        if (!isStable(tree.getRight(), tree.getRoot()))
            return false;
        return true;
    }
}
