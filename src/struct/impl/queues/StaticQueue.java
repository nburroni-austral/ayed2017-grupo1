package struct.impl.queues;

// Think of the array as a circle

import struct.istruct.Queue;

public class StaticQueue<Q> implements Queue<Q> {
    private int front;
    private int back;

    private int capacity;
    private int length;

    private Q[] data;

    public StaticQueue() {
        this(10);
    }

    public StaticQueue(int capacity) {
        this.front = 0;
        this.back = 0;
        this.capacity = capacity;
        this.length = 0;
        data = (Q[]) new Object[capacity];
    }

    @Override
    public void enqueue(Q el) {
        if (length == capacity)
            grow();

        data[back] = el;
        back = increment(back);
        length++;
    }

    @Override
    public Q dequeue() {
        if (isEmpty())
            throw new CanNotDequeueEmptyQueueException();

        Q el = this.data[front];

        front = increment(front);
        length--;
        return el;
    }

    @Override
    public boolean isEmpty() {
        return length == 0;
    }

    @Override
    public int length() {
        return length;
    }

    public int capacity() {
        return capacity;
    }

    @Override
    public void empty() {
        data = (Q[]) new Object[capacity];
        front = 0;
        back = 0;
        length = 0;
    }

    private int increment(int toIncrement) {
        if(toIncrement == capacity-1) {
            toIncrement = 0;
            return toIncrement;
        }

        return ++toIncrement;
    }

    private void grow() {
        Q[] newArray = (Q[]) new Object[capacity*2];

        int copyIndex = 0;
        int tmpLength = length;

        while (!isEmpty()) {
            newArray[copyIndex++] = dequeue();
        }

        length = tmpLength;
        data = newArray;
        capacity = newArray.length;
        front = 0;
        back = copyIndex;
    }

}
