package struct.impl.queues;


import struct.istruct.Queue;


public class DynamicQueue<T> implements Queue<T> {
    private Node<T> firstNode;
    private Node<T> lastNode;

    public DynamicQueue() {
        this.firstNode = null;
        this.lastNode = null;
    }

    @Override
    public void enqueue(T el) {
        Node<T> aux = new Node<>(el);
        if(isEmpty()) {
            firstNode = aux;
            lastNode = aux;
            return;
        }
        lastNode.next=aux;
        lastNode=aux;
    }

    @Override
    public T dequeue() {

        if(isEmpty())
            throw new CanNotDequeueEmptyQueueException();

        T result = firstNode.data;
        firstNode = firstNode.next;
        return result;
    }

    @Override
    public boolean isEmpty() {
        return firstNode == null;
    }

    @Override
    public int length() {
        int counter = 0;
        Node<T> aux = firstNode;

        while (aux.next != null) {
            counter++;
            aux = aux.next;
        }

        return counter;
    }


    public int capacity() {
        // TODO decide what to do since it is theoretically unlimited.
        // In the case of the static queue we return the amount of elements we can store
        // with the memory we already have. The parallel to that here would be to return the current length
        // of the queue, since adding a node would imply asking for more memory.

        return length();
    }

    @Override
    public void empty() {
        firstNode = null;
        lastNode = null;
    }

    /**
     * <b>Node</b>
     *
     * Inner class. Unit to build DynamicStack.
     *
     * @param <T> type of Nodes stored in stack.
     */
    private class Node<T> {
        public T data;
        public Node<T> next;

        public Node(Object o){
            data = (T) o;
        }
    }
}
