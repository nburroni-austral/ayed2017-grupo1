package struct.impl.queues;

import struct.istruct.Queue;

/**
 * <b>PriorityQueue</b>
 *
 * Inserts elements by priority. Priorities are a way of coordinating multiple tp4_Queues
 * to form a single queue ordered by relevance.
 *
 * @param <T> data type stored
 */
public class PriorityQueue<T> {

    private int size;
    private int amountOfPriorities;
    private DynamicQueue<T>[] priorities;


    /**
     * <b>PriorityQueue(Constructor)</b>
     *
     * Initializes all tp4_Queues corresponding to a certain priority.
     *
     * @param amountOfPriorities priorities to arrange the queue
     */
    public PriorityQueue(int amountOfPriorities){
        this.amountOfPriorities = amountOfPriorities;
        this.priorities = new DynamicQueue[amountOfPriorities];

        for(int i = 0; i < amountOfPriorities; i++){
            priorities[i] = new DynamicQueue<>();
        }

        this.size = 0;
    }

    /**
     * <b>enqueue</b>
     *
     *
     *
     * @param t element to deposit.
     * @param priority priority to arrange ele
     */
    public void enqueue(T t, int priority) {
        if(priority > amountOfPriorities){
            throw new RuntimeException("Priority does not exist");
        }

        priorities[priority-1].enqueue(t);
        size++;
    }

    /**
     * <b>dequeue</b>
     *
     * Takes the first element of the highest priority.
     *
     * @return element de-queued.
     */

    public T dequeue() {
        for(int i = amountOfPriorities-1; i >= 0; i--){
            if(!priorities[i].isEmpty()){
                size--;

                return priorities[i].dequeue();
            }
        }

        throw new RuntimeException("All tp4_Queues are empty!");
    }

    /**
     * <b>isEmpty</b>
     *
     * Checks if there are elements stored in PriorityQueue
     *
     * @return true/false if the are elements stored in queue
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * <b>size</b>
     *
     * @return amount of elements stored
     */
    public int size() {
        return size;
    }

    /**
     * <b>getAmountOfPriorities</b>
     *
     * @return initial definition of how many priorities exist in queue
     */
    public int getAmountOfPriorities() {
        return amountOfPriorities;
    }

    /**
     * <b>empty</b>
     *
     * Resets current queue to initial state.
     */
    public void empty() {
        this.priorities = (DynamicQueue<T>[]) new Queue[amountOfPriorities];

        for(int i = 0; i < amountOfPriorities; i++){
            priorities[i] = new DynamicQueue<>();
        }

        this.size = 0;
    }

}
