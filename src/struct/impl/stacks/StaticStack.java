package struct.impl.stacks;

import struct.istruct.Stack;

import java.util.EmptyStackException;

/**
 * <b>StaticStack</b>
 *
 * Creates an array-based Stack data structure, following LIFO logistic.
 *
 * @author Nicolás Gargano <nicolas.gargano @ ing.austral.edu.ar>, Lautaro Paskevicius <lautaro.paskevicius @ ing.austral.edu.ar>
 */
public class StaticStack<T> implements Stack {

    private int top;
    private T[] stackArray;

    /**
     * Constructor. Creates a StaticStack containing an empty array of length 10.
     *
     */
    public StaticStack() {
        this(10);
    }

    /**
     * <b>StaticStack</b>
     *
     * Constructor. Creates a StaticStack with top -1, and an empty array.
     *
     * @param size initial capacity of the stackArray field.
     */
    public StaticStack(int size){
        this.top = -1;
        this.stackArray =(T[]) new Object[size];
    }

    /**
     * <b>push</b>
     *
     * param o is stored in stackArray field at top. If there's no space, stackArray duplicates its capacity.
     *
     * @param o value to store at top.
     */
    @Override
    public void push(Object o) {
        top++;

        try{
            stackArray[top] = (T) o;
        }catch(ArrayIndexOutOfBoundsException e){
            this.grow();
            stackArray[top] = (T) o;
        }
    }

    /**
     * <b>pop</b>
     *
     * Reduces top value, so when push occurs, all values over top can be replaced.
     *
     */
    @Override
    public void pop() {

        if(top >= 0){
            top--;
        }
    }

    /**
     * <b>peek</b>
     *
     * Returns value at the top. If StaticStack is empty, throws exception.
     *
     * @return Object at the top.
     * @throws EmptyStackException
     */

    @Override
    public Object peek() throws EmptyStackException{
        if (top >= 0){
            return stackArray[top];
        }

        throw  new EmptyStackException();
    }

    /**
     * <b>isEmpty</b>
     *
     * Checks if StaticStack is empty or not.
     *
     * @return true/false if its empty or not.
     */
    @Override
    public boolean isEmpty() {
        return top == -1;
    }

    /**
     * <b>capacity</b>
     *
     * Size of the stack.
     *
     * @return  the number of Objects stored in the stack.
     */
    @Override
    public int size() {
        return top+1;
    }

    /**
     * <b>empty</b>
     *
     * Makes top = -1, so now every value can be replaced.
     */
    @Override
    public void empty() {
        top = -1;
    }

    /**
     * <b>grow</b>
     *
     * If push cant find a spot to store a new value, grow() is called.
     * Creates a new array 2x bigger and stores it to stackArray reference.
     */
    private void grow(){
        T[] bigger = (T[]) new Object[2*(stackArray.length)];

        for(int i = 0; i < stackArray.length; i++){
            bigger[i] = stackArray[i];
        }

        stackArray = bigger;
    }
}
