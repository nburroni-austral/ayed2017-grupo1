package struct.impl.stacks;

import struct.istruct.Stack;

import java.util.EmptyStackException;

/**
 * <b>StaticStack</b>
 *
 * Creates a node-based Stack data structure, following LIFO logistic.
 *
 * @author Nicolás Gargano <nicolas.gargano @ ing.austral.edu.ar>, Lautaro Paskevicius <lautaro.paskevicius @ ing.austral.edu.ar>
 */
public class DynamicStack<T> implements Stack<T> {
    private int size;
    private Node<T> headNode;

    /**
     * <b>DynamicStack</b>
     *
     * Constructor. Creates a DynamicStack with top 0.
     */
    public DynamicStack(){
        this.size = 0;
    }

    /**
     * <b>push</b>
     *
     * param o is stored in dynamicStack at top.
     *
     * A new node is created, with param o in data. Current headNode is signed as new node's next.
     * New node is now headNode. The capacity increments in 1.
     *
     * @param o value to store at top.
     */
    @Override
    public void push(T o) {
        Node<T> auxNode = new Node<>(o);
        auxNode.next = headNode;
        headNode = auxNode;
        size++;
    }

    /**
     * <b>pop</b>
     *
     * Value under top is now top. headNode is dismissed, as it's next takes it's place.
     * The capacity decrements in 1.
     */
    @Override
    public void pop() {
        if (size >= 1){
            headNode = headNode.next;
            size--;
        }
    }

    /**
     * <b>peek</b>
     *
     * Returns value at the top (headNode's data).
     *
     * @return Object at the top.
     */
    @Override
    public T peek() throws EmptyStackException {
        if( size > 0) {
            return headNode.data;
        }

        throw new EmptyStackException();
    }

    /**
     * <b>isEmpty</b>
     *
     * Checks if DynamicStatic is empty or not.
     *
     * @return true/false if its empty or not.
     */
    @Override
    public boolean isEmpty() {
        if(size == 0) return  true;
        return false;
    }

    /**
     * <b>capacity</b>
     *
     * Size of the stack.
     *
     * @return  the number of Objects stored in the stack.
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * <b>empty</b>
     *
     * While DynamicStack has a capacity > 0, pop its top.
     */
    @Override
    public void empty() {
        while (size != 0){
            this.pop();
        }
    }

    public T peekAndPop() {
        T el = peek();
        pop();
        return el;
    }

    /**
     * <b>Node</b>
     *
     * Inner class. Unit to build DynamicStack.
     *
     * @param <T> type of Nodes stored in stack.
     */
    private class Node<T> {
        public T data;
        public Node<T> next;

        public Node(Object o){
            data = (T) o;
        }
    }
}
